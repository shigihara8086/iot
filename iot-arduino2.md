[TOC]

# ■ IoTにおけるArduino その２
----

## ▼ Wemos Lolin ESP32 + BME280

![img_0035](./img/img_0035.jpg)

**wemos Lolin ESP32**に**BME280**を接続し、**気温・湿度・気圧**を測定できるようにしたもの。この構成にすると、測定データをOLEDディスプレイに表示しながら、データをWi-Fi経由でmuninサーバに送るといった方法が使える。とりあえず今回は**wemos Lolin ESP32**の**I2C**経由で測定できるかどうかの検証を行った。**wemos Lolin ESP32**はOLEDディスプレイの接続に**I2C**を使用しているので、**I2C**で使用するデータピンが固定されているのでライブラリとの整合性に不安があったが、検証の結果問題なく使用できることがわかった。

## ▼ Lolion32を使った計測ボード

先に**wemos Lolin ESP32**に**BME280**をつけたものを紹介したが、それだけではつまらないので、手持ちの**I2C**デバイスを３つ接続してみた。

![img_0036](./img/img_0036.jpg)

![img_0037](./img/img_0037.jpg)

![img_0038](./img/img_0038.jpg)

まだ余裕があったので、あと２つ追加してみた。

![](./img/img_0041.jpg)

上の写真は、**BME280(気温、湿度、気圧)**,**LIS3DH(３軸加速度計)**,**INA226(電圧、電流、消費電力)**、**ADT7410(温度計)**、**DS3231(リアルタイムクロック)**の５つを接続した時の表示である。この写真では見にくいが、LIS3DHの３軸の加速度を表示してところである。

記録のためスケッチを載せておく。

**multi_meter.h**

```C++
/*
  multi_meter header
*/

#define I2C_SDA 5                                     // SDA Pin No.5 yellow
#define I2C_SCL 4                                     // SCL Pin No.4 green
// #define WIRE_CLOCK 400000                             // I2C clock speed
#define LINE_OFFSET 4                                 // header offset lines
#define DISPLAY_TIMES 20                              // how many cycle
#define DISPLAY_WAIT 250                              // wait time par cycle
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)  // BME280 presser senser sealevel adjustment value

```

**multi_meater.ino**

```C++
/*
  INA226,BME280 multi-meter
*/

#include <Wire.h>
#include "SSD1306.h"
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"
#include "multi_meter.h"

SSD1306 display(0x3c, I2C_SDA, I2C_SCL);

int bme280_disable;
int lis3dh_disable;
int ina226_disable;
int adt7410_disable;
int ntp_disable;
int adc_disable;

int ssd1306_init() {
  display.init();
  display.flipScreenVertically();
  display.clear();
  display.setFont(ArialMT_Plain_16);
  // display.setFont(Monospaced_plain_16);
  display.setTextAlignment(TEXT_ALIGN_CENTER_BOTH);
  display.drawString(64, 32, "Multi-Meter");
  display.display();
  return 0;
}

void setup() {
  Serial.begin(115200);
  ssd1306_init();
  bme280_disable  = bme280_init();
  lis3dh_disable  = lis3dh_init();
  ina226_disable  = ina226_init();
  adt7410_disable = adt7410_init();
  ntp_disable     = ntp_init();
  adc_disable     = adc_init();
  delay(1000);
}

void loop() {
  Wire.reset();
  if (bme280_disable  == 0) {
    bme280_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (adt7410_disable == 0) {
    adt7410_display(DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (ina226_disable  == 0) {
    ina226_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (true) {
    adc_display    (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (true) {
    ntp_display    (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  if (lis3dh_disable  == 0) {
    lis3dh_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  Serial.println();
}
```

**adc.ino**

```c++
/*
  ADC
*/

#include "multi_meter.h"

#define ADC_ADJUSTMENT (1.06)

int adc_init() {
  return 0;
}

void adc_display(int times, int wait) {
  char strbuf[64];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int volt_pos = (16 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // 電圧、電流、電力読込み
    int adcv      = (int)(3.3 / 4096 * analogRead(A3) * 3 * 1000 * ADC_ADJUSTMENT);         // ADC(ADC3,SVN pin)

    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "ADC");

    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, volt_pos, "Volt");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, volt_pos, "mV");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5ld", adcv);
    display.drawString(data_pos, volt_pos, strbuf);

    if (not done) {
      Serial.printf("V:%5ldmV ", adcv);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**bme280.ino**

```C++
/*
  BME280
*/

#include <Wire.h>
#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"
#include "multi_meter.h"

#define BME280_ADD 0x76

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

int bme280_init() {
  bme.begin(BME280_ADD);
  return 0;
}

void bme280_display(int times, int wait) {
  char strbuf[100];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int temp_pos = (16 - LINE_OFFSET);
  int humi_pos = (32 - LINE_OFFSET);
  int pres_pos = (48 - LINE_OFFSET);
  bool done = false;

  // BME280
  for (int i = 0; i < times; i ++) {
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "BME280");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_10);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, temp_pos, "Temp");
    display.drawString(head_pos, humi_pos, "Humi");
    display.drawString(head_pos, pres_pos, "P:slvl");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, temp_pos, "ºC");
    display.drawString(unit_pos, humi_pos, "%");
    display.drawString(unit_pos, pres_pos, "hPa");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", bme.readTemperature());
    display.drawString(data_pos, temp_pos, strbuf);
    sprintf(strbuf, "%5.1f", bme.readHumidity());
    display.drawString(data_pos, humi_pos, strbuf);
    sprintf(strbuf, "%5.1f", bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
    display.drawString(data_pos, pres_pos, strbuf);

    if (not done) {
      Serial.printf("T:%5.1fºC,H:%5.1f%%,P:%5.1fhPa ", bme.readTemperature(), bme.readHumidity(), bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**ina226.ino**

```C++
/*
  INA226
*/

#include <Wire.h>
#include "multi_meter.h"

#define INA226_ADD (0x40)

// I2c通信送信
void ina226_writeRegister(byte reg, word value) {
  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  Wire.write((value >> 8) & 0xFF);
  Wire.write(value & 0xFF);
  Wire.endTransmission();
}

// I2c通信受信
word ina226_readRegister(byte reg) {
  word res = 0x0000;

  Wire.beginTransmission(INA226_ADD);
  Wire.write(reg);
  if (Wire.endTransmission() == 0) {
    if (Wire.requestFrom(INA226_ADD, 2) >= 2) {
      res  = Wire.read() * 256;
      res += Wire.read();
    }
  }
  return res;
}

int ina226_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  ina226_writeRegister(0x00, 0x4127);
  ina226_writeRegister(0x05, 0x0A00);
  return 0;
}

void ina226_display(int times, int wait) {
  char strbuf[64];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int volt_pos = (16 - LINE_OFFSET);
  int crnt_pos = (32 - LINE_OFFSET);
  int pwer_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // 電圧、電流、電力読込み
    long voltage  = (long)((short)ina226_readRegister(0x02)) * 1250L;    // LSB=1.25mV
    short current = (short)ina226_readRegister(0x04);
    long power    = (long)ina226_readRegister(0x03) * 25000L;            // LSB=25mW

    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "INA226");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, volt_pos, "Volt");
    display.drawString(head_pos, crnt_pos, "Current");
    display.drawString(head_pos, pwer_pos, "Power");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, volt_pos, "mV");
    display.drawString(unit_pos, crnt_pos, "mA");
    display.drawString(unit_pos, pwer_pos, "mW");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5ld", (voltage + (1000 / 2)) / 1000);
    display.drawString(data_pos, volt_pos, strbuf);
    sprintf(strbuf, "%5ld", current);
    display.drawString(data_pos, crnt_pos, strbuf);
    sprintf(strbuf, "%5ld", (power + (1000 / 2)) / 1000);
    display.drawString(data_pos, pwer_pos, strbuf);

    if (not done) {
      Serial.printf("V:%5ldmV,I:%5ldmA,P:%5ldmW ", (voltage + (1000 / 2)) / 1000, current, (power + (1000 / 2)) / 1000);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**lis3dh.ino**

```C++
/*
  LIS3DH
*/

#include <Wire.h>
#include "multi_meter.h"

#define LIS3DH_ADD 0x19

#define DEFAULT_XA (0.0)
#define DEFAULT_YA (0.0)

unsigned int lis3dh_readRegister(byte reg) {
  Wire.beginTransmission(LIS3DH_ADD);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(LIS3DH_ADD, 1);
  return Wire.read();
}

void lis3dh_writeRegister(byte reg, byte data) {
  Wire.beginTransmission(LIS3DH_ADD);
  Wire.write(reg);
  Wire.write(data);
  Wire.endTransmission();
}

int lis3dh_init() {
  lis3dh_writeRegister(0x20, 0x27);
  int res = lis3dh_readRegister(0x0f);
  return 0;
}

int s18(unsigned int v) {
  return -(v & 0b100000000000) | (v & 0b011111111111);
}

void lis3dh_display(int times, int wait) {
  char strbuf[100];
  unsigned int x, y, z, h, l;
  float xa, ya, za;
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int galx_pos = (16 - LINE_OFFSET);
  int galy_pos = (32 - LINE_OFFSET);
  int galz_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    // LIS3DH
    lis3dh_writeRegister(0x20, 0x27);
    // X
    l = lis3dh_readRegister(0x28);
    h = lis3dh_readRegister(0x29);
    x = (h << 8 | l) >> 4;
    xa = s18(x) / 1024.0 * 980.0 - DEFAULT_XA;
    // Y
    l = lis3dh_readRegister(0x2a);
    h = lis3dh_readRegister(0x2b);
    y = (h << 8 | l) >> 4;
    ya = s18(y) / 1024.0 * 980.0 - DEFAULT_YA;
    // Z
    l = lis3dh_readRegister(0x2c);
    h = lis3dh_readRegister(0x2d);
    z = (h << 8 | l) >> 4;
    za = s18(z) / 1024.0 * 980.0;
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "LIS3DH");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, galx_pos, "Xaxis");
    display.drawString(head_pos, galy_pos, "Yaxis");
    display.drawString(head_pos, galz_pos, "Zaxis");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", xa);
    display.drawString(data_pos, galx_pos, strbuf);
    sprintf(strbuf, "%5.1f", ya);
    display.drawString(data_pos, galy_pos, strbuf);
    sprintf(strbuf, "%5.1f", za);
    display.drawString(data_pos, galz_pos, strbuf);

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, galx_pos, "Gal");
    display.drawString(unit_pos, galy_pos, "Gal");
    display.drawString(unit_pos, galz_pos, "Gal");

    if (not done) {
      Serial.printf("X:%5.1fGal,Y:%5.1fGal,Z:%5.1fGal ", xa, ya, za);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**adt7410.ino**

```C++
/*
 ADT7410
*/

#include <Wire.h>
#include "multi_meter.h"

#define ADT7410_ADD 0x48

int adt7410I2CAddress = 0x48;

int adt7410_init() {
  Wire.begin(I2C_SDA, I2C_SCL);
  return 0;
}

unsigned int adt7410_readRegister(void) {
  unsigned int val;

  Wire.requestFrom(ADT7410_ADD, 2);
  val  = Wire.read() << 8;
  val |= Wire.read();
  return val;
}

// メインループ
void adt7410_display(int times, int wait) {
  char strbuf[100];
  int head_pos =  0;
  int data_pos = 94;
  int unit_pos = 128;
  int temp_pos = (16 - LINE_OFFSET);
  uint16_t uiVal; //2バイト(16ビット)の領域
  float fVal;
  int iVal;
  bool done = false;

  for (int i = 0; i < times; i ++) {
    uiVal = adt7410_readRegister();
    uiVal >>= 3;                          // シフトで13bit化
    if (uiVal & 0x1000) {                 // 13ビットで符号判定
      iVal = uiVal - 0x2000;              // マイナスの時 (10進数で8192)
    } else {
      iVal = uiVal;                       //プラスの時
    }
    fVal = (float)iVal / 16.0;            // 温度換算(摂氏)
    //
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "ADT7410");

    display.setFont(ArialMT_Plain_16);
    // display.setFont(Monospaced_plain_16);

    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, temp_pos, "Temp");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(unit_pos, temp_pos, "ºC");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    sprintf(strbuf, "%5.1f", fVal);
    display.drawString(data_pos, temp_pos, strbuf);

    if (not done) {
      Serial.printf("T:%5.1fºC ", fVal);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

**ntpclient.ino**

```C++
/*
  NTP client
*/

#include <WiFi.h>
#include <DS3231.h>

#include "multi_meter.h"

#define JST (3600L * 9)

const char *ssid = "********";
const char *password = "********";
const char *ntp_servers[] { "ntp.nict.jp", "ntp.jst.mfeed.ad.jp", "time.google.com" };

struct tm timeInfo;
DS3231 rtc;
bool rtc_century = false;
bool rtc_h12, rtc_pm;

char *dayofweek[] = { "Sunday", "Monday", "Tuesday", "Wednsday", "Thursday", "Friday", "Saturday" };
char *dayofweek_short[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

int ntp_init() {
  int count;

  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  if (WiFi.begin(ssid, password) != WL_DISCONNECTED) {
    ESP.restart();
  }
  count = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    count ++;
    if (count > 5) {
      return -1;
    }
  }
  configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
  // sprintf(datebuf,"%02d/%02d/%02d(%s)",timeInfo.tm_year + 1900 - 2000, timeInfo.tm_mon + 1, timeInfo.tm_mday,dayofweek_short[timeInfo.tm_wday]);
  // sprintf(timebuf,"%02d:%02d:%02d",timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
  getLocalTime(&timeInfo);
  rtc.setClockMode(false);  // set to 24h
  rtc.setYear(timeInfo.tm_year + 1900);
  rtc.setMonth(timeInfo.tm_mon + 1);
  rtc.setDate(timeInfo.tm_mday);
  rtc.setDoW(timeInfo.tm_wday);
  rtc.setHour(timeInfo.tm_hour);
  rtc.setMinute(timeInfo.tm_min);
  rtc.setSecond(timeInfo.tm_sec);
  return 0;
}

void ntp_ticker() {
  char strbuf[100];

  // Serial.print(".");
  if (!ntp_disable) {
    getLocalTime(&timeInfo);
    // sprintf(strbuf, "%02d/%02d(%s) %02d:%02d:%02d",
    //         timeInfo.tm_mon + 1, timeInfo.tm_mday, dayofweek_short[timeInfo.tm_wday],
    //         timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    sprintf(strbuf, "%02d/%02d %02d:%02d:%02d",
            timeInfo.tm_mon + 1, timeInfo.tm_mday, timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);

    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 0, strbuf);
    if (timeInfo.tm_hour == 0 && timeInfo.tm_min == 0 && timeInfo.tm_sec == 0) {
      configTime(JST, 0, ntp_servers[0], ntp_servers[1], ntp_servers[2]);
      getLocalTime(&timeInfo);
      rtc.setClockMode(false);  // set to 24h
      rtc.setYear(timeInfo.tm_year + 1900);
      rtc.setMonth(timeInfo.tm_mon + 1);
      rtc.setDate(timeInfo.tm_mday);
      rtc.setDoW(timeInfo.tm_wday);
      rtc.setHour(timeInfo.tm_hour);
      rtc.setMinute(timeInfo.tm_min);
      rtc.setSecond(timeInfo.tm_sec);
      delay(1000);
    }
  } else {
    int year  = rtc.getYear();
    int mon   = rtc.getMonth(rtc_century);
    int day   = rtc.getDate();
    int hour  = rtc.getHour(rtc_h12, rtc_pm);
    int min   = rtc.getMinute();
    int sec   = rtc.getSecond();
    char *dow = dayofweek_short[rtc.getDoW()];
    // sprintf(strbuf, "%02d/%02d(%s) %02d:%02d:%02d", mon, day, dow, hour, min, sec);
    sprintf(strbuf, "%02d/%02d %02d:%02d:%02d", mon, day, hour, min, sec);

    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(128, 0, strbuf);
  }
  display.drawLine(0, 11, 128, 11);
}

void ntp_display(int times, int wait) {
  char datebuf[20], timebuf[20];
  int head_pos =  0;
  int data_pos = 128;
  int date_pos = (16 - LINE_OFFSET);
  int time_pos = (32 - LINE_OFFSET);
  int week_pos = (48 - LINE_OFFSET);
  bool done = false;

  for (int i = 0; i < times; i ++) {
    if (ntp_disable == 0) {
      getLocalTime(&timeInfo);
      sprintf(datebuf, "%04d/%02d/%02d", timeInfo.tm_year + 1900, timeInfo.tm_mon + 1, timeInfo.tm_mday);
      sprintf(timebuf, "%02d:%02d:%02d", timeInfo.tm_hour, timeInfo.tm_min, timeInfo.tm_sec);
    } else {
      int year  = rtc.getYear() + 1952;
      int mon   = rtc.getMonth(rtc_century);
      int day   = rtc.getDate();
      int hour  = rtc.getHour(rtc_h12, rtc_pm);
      int min   = rtc.getMinute();
      int sec   = rtc.getSecond();
      char *dow = dayofweek_short[rtc.getDoW()];
      sprintf(datebuf, "%04d/%02d/%02d", year, mon, day);
      sprintf(timebuf, "%02d:%02d:%02d", hour, min, sec);
    }
    display.clear();
    ntp_ticker();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    if (ntp_disable == 0) {
      display.drawString(0, 0, "NTP");
    } else {
      display.drawString(0, 0, "DS3231");
    }
    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(head_pos, date_pos, "Date");
    display.drawString(head_pos, time_pos, "Time");
    display.drawString(head_pos, week_pos, "Week");

    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.drawString(data_pos, date_pos, datebuf);
    display.drawString(data_pos, time_pos, timebuf);
    if (ntp_disable == 0) {
      display.drawString(data_pos, week_pos, dayofweek[timeInfo.tm_wday]);
    } else {
      display.drawString(data_pos, week_pos, dayofweek[rtc.getDoW()]);
    }

    if (not done) {
      Serial.printf("DATE:%s,TIME:%s ", datebuf, timebuf);
      done = true;
    }
    display.display();
    delay(wait);
  }
}
```

センサー毎にソースファイルを分割している。arduino IDEはヘッダファイルを作成しなくとも自動的にプロトタイプ宣言を作ってコンパイルしてくれるようなので、ファイルを分割してもそれほど手間がかからない。

**Wemos Lolin ESP32**はオンボードディスプレイのコントロールにI2Cを使用しているのだが、標準的なライブラリとは使用しているピンが異なる。そのため、標準的なピン接続を想定しているライブラリは動かない事が多い。そのため、ピン番号を指定できる**BME280**以外はarduino用にダウンロードしたライブラリではなく、**Wireライブラリ**を直接使用して計測を行っている。

## ▼ OLEDの表示が止まる

こんな形でスケッチを作成し、**Wemos Lolin ESP32**にプログラムを転送しテストしていたところ、いつの間にか**OLEDの表示が止まる**という現象が発生していることに気がついた。いろいろ調べてみると、

- 電源のノイズで誤動作している
- I2Cバスのプルアップ抵抗が無い
- I2Cのクロックが高すぎる

などが考えられた。で、パスコンを入れたり、プルアップ抵抗を追加したり、`Wire.setClock()`でI2Cバスのクロックを下げてみたりとやってみたのだが、どうも事態は改善しない。

なにか情報はないかと色々ググっていたら、[ESP32 の I2C は壮絶にバグってる](http://d.hatena.ne.jp/wakwak_koba/touch/20171228)という記事を見つけた。どうもこれくさいということで、

**multi-meter.ino**

```C++
...
void loop() {
  Wire.reset();
  if (bme280_disable  == 0) {
    bme280_display (DISPLAY_TIMES, DISPLAY_WAIT);
  }
  ...
}
```

というふうに適当なタイミングでI2Cインターフェースをリセットするようにしてみた。これで事態が改善するようなら今後OLED付き以外でも大量のデータ転送を行う場合には適当なところで`Wire.reset()`を行うようにするのが良いようだ。

## ▼ NTPとDS3231RTC

とりあえず、手持ちのI2Cデバイスを全てつけてみようということで、時計の表示も追加した。基本的にはNTPを使って時刻を取得して表示するのだが、Wi-Fiの届かないところでは時刻が取得できない。そこで手持ちのDS3231RTCモジュールを接続し、

- NTPで時刻が取得できたらRTCもその時刻に同期
- NTPで時刻が取得できない場合はRTCから時刻を取得

という動作にしてみた。こうすれば、Wi-Fiで接続できる自宅ではNTP時刻を使い、屋外などNTPサーバに接続できないときはRTCを使って時刻を表示することができるようになる。RTCはNTPに接続した時にNTPの時刻と同期しているので、数日毎にNTPで時刻を取得してやれば、かなり正確な時刻を取得できる。

## ▼ adc.ino

これは、ESP32**内臓ADC**(ADコンバータ)を使用して、電池の電源電圧を測定するものである。回路的にはかなり怪しいものであるが、この構成では正常に動作しているようだ(電位差のある別電源系に接続しているので、本来の電源系やUSBの電源供給に悪影響を及ぼす可能性がある)。

**内臓ADC**はフルスケール(12bit=4086)で**3.3V**に対応しているので、バッテリー側の電源を抵抗で**1/3に分圧**して測定し、その値を**３倍**することで実際の電圧を測定している。**内蔵ADC**はあまり精度が良くないようなので、正確な値を測定することは難しいが、電源電圧を監視し、スレッショルド値以下なら起動しないようにするなどの**電源保護**(リチウムイオン電池は3.0V以下まで放電すると**充電できなくなる**)には使えそうである。

