[TOC]

# ■ IoTにおけるArduino その１
----

[電源系](./iot-power.md)でも書いたが、**Arduino**は単体ではできることが限られているが、**Raspberry pi**と組み合わせたり、高機能な周辺機器と組み合わせることで、様々な利用方法が考えられる。ここでは、その応用方法について考察してみたい。

## ▼ Arduinoとは

まとまった情報としては[WikipediaのArduinoの項目](https://www.wikiwand.com/ja/Arduino)が参考になる。また["arduino"でググれば](https://www.google.co.jp/search?q=arduino&ie=utf-8&oe=utf-8&hl=ja)様々な情報が引っかかってくる。情報は非常に多いので、かえってその中から有用な情報を見つけるのが大変な状況にある。

価格もいわゆる**ピンキリ**で、高いものは**5,000円以上**するものもあれば、中華ボードならば**400円**くらいからとなかなかの幅がある。今回評価に使用したのは「[Mini USB Arduino Nano V3.0 改造バージョン Arduino Nano V3.0互換ボード ATmega328P搭載 (5個)](https://www.amazon.co.jp/gp/product/B01F741W6O/ref=oh_aui_detailpage_o07_s00?ie=UTF8&psc=1)」というもので、5個で1,980円である。中華ボードの特徴としてはオンボードのUSBシリアル変換チップが**CH340**に変更されていてる、USBコネクタも**mini-B規格**になっているなど、本物に比べて多少の変更点がある。

**Arduino**はほとんど**ワンチップマイコン**であり、周辺チップとしては**USBシリアル変換チップ**と**12V→5Vシリーズレギュレータ**が載っている程度である。なので量産する場合はCPUチップのみ購入して互換ボードと同等の回路を構成することができる。

**Arduino**は高度な処理を行うことはできず、インターネットに接続することもTCPパケットの送受信程度の低機能な部分に限られており、そのままではIoTデバイスとしてはあまり使いやすいものではない。

しかしながら、**Raspberry pi zero W**と組み合わせると、補完的な動作をさせることが可能になり、活用の幅が大きく広がると思われる。

## ▼ Arduino microを入手した

**Arduino**は結構高いなあ、と思い躊躇していたのだが、**Arduino micro**が**2,780円**と比較的手頃な価格だったので購入してみた。

![Arduino micro](./img/img_0131.jpg "Arduino micro")

見ての通り、**34PIN DIP**パッケージと非常に小さい。
ブレッドボードに刺すことができるので、実験には便利な構造になっている。

なぜ購入したかというと、きちんとした理由がある。

- 電源をいれるだけで所定のプログラムが動作する
- 電源電圧が、7～12Vとワイド
- RTCモジュールが追加できる
- RTCモジュールからArduinoに割り込みがかけられる
- sleepモードが複数あり、数mAまで消費電力を落とすことができる

つまり、**RPi1114FDH**より安価に低消費電力の電源管理モジュールとして使える可能性があるのだ(まあ、やってみないとわからないが)。

とりあえず、RTCモジュールの接続や電源管理用のMOS-FETを接続しての**Lチカ**などの予備的な実験を行う予定である。

## ▼ Arduino nano互換品を入手した

とりあえず、Arduino microでの**Lチカ**はできたので「[Mini USB Arduino Nano V3.0 改造バージョン Arduino Nano V3.0互換ボード ATmega328P搭載 (5個)](https://www.amazon.co.jp/gp/product/B01F741W6O/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1)」なるものを入手した。**５個で1,980円**と格安の中華ボードである。
本物の**Arduino nano**はスイッチサイエンスで、**１個2,880円**なので**Arduino micro**と大差のない価格であるが、中華ボードは格安なので、幾つか壊したとしても惜しくはない。電源及びI2Cの実験用に使用する予定である。

**中華ボード**
![中華ボード](./img/img_0133.jpg)

これも、テストとして**Lチカ**で動作の確認を行った。

## ▼ I2Cの接続方法

最終的に**I2C**で**Raspberry pi**と**Arduino**を接続してマスタ・スレーブ構成のシステムにするわけだが、ひとつ問題が発覚した。それは、**I2C**の電圧が**Raspberry pi**は**3.3V**なのに対して**Arduino**は**5V**だということ。ロジックレベルの電圧が違うので、何らかの対策を行わなければならない。

そこで「[I2Cバス用双方向電圧レベル変換モジュール(PCA9306)](http://akizukidenshi.com/catalog/g/gM-05452/)」を使用して**I2C**のレベル変換を行って接続するようにする。価格も**１個150円**と安いので、このような目的には気軽に使える。

と目論んでいたのだが、あとで述べるセミ・セルフプログラミング環境を構築する場合、**Raspberry pi**と**Arduino**はシリアル(USBシリアル)で接続するのが最も合理的であると思うようになってきたのと、**I2C**は**Arduino**にデバイスを接続するために使用したいとか、いろいろあって**I2C**を使って接続することは断念した。

が、やってみたら、シリアル回線を使って**Arduino**を接続すると、**スケッチ**を書き込んだり、ターミナルソフトを使って**Arduino**に接続すると、**Arduino**が**リセット**されてしまうことがわかった。まあ確かに、何らかの方法でリセットがかからないと困るわけだが、ターミナルソフトで接続しただけでリセットされてしまうのは、ちょっと困ったことになる。詳細については後で述べる。

## ▼ Raspberry pi + Arduino

**Raspberry pi zero W**と**Arduino Nano**を組み合わせたのが下の写真である。

**Raspberry pi + Arduino**
![Raspberry pi + Arduino](./img/img_0134.jpg)

単純にケーブルを繋いだだけではあるが、この構成に多少の部品をつけると、

- **Raspberry pi zero W**の間欠駆動用コントローラ
- **Arduino**のセミ・セルフプログラミング環境
- **Arduino**を計測用プローブにする

などの用途に使用することができる。このときは比較的長い、通常のUSBケーブルを使用したが、現在は、「[USB mini-B→micro-B接続ケーブル](https://www.amazon.co.jp/gp/product/B00O4VKNXU/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)」を使って、変換コネクタなどなしにダイレクトに接続している。

### Raspberry pi Wの間欠駆動用コントローラ

**Raspberry pi zero W**を間欠的に駆動するために**RPi1114FDH**使用してきたわけだが、この役割を**Arduino**で行うことが可能であり、その動作もセミ・セルフプログラミング環境を使って**Raspberry pi zero W**から設定や変更を行うことができる。

**Arduino nano**の場合、動作している状態の消費電力は**10mA**程度であり、RTCモジュールと適当なスリープモードを選択すれば**数mA以下**にすることも不可能ではない。そうなると一旦は却下した太陽電池なしのバッテリー駆動も再度実現性を帯びてくる。

と思っていたのだが、先に述べたターミナルソフトで**リセット**がかかる問題をなんとかしないと、**Raspberry pi**から**Arduino**をコントロールしようとした途端、**RAspberry pi**の電源が落ちる、といったことが起こりかねないのである。

### Arduinoのセミ・セルフプログラミング環境

**Arduino**のプログラム開発には[Arduino IDE](https://www.arduino.cc/en/main/software)を使用して行うことが一般的であるが、この開発環境は**Raspberry pi zero W**上でも動かすことができる。USBケーブルが繋がっていれば、立派な開発環境として使用することが可能である。

**Raspberry pi zero W**で**X11**ウインドウ環境を動かすのはメモリーも食うし、リモートで行うことができないということが問題になるかもしれない。その場合には、[PlatfoemIO](http://platformio.org/)という開発環境をインストールすれば、Linuxのコマンドラインから**Arduino**プログラムのコンパイル・インストールを行うことができる。

### Arduinoを計測用プローブにする

**Raspberry pi zero W**を間欠的に動かす場合、**Raspberry pi zero W**単体では間欠動作のタイミングでしか計測を行うことができない。しかし**Arduino**をスレーブとして使えば、計測自体は**Arduino**のメモリーが許す限りの頻度で行うことができる。

## ▼ Piduino構想

とりあえず、電源系を含めた**Raspberry pi zero W**と**Arduino nano**をどのように接続するかを示したのが下図である。**構想**というほどだいそれたものではないのだが、回路にはちょっとした工夫がある。

![Piduino](./img/pi-duino.png)

付加した回路は電源系の**DC-DCコンバータ**と制御用の**p-ch MOSFET**だけである。**MOSFET**は**p-ch**タイプを使用することで、**Arduino**の電源が**OFF**の場合に**ON**になるようになっている。
こうすることで、シリアル回線を繋いだときに**Arduino**がリセットされたときでも**Raspberry pi**の電源が**OFF**にならないようにしている。**Raspberry pi**の電源を**OFF**にするときは**Arduino**の**D2**ピンを**HIGH**にすれば良い。

とりあえず、予備的な実験として**Raspberry pi**の代わりに**LED**をつけて正常に動くかどうかを検証した回路が下の写真である。

![検証回路](./img/img_0135.jpg)

**D2**ピンの代わりに**D13**ピンに接続して**Lチカ**プログラムを走らせたもの。LEDが交互に点滅し正常に動作していることが確かめられた。また、**D2**ピンに接続して、リセットされた時もLEDが点灯状態のままであることを検証した。またLEDに印加される電圧を測定し、**VCC電圧**がそのままLEDに印加されていることも検証した。

## ▼ リアルタイムクロック(RTC)について

**Raspberry pi**だけを使っている場合にはあまり意識しないが、**Raspberry pi**も**Arduino**も基本的にはパソコンなどでは標準で付いている**リアルタイムクロック**が付いていない。

**Raspberry pi**であまり問題にならないのはリアルタイムクロックの代わりに**NTP**サーバにアクセスして時刻を取得しているためである。当然ながら**NTP**にアクセスするにはインターネットにつながっている必要があり、そのような機能がないか、限られている**Arduino**では別途何らかの方策が必要になる。

そのために使われているのが、

- [VKLSVAN DS3231 AT24C32 IIC 高精度リアルタイムクロックモジュールArduino用](https://www.amazon.co.jp/gp/product/B01J5F5ORO/ref=oh_aui_detailpage_o01_s01?ie=UTF8&psc=1)
- [リアルタイムクロック(RTC)モジュール DS1307 EEPROM:AT24C32(32Kb)搭載](https://www.amazon.co.jp/gp/product/B00P51G026/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)

といった**RTC**と呼ばれるモジュールである。要するに単なる時計なのだが、**I2C**バスに接続して使用することができ、**DS3231**を使用したものは**Arduino**にタイマー割り込みを掛けることができるようになっている。

何らかの計測を行う場合、**時間**は非常に重要なファクタであり、時間が不正確では、測定データ自体が無意味になることすらある。またこれらの**RTC**にはバックアップ電池が搭載できるようになっており、**Arduino**の電源がOFFの場合にも時刻を保持することができる。

問題は、どうやって最初の時刻合わせを行うかだ。インターフェースは**I2C**しか無いわけだから、**I2C**経由で最初の時刻を設定しなければならないのだが、いちいち手入力で時刻合わせなど行いたくはない。

そこで、次のようなスケッチを使って時刻合わせを行うのが良いようだ。スケッチとは**Arduino**のプログラムのことで、言語仕様としては**C/C++言語**の組み込み版で`main()`からではなく、`setup()`と`loop()`関数があり、`setup()`は最初に一度だけ実行され、`loop()`は`setup()`の後に呼び出される。`loop()`は無限に呼び出されるようになっていて、プログラムを終了する方法はない。

```c
// CONNECTIONS:
// DS3231 SDA --> SDA
// DS3231 SCL --> SCL
// DS3231 VCC --> 3.3v or 5v
// DS3231 GND --> GND

/* for software wire use below
#include <SoftwareWire.h>  // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>

SoftwareWire myWire(SDA, SCL);
RtcDS3231<SoftwareWire> Rtc(myWire);
 for software wire use above */

/* for normal hardware wire use below */
#include <Wire.h> // must be included here so that Arduino library object file references work
#include <RtcDS3231.h>
RtcDS3231<TwoWire> Rtc(Wire);
/* for normal hardware wire use above */

void setup () {
    Serial.begin(9600);
    Serial.print("compiled: ");
    Serial.print(__DATE__);
    Serial.println(__TIME__);
    //--------RTC SETUP ------------
    // if you are using ESP-01 then uncomment the line below to reset the pins to
    // the available pins for SDA, SCL
    // Wire.begin(0, 2); // due to limited pins, use pin 0 and 2 for SDA, SCL
    Rtc.Begin();
    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__) + 18;
    printDateTime(compiled);
    Serial.println();
    if (!Rtc.IsDateTimeValid()) {
        // Common Cuases:
        //    1) first time you ran and the device wasn't running yet
        //    2) the battery on the device is low or even missing
        Serial.println("RTC lost confidence in the DateTime!");
        // following line sets the RTC to the date & time this sketch was compiled
        // it will also reset the valid flag internally unless the Rtc device is
        // having an issue
        Rtc.SetDateTime(compiled);
    }
    if(!Rtc.GetIsRunning()) {
        Serial.println("RTC was not actively running, starting now");
        Rtc.SetIsRunning(true);
    }
    RtcDateTime now = Rtc.GetDateTime();
    if(now < compiled) {
        Serial.println("RTC is older than compile time!  (Updating DateTime)");
        Rtc.SetDateTime(compiled);
    }
    else if (now > compiled) {
        Serial.println("RTC is newer than compile time. (this is expected)");
    }
    else if (now == compiled) {
        Serial.println("RTC is the same as compile time! (not expected but all is fine)");
    }
    // never assume the Rtc was last configured by you, so
    // just clear them to your needed state
    Rtc.Enable32kHzPin(false);
    Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}

void loop () {
    if (!Rtc.IsDateTimeValid()) {
        // Common Cuases:
        //    1) the battery on the device is low or even missing and the power line was disconnected
        Serial.println("RTC lost confidence in the DateTime!");
    }
    RtcDateTime now = Rtc.GetDateTime();
    printDateTime(now);
    Serial.println();
    RtcTemperature temp = Rtc.GetTemperature();
    Serial.print(temp.AsFloat());
    Serial.println("C");
    delay(10000); // ten seconds
}

#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt) {
    char datestring[20];
    snprintf_P(datestring,
            countof(datestring),
            PSTR("%04u/%02u/%02u %02u:%02u:%02u"),
            dt.Year(),
            dt.Month(),
            dt.Day(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    Serial.print(datestring);
}
```

`__DATE__`と`__TIME__`マクロを使用するのが**ミソ**である。このマクロは、スケッチのコンパイル時点の日付と時刻が入っていて、それぞれ、`"Sep 28 2017"`,`"17:18:52"`のような文字列に展開される。スケッチではこの文字列を日付、時刻として評価し、RTCの日付・時刻を設定する。コンパイル時点の日付・時刻(これはNTPで取得した時刻なので比較的正確)
なので、実際にプログラムを書き込んで動くまでの時間分ずれるので、その分をの秒数を加算して設定している。このスケッチを使うと２秒程度の誤差内で時刻の設定を行うことができる。

**Arduino**の言語については「[Arduino 日本語リファレンス](http://www.musashinodenpa.com/arduino/ref/)」があり、標準でどのような機能が使えるかがわかる。ただし特定のハードウエアを前提にしたものもあるので、全ての機能が最初から使えるわけではない。

上の、**RTC**で使われているライブラリも上記リファレンスには含まれていないので、ライブラリの解説やソースコードを見て使用方法を見つける必要がある。

上のスケッチで日付・時刻を合わせたら、次のスケッチをコンパイル・書き込みを行うと、一分に一回タイマー割り込みを行ってシリアルコンソールに日付時刻を表示する。

```c
//Reference :
//https://github.com/JChristensen/DS3232RTC
//http://forum.arduino.cc/index.php?topic=109062.0
//http://donalmorrissey.blogspot.jp/2010/04/sleeping-arduino-part-5-wake-up-via.html
//http://easylabo.com/2015/04/arduino/8357/

#include <DS3232RTC.h>    //http://github.com/JChristensen/DS3232RTC
#include <Time.h>         //http://www.arduino.cc/playground/Code/Time
#include <Wire.h>         //http://arduino.cc/en/Reference/Wire (included with Arduino IDE)
#include <avr/sleep.h>
#include <avr/power.h>

bool rtcint = false;

void setup(void) {
  //clear all
  RTC.alarmInterrupt(1, false);
  RTC.alarmInterrupt(2, false);
  RTC.oscStopped(true);

  //sync time with RTC module
  Serial.begin(9600);
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
  if(timeStatus() != timeSet) {
    Serial.println("Unable to sync with the RTC");
  } else {
    Serial.println("RTC has set the system time");
  }
  Serial.flush();

  //show current time, sync with 0 second
  digitalClockDisplay();
  synctozero();

  //set alarm to fire every minute
  RTC.alarm(2);
  attachInterrupt(1, alcall, FALLING);
  RTC.setAlarm(ALM2_EVERY_MINUTE , 0, 0, 0);
  RTC.alarmInterrupt(2, true);
  digitalClockDisplay();
}

void loop(void) {
  //process clock display and clear interrupt flag as needed
  if (rtcint) {
    rtcint = false;
    digitalClockDisplay();
    RTC.alarm(2);
  }

  //go to power save mode
  enterSleep();
}

void synctozero() {
  //wait until second reaches 0
  while (second() != 0) {
    delay(100);
  }
}

void alcall() {
  //per minute interrupt call
  rtcint = true;
}

void digitalClockDisplay(void) {
  // digital clock display of the time
  setSyncProvider(RTC.get); //sync time with RTC
  Serial.print(year());  Serial.print('/');
  printDigits(month());  Serial.print('/');
  printDigits(day());    Serial.print(' ');
  printDigits(hour());   Serial.print(':');
  printDigits(minute()); Serial.print(':');
  printDigits(second());
  Serial.println();
  Serial.flush();
}

void printDigits(int digits) {
  // utility function for digital clock display: prints preceding colon and leading 0
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void enterSleep(void) {
  //enter sleep mode to save power
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_mode();
  sleep_disable();
  power_all_enable();
}
```

このように、複数のスケッチを使い分けたりすることがあるので、**Raspberry pi**と**Arduino**が常時接続されていることは非常に重要になってくるのである。もし、常時接続でないとしたら、別の**Arduino**を使って時刻合わせを行った**RTC**を**外して**ターゲットの**Arduino**に接続するといった方策が必要になってしまう。

## ▼ GR-CITRUS & WA-MIKAN 入手

いろいろ情報をあさっていたら、[GR-CITRUS](http://akizukidenshi.com/catalog/g/gK-10281/)なるものを見つけた。なんと、マイコン上で**RUBYVM**が標準で動いていて**mruby/c**(組み込み向けruby処理系)でプログラムが作れる、ということで、価格も**2,400円**と比較的リーズナブルで、CPUも**Arduino**に比べると**96MHz**と高速。[WA-MIKAN](http://akizukidenshi.com/catalog/g/gK-10530/)(**1,500円**)と組み合わせるとWi-Fiも使えるという触れ込み。ピン配列は**Arduino Pro Mini**互換ということで**Arduino**の周辺デバイスが利用可能である。これは評価するしか無い、ということで早速ポチってしまった。

で先日ブツが届いたので早速評価してみた。

**上がGR-CITRUS下がWA-MIKAN**
![GR-CITRUS+WA-MIKAN](./img/img_0136.jpg)

使ってみた結果は、残念な結果であった(;_;)。

- mrubyを使ったときは**割り込み**が使えない。
- 消費電力が100mA以上と大きい。
- mruby実行時は**deep_sleep**などの省電力機能が使えない。
- **RTC**が付いているがバッテリーバックアップがない。
- 開発環境が**Arduino**互換の割味は微妙に使いにくい。
- そのままでは**PC**に接続しないと動作しない。
- 電源だけで動かすにはジャンパをショートさせる必要がある。
- ジャンパは**ハーフピッチ**(1.27mm)のものが必要。
- **ライブラリ**が揃っていない。

いろいろ書いたけど、mrubyが動くのは結構良いアプローチだと思うので、太陽電池で動かそうという目的にはあまり適していないが、目的によっては十分使える可能性があるので、もう少し使う方を考えてみたい。

## ▼ ESP8266の可能性

**GR-CITRUS**は少し残念な結果だったが、**WA-MIKAN**で使われている**ESP8266**は太陽電池駆動に非常に有用な機能を持っているので、少し深掘することにした。
このチップは、**Wi-Fi**アクセスと基本的な**TCPサーバ・クライアント**としての機能を持ち、**単独でArduino**として動作させることが可能である。

「[WROOM-02で温度／湿度計測　その１](http://okiraku-camera.tokyo/blog/?p=4738)」というブログによると、**Arduino IDE**を使ってプログラミング可能であり、いろいろ制限はあるものの、待機時には**deep_sleep**が使用可能で消費電力をかなり低く抑えることが可能なようだ。

また、**WA-MIKAN**自体も「[Arduino IDEでWA-MIKAN(和みかん)のESP8266をプログラミングする　環境インストール編](https://qiita.com/tarosay/items/28ba9e0208f41cec492d)」にあるように少し面倒だが、同様のプログラムを動作させることができるようだ。まあ価格を考えると積極的に**WA-MIKAN**を使うことはありえないのだが。

ということで、**WA-MIKAN**を流用するにしろ、新たに**WROOM-02**を使うにしろ**3.3V**に対応した**USBシリアル変換モジュール**は必要になるので、これまた購入することにした。購入したのは、

- [Wi-Fiモジュール ESP-WROOM-02 DIP化キット](http://akizukidenshi.com/catalog/g/gK-09758/)
- [FT231X USBシリアル変換モジュール](http://akizukidenshi.com/catalog/g/gK-06894/)

である(うーん、ブログのまんまだよなぁ)。これだけあれば、実際にESP-WROOM-02を使ってWi-Fi経由で測定を行うことが可能だ。

**ESP-WROOM-02とFT231X**
![ESP-WROOM-02とFT231X](./img/img_0138.jpg)

いつものようにamazonで安い中華ボードがないかどうか調べていたら、

**HiLetgo ESP8266 NodeMCU LUA CP2102 ESP-12E モノのインターネット開発ボード ESP8266 無線受信発信モジュール WIFI無ジュール Arduinoに適用 （2個セット） [並行輸入品]**
![HiLetgo ESP8266 NodeMCU LUA CP2102 ESP-12E モノのインターネット開発ボード ESP8266 無線受信発信モジュール WIFI無ジュール Arduinoに適用 （2個セット） [並行輸入品]](./img/img_0140.jpg)

と言うものを見つけてポチッた。ポチったときはあまり気にしていなかったのだが、商品名の中の「**NodeMCU LUA**」というのがなかなかの曲者であった。これについては後述する。

これで低消費電力で測定ができるようであれば、**N-MOSFET**を使った電源スイッチを付加して**PIX-MT100**の電源もコントロールすることができれば完璧である。と目論見道理になるかどうかはやってみなけりゃわからないので、とりあえずやってみる。

## ▼ 「NodeMCU LUA」とは何か？

ポチったときには気にしていなかった「**NodeMCU LUA**」とは何か？調べてみたら、意外ととんでもないものであった。「**NodeMCU**」でググると、

- [ESP8266](https://www.wikiwand.com/ja/ESP8266)
- [ESP8266にNodeMCUをアップロードしてHello Worldする](https://qiita.com/masato/items/a91e0df84ac8be45e305)
- [ESP8266 上で Lua インタプリタが動くファームウェア、NodeMCU を使う](http://embedded.hateblo.jp/entry/2016/09/16/192333)

といった情報が引っかかってくる。**nodeMCU**は**ESP8266**上で動作している**luaVM**であり、**luaスクリプト**で書かれたプログラムを動作させる事ができる。**GR-CITRUS**の上で**mrubyスクリプト**が動くのと同じようなものである。**mruby**と違う点は、

- 言語仕様がmrubyよりも軽そうである
- deep_sleepのようなハードよりのライブラリがある
- Wi-Fi,htttp,mDNS,sntpなど通信関係のライブラリが豊富にある
- 用途に適したカスタムVMが作成・ダウンロードできるサイトがある

など、mrubyよりも開発環境は整備されていると言えそうだ。ただ、日本では**luaインタプリタ**はあまり普及しているとは言い難く、日本語で検索できた情報には「**Hello World**」程度で終わっており、情報が多いとは言い難い(まあmrubyも豊富とは言い難いのだが)。

また、開発環境の多くが**python**で書かれ、**linux**上で開発することを前提としているものが多く、windowsでの情報が少ないため、mruby同様、実際に開発を行うには超えなければならないハードルが多く存在する。しかしながら、**Arduino IDE**のような完成度の開発環境が出てくれば、相当使えそうな感じである。

## ▼ ESP8266でWebサーバを動かす

Amazonで買ったボードは**nodeMCU**がプリインストールされているというだけで、物は**ESP8266**モジュールを積んだワンボードマイコンである。なので、当然**Arduino IDE**を使ってプログラムを作って動かすことができる。そこで、次のようなスケッチを書き込んで動かしてみた。

```C++
/*
  ESP8266 mDNS responder sample

  This is an example of an HTTP server that is accessible
  via http://esp8266.local URL thanks to mDNS responder.

  Instructions:
  - Update WiFi SSID and password as necessary.
  - Flash the sketch to the ESP8266 board
  - Install host software:
    - For Linux, install Avahi (http://avahi.org/).
    - For Windows, install Bonjour (http://www.apple.com/support/bonjour/).
    - For Mac OSX and iOS support is built in through Bonjour already.
  - Point your browser to http://esp8266.local, you should see a response.

 */


#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>

#define LED 16

const char* ssid = "<ssid>";
const char* password = "<password>";

// TCP server at port 80 will respond to HTTP requests
WiFiServer server(80);

void setup(void) {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  Serial.begin(115200);
  // Connect to WiFi network
  WiFi.begin(ssid, password);
  Serial.println("");  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  // Set up mDNS responder:
  // - first argument is the domain name, in this example
  //   the fully-qualified domain name is "esp8266.local"
  // - second argument is the IP address to advertise
  //   we send our IP address on the WiFi network
  if(!MDNS.begin("esp8266")) {
    Serial.println("Error setting up MDNS responder!");
    while(1) { 
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  // Start TCP (HTTP) server
  server.begin();
  Serial.println("TCP server started");
  // Add service to MDNS-SD
  MDNS.addService("http","tcp",80);
}

void loop(void) {
  // Check if a client has connected
  WiFiClient client = server.available();
  if(!client) {
    return;
  }
  Serial.println("");
  Serial.println("New client");
  // Wait for data from client to become available
  while(client.connected() && !client.available()) {
    delay(1);
  }
  // Read the first line of HTTP request
  String req = client.readStringUntil('\r');
  // First line of HTTP request looks like "GET /path HTTP/1.1"
  // Retrieve the "/path" part by finding the spaces
  int addr_start = req.indexOf(' ');
  int addr_end = req.indexOf(' ', addr_start + 1);
  if (addr_start == -1 || addr_end == -1) {
    Serial.print("Invalid request: ");
    Serial.println(req);
    return;
  }
  digitalWrite(LED, LOW);
  req = req.substring(addr_start + 1, addr_end);
  Serial.print("Request: ");
  Serial.println(req);
  client.flush();
  String s;
  if(req == "/") {
    IPAddress ip = WiFi.localIP();
    String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
    s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>Hello from ESP8266 at ";
    s += ipStr;
    s += "</html>\r\n\r\n";
    Serial.println("Sending 200");
  } else {
    s = "HTTP/1.1 404 Not Found\r\n\r\n";
    Serial.println("Sending 404");
  }
  client.print(s);
  digitalWrite(LED, HIGH);
  Serial.println("Done with client");
}
```

Webサーバとして動くのはもちろんだが、**mDNS**も一緒に動かしているので、IPアドレス指定ではなく、

```
http://esp8266.local/
```

のように名前でアクセスすることができる。この時の消費電力を測定してみると、**80mA**から**90mA**程度と常時電源が入っている状態としては低消費電力だといえる。

また、ping応答を見てみると、
```DOS
C:\Users\ashig>ping esp8266.local

esp8266.local [192.168.254.182]に ping を送信しています 32 バイトのデータ:
192.168.254.182 からの応答: バイト数 =32 時間 =2ms TTL=128
192.168.254.182 からの応答: バイト数 =32 時間 =3ms TTL=128
192.168.254.182 からの応答: バイト数 =32 時間 =24ms TTL=128
192.168.254.182 からの応答: バイト数 =32 時間 =1ms TTL=128

192.168.254.182 の ping 統計:
    パケット数: 送信 = 4、受信 = 4、損失 = 0 (0% の損失)、
ラウンド トリップの概算時間 (ミリ秒):
    最小 = 1ms、最大 = 24ms、平均 = 7ms

C:\Users\ashig>
```
のように、Wi-Fi経由でも数msのラウンドトリップタイムで結構早い印象である。

## ▼ ESP8266でWeb電圧計を作る

単純にwebサーバを作っただけではつまらないので、これに**I2C**の電流・電圧・電力計を接続してweb上で測定できるようにした。

![web電流・電圧・電力計](./img/img_0141.jpg)

[http://esp8266.local/](http://esp8266.local/)にアクセスすると、

```text
Web-meter by ESP8266 at 192.168.254.182
電圧: 0.00mV
電流: 0.00mA
電力: 0.00mW
```

のようなページを表示する。ここでは入力端子に何も接続していないので、全て**0.00**であるが、電源を接続すれば、その電圧や電流を表示する。詳細は[ソースコード](https://bitbucket.org/sd21y/rpi-iot/src/110be0aa9b2d50a7e8702c3544c806a23b3c41c2/Arduino/web-meter/?at=master)を見てほしい。

## ▼ ESP8266の省電力動作

![省電力テストボード](./img/img_0145.jpg)

上の写真のような簡単な回路でESP8266の**deep_sleep**のテストを行った。

回路的には**D0**ピンと**RST**ピンを**1KΩ**の抵抗を介して接続しているところがミソである。このような回路にすると、**deep_sleep**が使えるようになり、**deep_sleep**中は内臓のリアルタイムクロック以外の電源が落とされ、論理的には数μAレベルの消費電力になる。実際にはUSB周りの回路が生きていたりするため、数mAレベルだと思われるが、それでも定常動作時の100mAレベルに比べれば、1/10以下の消費電力に抑えることができる。また、USBからの電源供給ではなく、**Vin**ピンもしくは**3V3**ピンから電源を供給するようにすれば、もう少し論理値に近い消費電力にすることが可能ではないかと思われる。**deep_sleep**ができることは、次のような**sketch**で動作の確認を行った。

```c++
#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>

// LED port
#define LED_PIN D8
#define RST_PIN 16

// pre-wakeup time
#define LTE_WAKEUP 60
#define PRE_WAKEUP 20
// HTTP
const char* httpServerIP = "192.168.254.7";
// Wi-Fi
const char* wifi_ssid   = "<ssid>";
const char* wifi_passwd = "<password>";
// NTP
unsigned int localPort = 2390;      // local port to listen for UDP packets
const char* ntpServerName = "ntp.nict.jp";
IPAddress timeServerIP;            // time.nist.gov NTP server address
const int NTP_PACKET_SIZE = 48;      // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP udp;                         // A UDP instance to let us send and receive packets over UDP

void WiFiconnect(void) {
int count = 0;
  Serial.print("Connecting Wi-Fi ");
  WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid,wifi_passwd);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    count ++;
    if(count > 20) {
      pinMode(RST_PIN,OUTPUT);
      digitalWrite(RST_PIN,LOW);
    }
  }
  Serial.println();
  Serial.print("Connected to ");
  Serial.println(wifi_ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

unsigned long sendNTPpacket(IPAddress& address) {
  Serial.println("Sending NTP packet ...");
  memset(packetBuffer,0,NTP_PACKET_SIZE); // set all bytes in the buffer to 0
  // Initialize values needed to form NTP request
  packetBuffer[ 0] = 0b11100011;           // LI, Version, Mode
  packetBuffer[ 1] = 0;                    // Stratum, or type of clock
  packetBuffer[ 2] = 6;                    // Polling Interval
  packetBuffer[ 3] = 0xEC;                 // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123);          // NTP requests are to port 123
  udp.write(packetBuffer,NTP_PACKET_SIZE);
  udp.endPacket();
}

time_t getNTPtime() {
  udp.begin(localPort);
  Serial.print("UDP local port: ");
  Serial.println(udp.localPort());
  WiFi.hostByName(ntpServerName,timeServerIP); 
  sendNTPpacket(timeServerIP);   // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no packet yet");
    return(0);
  } else {
    Serial.print("Packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = " );
    Serial.println(secsSince1900);
    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    // print Unix time:
    Serial.println(epoch);
    epoch += 3600 * 9; // Add JST offset
    return(epoch);
  }
}

time_t getNTP() {
unsigned long epoch = 0;
  while(epoch == 0) {
    epoch = getNTPtime();
    if(epoch != 0) break;
    delay(1000);
    Serial.println("Retry ...");
  }
  return(epoch);
}

char* mysPrintf(char* buf,int len,char *fmt, ...) {
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,len,fmt,args);
  va_end(args);
  return(buf);
}

void myPrintf(char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,128,fmt,args);
  va_end(args);
  Serial.print(buf);
}

void myPrintfln(char *fmt, ...) {
  char buf[128];
  va_list args;
  va_start(args,fmt);
  vsnprintf(buf,128,fmt,args);
  va_end(args);
  Serial.println(buf);
}

void measure(time_t left) {
HTTPClient http;
char buffer[256];
int httpCode;
  mysPrintf(buffer,256,"http://%s/cgi/esp8266.cgi?t=%04d/%02d/%02dT%02d:%02d:%02d&l=%d",httpServerIP,year(),month(),day(),hour(),minute(),second(),left);
  http.begin(buffer);
  Serial.printf("HTTP GET request: %s\n",buffer);
  httpCode = http.GET();
  if(httpCode > 0) {
    // HTTP header has been send and Server response header has been handled
    Serial.printf("HTTP GET response: %d\n",httpCode);
    if(httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      Serial.println(payload);
    }
  } else {
    Serial.printf("HTTP GET faiLED_PIN, error: %s\n",http.errorToString(httpCode).c_str());
  }
  http.end();
}

void setup() {
unsigned long start,measure_time,sleep_time,error_time;
time_t left;
  // wakeup indicator
  pinMode(LED_PIN,OUTPUT);
  digitalWrite(LED_PIN,HIGH);
  //デバッグ用にシリアルを開く
  Serial.begin(115200);
  Serial.println();
  Serial.print("Reboot reason: ");
  Serial.println(ESP.getResetReason());
  // Wait for LTE dongle starting
  Serial.println("Wait LTE dongle starting ...");
  delay(LTE_WAKEUP * 1000);
  // Start
  WiFiconnect();
  setSyncProvider(getNTP);
  setSyncInterval(300);
  myPrintfln("Wakeup: %04d/%02d/%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),second());
  // measure(0);
  // xx:00:00まで待機
  left = (hour() + 1) * 60 * 60 - ((hour() * 60 * 60) + (minute() * 60) + second());
  if(left > (PRE_WAKEUP + LTE_WAKEUP)) {
    myPrintfln("Short deep sleep %d sec ...",left - (PRE_WAKEUP + LTE_WAKEUP));
    digitalWrite(LED_PIN,LOW);
    ESP.deepSleep((left - (PRE_WAKEUP + LTE_WAKEUP)) * 1000 * 1000,WAKE_RF_DEFAULT);
    delay(1000); // deepsleepモード移行までのダミー命令
  } else {
    myPrintf("Waiting %d sec at %02d:00:00 ...",left,hour());
    while(minute() != 0 || second() != 0) {
      delay(500);
      Serial.print(".");
    }
    Serial.println();
  }
  start = millis();
  myPrintfln("Measure start: %04d/%02d/%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),second());
  measure(left);
  measure_time = (millis() - start) / 1000;
  myPrintfln("Measure time: %d sec ...",measure_time);
  digitalWrite(LED_PIN,LOW);
  myPrintfln("Measure end: %04d/%02d/%02d %02d:%02d:%02d",year(),month(),day(),hour(),minute(),second());
  sleep_time = 60 * 60 - (measure_time + PRE_WAKEUP + LTE_WAKEUP);
  myPrintfln("Powerdown deep sleep %d sec ...",sleep_time);
  ESP.deepSleep(sleep_time * 1000 * 1000,WAKE_RF_DEFAULT);
  delay(1000); // deepsleepモード移行までのダミー命令
}

void loop() {}
```

この**sketch**では大まかに、

- LTEドングルの起動待ち(60秒)
- Wi-Fi接続を開始する
- NTPサーバから時刻を取得し、Timerに設定
- xx:00:00までdeep_sleepまたはループで待機
- 測定(今は何もしていない)
- 次のタイミングを計算し、deep_sleepを実行

ということを行っている。

**deep_sleep**は、指定の時間が経過するまで省電力モードになり、指定の時刻になったら**D0**ピンを**LOW**にする。**D0**ピンは**RST**ピンにつながれているので**ESP8266**が**リセット**される、という**かなり乱暴な**スリープモードである。

```text
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718315739
Unix time = 1509326939
Wakeup: 2017/10/30 10:28:59
Short deep sleep 1791 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718317444
Unix time = 1509328644
Wakeup: 2017/10/30 10:57:24
Short deep sleep 86 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718317586
Unix time = 1509328786
Wakeup: 2017/10/30 10:59:46
Waiting 14 sec at 10:00:00 ...............................
Measure start: 2017/10/30 11:00:00
HTTP GET request: http://192.168.254.7/cgi/esp8266.cgi?t=2017/10/30T11:00:00&l=14
HTTP GET response: 200
DONE
Measure time: 0 sec ...
Measure end: 2017/10/30 11:00:00
Powerdown deep sleep 3530 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi .....................
<...文字化け...>
Reboot reason: External System
Wait LTE dongle starting ...
Connecting Wi-Fi .....................
Reboot reason: External System
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718321041
Unix time = 1509332241
Wakeup: 2017/10/30 11:57:21
Short deep sleep 89 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718321186
Unix time = 1509332386
Wakeup: 2017/10/30 11:59:46
Waiting 14 sec at 11:00:00 ...............................
Measure start: 2017/10/30 12:00:00
HTTP GET request: http://192.168.254.7/cgi/esp8266.cgi?t=2017/10/30T12:00:00&l=14
HTTP GET response: 200
DONE
Measure time: 0 sec ...
Measure end: 2017/10/30 12:00:00
Powerdown deep sleep 3530 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718324486
Unix time = 1509335686
Wakeup: 2017/10/30 12:54:46
Short deep sleep 244 sec ...
<...文字化け...>
Reboot reason: Deep-Sleep Wake
Wait LTE dongle starting ...
Connecting Wi-Fi 
Connected to 106F3FDBCFA9
IP address: 192.168.254.188
UDP local port: 2390
Sending NTP packet ...
Packet received, length=48
Seconds since Jan 1 1900 = 3718324775
Unix time = 1509335975
Wakeup: 2017/10/30 12:59:35
Waiting 25 sec at 12:00:00 .....................................................
Measure start: 2017/10/30 13:00:00
HTTP GET request: http://192.168.254.7/cgi/esp8266.cgi?t=2017/10/30T13:00:00&l=25
HTTP GET response: 200
DONE
Measure time: 0 sec ...
Measure end: 2017/10/30 13:00:00
Powerdown deep sleep 3530 sec ...
```

実際に動かすと上記のように59分(3540秒)スリープするはずが、54～55分程度の時間経過すると起きてしまい、時間調整用の短時間の**deep_sleep**が実行されるため、実際の計測を行うために２回**deep_sleep(=リセット)**が実行されてしまう。消費電力的にはあまりよくないのだが、deep_sleepの精度が悪いので、この方法が一番安定して動作する可能性がある。

複雑なOSがないシステムの利点はこのように強制的なリセットを行っても大丈夫なところにある。**Raspberry Pi**ではこれを安全に行うために色々工夫はしているが、起動途中で電源が落ちるというような事態になると**SDカード**が破損したり、最悪ボード自体が壊れる可能性がある。Arduinoではそのような事態になる心配は少ない。

この回路に、ニッケル水素電池x4の電源を**Vin**に供給して**INA226**を使った電流・電圧計で測定したところ、

| 条件      | 状態       | Vin電圧 | Vin電流 |
| ------- | -------- | ----- | ----- |
| USB接続あり | Wi-Fi通信時 | 5.57V | 80mA  |
| USB接続あり | 待機時      | 5.57V | 10mA  |
| USB接続なし | Wi-Fi通信時 | 5.57V | 70mA  |
| USB接続なし | 待機時      | 5.57V | 3mA   |

という結果になった。論理値のμAレベルにはならなかったものの、USB接続なしの状態では待機時の電流が3mAとかなり省電力になっていることがわかる。待機時の電流・電圧を次に示す。

![待機時の消費電流](./img/img_0146.jpg)

どうやら**NodeMCU**を使った構成での低消費電力化はこのあたりが限界のようだ。

![img_0147](./img/img_0147.jpg)

ちなみに、0.9Vから3.3Vに昇圧可能なDC-DCコンバータを接続してニッケル水素電池２本で動かしてみたのが上の写真である。この状態でどのくらい電池が持つかを現在実験中である。

で、ニッケル水素電池でだいたい３日くらいは動作するようだ。こんなもんかと思いつつ、シリアル接続用の回路が違えば、多少消費電力も変わるのではないかと思い、別の[ESR8266ボード](https://www.switch-science.com/catalog/2500/)でも試してみたのだが、

```text
Web-meter by ESP8266
MyIP: 192.168.254.182
日時: 2017/11/08 17:32:53
気温: DS18B20: 27.13℃,RTC: 27.75℃
電圧: 8101.25mV
電流: 4.00mA
電力: 50.00mW
```

ということで、全く同じ消費電力であった。これは別のWeb上で電圧・電流・消費電力を測定できるように作成した、ESP8266ボードで測定した。

## ▼ 電圧・電流・消費電力計

**NodeMCU**の電池駆動では消費電力の削減が重要であるが、この時、地味に役に立ったのが、**Arduino nano**に**LCD**(I2C接続のもの)をつけて作った電圧・電流・消費電力計である。上の写真の表示がそうなのだが、最初は単なる時計だったのだが、それに**INA226**を使った電圧・電流・電力を表示できるようにしたものだ。消費電力を常時モニタしたいとなれば、テスターではとても無理だし、最近の安物のテスターは電流が計測できなかったりするので、まずひとつ、このようなものを作っておくと動作検証がしやすくなる。

また、Web上で見ることができるものも作成しておいたので、上記のスイッチサイエンスのボードの消費電力測定に使用した。これもこういった動作確認には便利である。

## ▼ ESP32

ESP8266はリーズナブルで使いやすいのだが、その後継機として[ESP32](https://www.wikiwand.com/ja/ESP32)というチップが出ている。機能的にもかなり強化され、クロックも160MHzもしくは240MHzと高速になっている(低消費電力の観点からするとオーバースペックのような気もするが)。とりあえず、ESP8266同様、[開発ボード](https://www.amazon.co.jp/HiLetgo-ESP32-ESP-32S-NodeMCU%E9%96%8B%E7%99%BA%E3%83%9C%E3%83%BC%E3%83%892-4GHz-Bluetooth%E3%83%87%E3%83%A5%E3%82%A2%E3%83%AB%E3%83%A2%E3%83%BC%E3%83%89/dp/B0718T232Z/ref=pd_)が売られているので入手してみた。実は、現在販売されているESP8266の大部分は[技適](https://www.wikiwand.com/ja/%E6%8A%80%E9%81%A9%E3%83%9E%E3%83%BC%E3%82%AF)を受けていないが、ESP32は技適を受けているので、違法かどうかを気にする必要がない。技適自体、Wi-Fiではほとんど形式的なものになっていて非関税障壁と化しているものの、技適マークがないと形式的には**違法**である。

## ▼ ブレッドボード

![ブレッドボード](./img/img20171111_000046.jpg)

説明していなかったが、こういった実験にはブレッドボードが便利でよく使っている。上の写真のようにブレッドボードは穴が金属のコネクタでつながっていて、同じ列にピンを刺すと電気的に接続できるようになっている。なので、間違って同じコネクタの列に電源などを接続してしまうと、簡単にショートしてしまうので、回路を構成する際にはよく注意して接続しなければならない。

## ▼ 抵抗

![抵抗その１](./img/img20171111_000025.jpg)

抵抗をブレッドボードに刺す場合、足が長くかつ細いため、扱いに困る場合が多い。何かいい方法がないかと思い考えたのがこれ。圧着端子に抵抗を付けてしまえば、抜き差しもしやすく縦型になるので場所も取らない。最大の欠点は作るのが面倒であることだ。あと何かラベルを貼るなりしておかないと抵抗値がわかりにくい。もっといい方法はないものか思案中である。

![抵抗その２](./img/img_0022.jpg)

最初の方法だと、作ってしまうと抵抗の交換ができないので使い回しができない。そこで、一列タイプのICソケットを２ピンずつ切り離して使う方法を考えた。ただ、普通に売られているソケットはピンの長さが短く、ブレッドボードの端子まで届かない。そこで、ICソケットと同じタイプのピンだけ売っていたので、それを継ぎ足してブレッドボードに刺せるようにした。この方法だと、抵抗の交換が簡単にできるので必要な抵抗を付け替えて使用できる。

## ▼ NodeMCU-32Sを使った測定回路

**ブレッドボード構成**

![esp32ブレッドボード](./img/img_0016.jpg)

**回路図**

![esp32回路図](./img/nodeMCU32S.jpg)

[NodeMCU-32S](https://www.amazon.co.jp/HiLetgo-ESP32-ESP-32S-NodeMCU%E9%96%8B%E7%99%BA%E3%83%9C%E3%83%BC%E3%83%892-4GHz-Bluetooth%E3%83%87%E3%83%A5%E3%82%A2%E3%83%AB%E3%83%A2%E3%83%BC%E3%83%89/dp/B0718T232Z)を使った測定回路である。[本体・計測系](./iot-device.md)の**Raspberry -pi zero W**を使ったものと同等の測定を**NodeMCU-32S**で実現しようというものである。

消費電力を削減するため、NodeMCU-32Sは降圧型DC-DCコンバータを使って3.3Vの電源で駆動している。スタンバイ電流の小さなDC-DCコンバータを選べば、deep-sleep時の消費電力を**8V時,1mA**程度に抑えることができ、電池の消耗を抑えることが可能である。

**PIX-MT100**は**5V**の電源が必要になるので、5V用のDC-DCコンバータを別途用意し、必要なときだけ電力を供給できるように**N-ch MOS FET**で電源を制御するようにしている。回路的には5V単一電源だけでも構成可能なのだが、deep-sleep時の消費電力が大きくなってしまう(**8V時,9mA程度**)ため個別に電源を供給するようにしている。

**Raspberry pi zero W**で構成したシステムと比較すると、

- **安価である**：**Raspberry pi zero W**と比べると半分以下の価格で構成することが可能である。専用の基板等を作成して、量産を行えるようなら、コストは**1/3**から**1/4**にすることが可能ではないかと思われる。
- **安定性が高い**：**Raspberry pi zero W**は、小さいとは言えフルの**Linux**が動作しているため、起動するまでには数多くのソフトウエアが介在する。**NodeMCU-32S**の場合、ブートローダーから直接起動されるので非常にシンプルな構成にすることができる。そのためソフトウエア的な安定性は圧倒的に**NodeMCU-32S**の方が安定的である。
- **低消費電力**：**Raspberry pi zero W**では待機時の消費電力が**10mA**程度あり、電池駆動の場合は決して無視できるものではなかったが、**NodeMCU-32S**では**1mA**と小さく、同じ電池容量であれば駆動時間が数倍から１０倍程度まで伸ばせることを意味する。
- **電池駆動に適している**：消費電力が少ないため**NodeMCU-32S**は、電池駆動に適しているが、**NodeMCU-32S**は電源電圧が**2.2V**程度まで動作可能である。上記の回路は**8V**から**3.3V**に降圧して電源を供給しているが、これを**乾電池２本**直列**(3V)**で動作させることも可能である。
  **PIX-MT100**を駆動する必要がなければ、乾電池で駆動することも十分可能である。

と言った点があげられる。

また、内蔵する**ADC**を使用して電池の電源電圧を測定し、電源電圧を測定できるようにしている。これによって電池の過放電を防止できるようになる。

## ▼ だめだった(!_!)けど成功！！

上記の構成は回路的には正しいのだが、一つ重大なことを見逃していた。**PIX-MT100**を駆動するFETが不適合だったのだ。

どういうことかというと、一般的にFETをフルにスイッチさせるためにはゲート電圧が**4V以上**必要だったのだ。**NodeMCU-32S**のI/Oコントロールの出力電圧は3.3VなのでFETをフルにON状態にすることができなかったのだ。そのためFET抵抗値が大きく、結果的に**PIX-MT100**に5Vの電圧を付加することができなかったのである。

探してみると、2V程度でON/OFFできるFETもあることはあるようなのだが結構入手が困難であることや3.3V系と5V系を混在させるのはやはりまずいだろうということで、フォトリレーを使ったらどうかと考えた。電源系を分離しフォトリレー経由でFETをスイッチすれば信号系の電圧不足は解決するはず…ということで東芝の**TLP222A**の手持ち品があったのでLEDを駆動するだけの回路に置き換えてテストしてみた。

もちろん、LED程度の電流であればバッチリ成功である。と、ここで**TLP222A**の回路を見ると出力側はFETが使われていることに気がついた。うーみゅ、もしかするとダイレクトに5V系のON/OFFできるんじゃないか？と思ったが**TLP222A**の許容電流は500mAと小さい。ということで、もう少し大電流を流すことができるフォトリレーはないのか？あれば回路が非常に簡単になる。

で探し回ったら、ありました。同じく東芝の**TLP241A**というやつ。これは**2A**まで流せるということでなんとか**PIX-MT100**を駆動できそうである。秋月で扱っていたので速攻ゲットし、**TLP222A**と差し替えてテストしてみた。結果、なんとか**PIX-MT100**を駆動できているようである。

![TLP222Aを使ったテスト回路](img/IMG_0061.jpg)

![実際のテスト回路](img/IMG_0063.jpeg)

上の写真が一間で使用したブレッドボード全体。基本的には以前の回路と同じだが、プログラムの変更で内蔵プルアップ抵抗をONにすることでBME280のプルアップ抵抗を省略している。また、電源供給は太陽電池は使用せず、リチウムイオンバッテリーを使用したUSB電源装置(アマゾンで安価に入手したもの)を使用している。また電源安定化の効果を狙って少し大きめの電解コンデンサーをバイパスコンデンサーとして電源系に接続している。

**回路図**

![timersampler_TLP241A](img/timersampler_TLP241A-1572976630669.png)

この回路でしばらく様子を見てみることにした。

## ▼ ブレッドボードから基板化

しばらく上記の回路で検証し十分安定して動作することを確認した。そこで、ブレッドボードではなくきちんとした基板上に回路を作り直すことにした。基板化するには本当ならプリント基板を作るのがベストなのだが、手間がかかるのでお手軽に**ユニバーサル基板**を使うことにした。

ユニバーサル基板にも色々なものがあるのだが、ブレッドボードから基板にするには、ブレッドボードと同じ配線パターンをもつユニバーサル基板を使用るのが一番簡単なようだ。

![IMG_0072](img/IMG_0072.jpg)

このように同じパターンになっているので、ブレッドボードから基板を作るのに適している。

![IMG_0073](img/IMG_0073.jpg)

実際に作成したのが上の写真のような基板である。NodeMCU32SとBME280センサーは後々他の用途に転用できるようにソケットを使用している。

配線はすべて表面側で行っているので、NodeMCU32Sを外すと回路の接続状況がよく分かるようにしてある。

![IMG_0071](img/IMG_0071.jpg)

![IMG_0077](img/IMG_0077.jpg)

上記写真はセンサーモジュールの違う別のバージョン。こちらのほうがセンサーのピン数が多いのでギリギリの実装になっている。

ESP32を使用したモジュールは多くの場合**19ピン**分の長さがあるので、ブレッドボード上で使える自由なエリアは**11ピン**分しかない。今回の場合のようにコネクタなどをつけてしまうと本当に自由に使えるエリアがなくなってしまう。上の例では余っている配線パターンは1列しか残っていない。

調べてみると、少数ではあるが**15ピン**のモジュールも売られているようなので、センサー部分の面積が必要な場合は、このような小さめのモジュールを選ぶ必要があると思う。

**15ピンのモジュール**

![IMG_0083](img/IMG_0083.jpg)

また、センサーもできる限り無駄なピンが出ていないものを使う必要があると思う。それでも足りない場合は、基板を２枚、３枚と連結して使用することも考えなければならない(どう結合するかがまた問題ではあるが)。

![IMG_0084](img/IMG_0084.jpg)

写真のBME280センサーは必要最小限の**SDA**,**SCL**,**GND**,**VIN**の４ピンのみが出ている。

**[BME280センサーの回路図]**

![bme280](img/bme280.jpg)

Amazonの商品ページには回路図が載っていたので見てみると、標準的には**5V**系で使用するようになっているらしいが、VINに**3.3V**を印加した場合は、3.3V及びBVCCラインには3.3Vよりも少し低い電圧がかかる(はず)。BME280のデータシートを見ると、**VDD:1.71V~3.6V**、**VIO:1.2V~3.6V**となっているので、BVCCが**3V**程度でも正常に動作するはずである。

**BME280モジュールの裏面**

![IMG_0085](img/IMG_0085.jpg)

使用している三端子レギュレータ(662Kという型番が確認できる)のデータシートを見つけたので確認すると、**3.3V**入力の場合**250mV**のドロップアウトで、出力側には約**3V**の電圧が出るようだ。

## ▼ どうやって固定するか？

スグレモノのサンハヤトのユニバーサル基板なのだが一つ大きな欠点がある。基板を固定するための**ネジ穴がない**のだ。別の5穴タイプのユニバーサル基板には写真のように一応ネジ穴がある。

![IMG_0079](img/IMG_0079.jpg)

また別のタイプのユニバーサル基板はスルーホールになっていてネジ穴も６つある(まあ穴が小さいのでこれはこれで固定に苦労しそうだけど)。スルーホールになっているのは工夫すれば配線しやすそうだ。

![IMG_0081](img/IMG_0081.jpg)

そこでなんとか探しだしたのが下写真のような基板のサイドを挟み込むタイプのスペーサーである。これで数箇所固定すれば基板に穴を開けたりすることなく固定することが可能だ。裏に両面テープが貼ってあるので好きな場所に貼り付けることができる。

少し残念なのが1.6mmまでの基板を挟むことができるようになっているが、サンハヤトの基板の厚さが1.0mmなので挟み込む部分がユルユルで外れやすい、基板を乗せる丸い足の部分があるので、はんだ付け部分に引っかからないところで固定しなければならないなど、少し使い方が難しい。

![IMG_0080](img/IMG_0080.jpg)

## ▼ 実際に屋外に設置してみた

ケースを加工し、センサーをケース外に引き出せるようにして実際に屋外に設置してみた。ところが、屋内では正常に測定できるのに屋外に設置すると数回測定したあと、測定できなくなってしまう。屋内に持ってくると、また正常に動作する。

うーん、何だー？

- Wi-Fiの電波が弱いのか？
- センサーのワイヤーが長いのでノイズが乗っているのか？
- もしかして電源にノイズが乗っているとか？

などなど、色々対策を行ってみたのだが全く症状が変わらない。

で、どうも**センサーが部分的に異常を起こしている**可能性にぶち当たった。別のセンサーに変えてみると正常に動作している。どうもセンサーの**温度が５度以下になると正常に動作しなくなる**という、妙な壊れ方をしているようだ。まあ、アマゾンで買った安物のBME280モジュールなので不良品のハネモノを売っていたとしても不思議ではない。

うーむ、なんともややっこしい状況を経験してしまった。

それでも、屋外用のNodeNCU-32Sが時々ハングアップすることがあり、Wi-Fiの見直しや(RSSIと呼ばれるWi-Fiの信号強度の測定項目の追加)、ソフトウエアの修正(Wi-Fi,NTPのリトライ方法の変更、Watchdogタイマーの追加)などを行ったのだが、どうやっても改善しないと思ったら、やはりこれもNodeMCU-32Sの不良だったようだ。現在、別のモジュールに交換して動作状況を見ている。

確実ではないが、どうもcore温度が高めのESP32は故障の可能性があるようだ。動作の良好なESP32のCore温度が30℃～40℃程度なのに対し、動作不良のものは50℃～60℃になるようだ。が、最近入手したNodeMCU-32Sは２つとも60℃程度になっているのでステッピングによって異なる可能性がある。

## ▼ やっぱりノイズ？

このような対策を行ったにもかかわらず、測定ができなくなる症状は少しは改善したが、まだ時々リセットされる症状が出る。で第二の原因候補としてノイズの可能性を考えてみたい。探ってみると測定インターフェースとして使用している**I2C**は明確な使用可能なケーブル長が定義されていないらしい。つまり配線やプルアップ抵抗その他の条件によって測定可能距離は変化し、**Your own risk**で使えということらしい。

ということで、i2Cの配線を単なる４線ケーブルからツイストペアケーブル(なんのことはない薄型LANケーブル=**きしめんケーブル**のこと)に変更してみた。基本的にはLANケーブルを窓枠から外に配線し、両端にLAN中継コネクタをつけてそこにばらしたLANケーブルを介してI2Cインターフェースにつなぐというもの。ケーブル長は少し長くはなるもののI2Cのコネクタ部分と中継コネクタ部分を除いてツイストペア状態になっているはずである。とりあえずこの状態で様子を見ている。

## ▼ 主原因は起動までのタイミングだった

色々試していたとき、もしかすると起動したときすでに測定タイミングを過ぎていることがあり、測定を空振りしているのではないかと思いあたった。そこで、もう少し早い時間にディープスリープから復帰するように設定を変更してみた。これがほぼ大当たりで、測定値が抜ける頻度が激減した。

Wi-Fiの状態によってはまだ抜けることはあるものの、頻度は少ないので、それほど支障にはならない程度には減ったのでとりあえずこれで良しとする。

## ▼ NTPも原因の一つ

また、測定のタイミングを合わせるため、ESP32から**NTPサーバ**に時刻の問い合わせを頻繁に行っている。**sgmail.jp**でNTPサーバを立てて、これを使用していたのだが、どうやら［**Wi-Fi→ソネット→sgmail.jp**］のNTPサーバアクセスが結構な頻度で届かない状態になるようだ。そのため、NTPサーバアクセスはリトライしているのだが、どうも長時間アクセスできない状態になっているようである。そのため、ローカルにNTPサーバーを立てて使用するようにした。

LTE経由のものについては、**sgmail.jp**にアクセスするようにしているが、こちらはNTPサーバにほぼ確実に接続できているようなので、どうもソネットがネックになっているようだ。

## ▼ もしかして結露？

タイミングとNTPサーバの変更で改善はしたものの、それでも数時間bootカウントが0(=リブートが発生している)が発生することがある。また、場合によっては数時間測定できていないことがある。これは、外気温測定用のTimerSamplerで多く発生しており、長らく原因不明だった。測定ボード自体は室内にあり直接風雨にはさらされないのだが、窓際においているために夜には結構冷える。そこで、結露を防ぐ目的でプラケースを加工してその中に測定用ボードを入れていたのだが、もしかするとこれが逆効果だったのではないかと思いあたった。

試しに、ほぼ同じ構成でケースに入れない裸の測定ボードをほぼ同じ場所に設置して様子を見てみると、外気温測定用ボードが止まっている状態でもほぼ問題なく動作していることがわかった。

どうやらプラケースに入れたために結露して動作不良を越していた可能性があるようだ。そこでプラケースの蓋を外した状態で同じ場所に設置して様子を見ることにした。数日その状態で測定してみたが、今の所、あの悩ましい不安定は発生していない。

その後、センサーの温度と湿度から露点温度をグラフ表示できるようにしてみた。また、これらのデータから結露を判定できそうなグラフを表示できるようにしてみた。その結果、

**稼働状況グラフ(100以下ののこぎり部分が異常動作)**

![bme280_outer_boot-pinpoint=1580923524,1581031524](img/bme280_outer_boot-pinpoint=1580923524,1581031524-1581032161825.png)

**結露判定グラフ(赤線のグラフが０℃以上が結露の可能性あり)**

![compare_dewpoint-pinpoint=1580923525,1581031525](img/compare_dewpoint-pinpoint=1580923525,1581031525-1581032215025.png)

異常が発生し始めた時点での露点温度のデータがないので、すこし不確かではあるが、**室内露点ー室外温度**が露点限界を超えると以上が発生しているように見える(赤いグラフが０℃を上回っている部分)。特に、この日は屋外の最低気温が**氷点下９℃**と非常に寒い日だったので、結露しやすい状況にあり、症状が顕著に出たものと思われる。

## ▼ Munin-node再び

ESP32はTCP/UDPのサーバとしても動作する。そこで以前Munin-nodeとして動作するものを作ってみたのだが、測定値が抜け落ちることが多く、断念していた経緯がある。

しかし、この方法はMuninサーバに直接登録でき、かつMuninプラグインを作らなくてもいいというメリットがある。そこでなんとかならないものかと再チャレンジしてみた。

結論から言うと、データの欠損は完全には防げないようだ。しかしながら欠損する頻度は少なくすることができた。

問題はデータが欠損するとその時点のデータは**NaN**(Not a Number、rrdtool上では\*UNKNOWN\*として扱われるようだ)として記録されるため、グラフに隙間が空いてしまう。もっと悪いことに移動平均を得るとき一つでも欠損があるとその周りの移動平均がすっぽり抜けてしまうのである。

**データの欠損のあるグラフ**

![temp-pinpoint=1525140637,1525248637](img/temp-pinpoint=1525140637,1525248637.png)

上のグラフを見ると虫食いのようにデータが欠損しているのがよくわかる。だが、よく見ると抜けているデータは１から３サンプリング分程度なので、データを補間することができればなんとか実用になるのではないか？移動平均と同じようにMuninのCDEFを使えば抜け落ちたデータを直近の正常データで補完できないものかと考えた。しかし、Muninで欠損データを補間して表示するような例がなかなか見つからない。CDEFは逆ポーランド法で記述するなど癖の多いものなので、あまり使用している人がいないようだ。

仕方がないのでなんとか自分で書いてみようと思い、CDEF関連の文書を探していると、

```text
例２
  この例はIFとUNを使って*UNKNOWN*を0に置き換えるやり方を示しています。これは
  データの開始時刻が異なるいくつかのインターフェイスをまとめて表示する場合に
  有用です。

    rrdtool graph demo.gif --title="Demo Graph" \
        DEF:idat1=interface1.rrd:ds0:AVERAGE \
        DEF:idat2=interface2.rrd:ds0:AVERAGE \
        DEF:odat1=interface1.rrd:ds1:AVERAGE \
        DEF:odat2=interface2.rrd:ds1:AVERAGE \
        CDEF:agginput=idat1,UN,0,idat1,IF,idat2,UN,0,idat2,IF,+,8,* \
        CDEF:aggoutput=odat1,UN,0,odat1,IF,odat2,UN,0,odat2,IF,+,8,* \
        AREA:agginput#00cc00:Input Aggregate \
        LINE1:aggoutput#0000FF:Output Aggregate

  idat1が*UNKNOWN*データを持つ場合、次のCDEF式

    idat1,UN,0,idat1,IF

  はスタックに1,0,NaNが入った状態となり、IF演算子がこの3つの値をポップして
  0と入れ替えます。idat1が7942099のような実際の値の場合は、スタックは
  0,0,7942099となり実際の値に置き換えられます。
```

という記述を見つけた。この例では抜け落ちたデータを"0"に置き換えているが**"0"**を**"PREV"**にすれば目的の補間動作を行わせることができるのではないか？

実は、最初は以下のようなコードでデバッグしていたのだが、どうもUNKNを使う方法は良くないようで、

```text
boot.cdef boot,UNKN,EQ,boot,PREV,IF
```

つぎのような、

```text
inter.cdef boot,UN,PREV,boot,IF
```

コードに落ち着いた。UNを使うとEQを使う必要がない(=多分、今回のような利用目的のために作られたもの)。

```C++
char *node_config_boot =
  "graph_title 死活(%s)\n"
  "graph_args --base 1000 -r -l 0 --alt-autoscale\n"
  "graph_vlabel 回\n"
  "graph_scale no\n"
  "graph_printf %%6.0lf\n"
  "graph_category measure_alive\n"
  "graph_info 死活\n"
  "boot.label 死活\n"
  "boot.info 死活\n"
  "inter.label 補間\n"
  "inter.draw LINE1\n"
  "inter.cdef boot,UN,PREV,boot,IF\n"
  "inter.update no\n";
```

上記のような応答をMuninのconfig要求に対して送ることでグラフを描画する際に補完したグラフを書くことができるようになった(下図参照)。

**データ補間を行ったグラフ**

![img](img/boot-pinpoint=1579980938,1580088938.png)

斜めの直線が少しギザギザしているところがデータが欠損しているところである。ちなみに温度など移動平均を描画している場合は次のようなconfigでに落ち着いた。移動平均には補間後のデータ(inter)を元データとして使うことで欠損を防止している。

```C++
char *node_config_sht3x_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info SHT3Xセンサーで測定した温度\n"
  "temp.label 温度　　\n"
  "inter.label 補間　　\n"
  "inter.draw LINE1\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";
```

この設定で描画したのが次のグラフである。

**データ補間後の移動平均を含むグラフ**

![img](img/temp-pinpoint=1579980938,1580088938.png)

これでなんとかMunin-nodeプログラムも日の目を見ることとなった。

しかし、このグラフだとタイマーでサンプリングしたグラフとグラフ本数や描画色が異なってしまう。できれば欠損を補完したラインは表示しなくてもいい。そのため、configに

```text
temp.graph no
inter.label 温度
```

のような記述を追加した。

**設定を追加したあとのグラフ**

![img](img/temp-pinpoint=1580035840,1580143840.png)

これで欠損データを省いてグラフを表示できるようになった。

## ▼ 人感センサーを使う

BME280で温度や湿度などを測定するだけではつまらないので、Arduino系では定番の人感センサーをつないで測定してみることにした。センサーは３個800円のAmazonで購入したもの。ピン配置の情報がないので少し苦労したが、無事センサーをつないで測定することができた。

**人感センサーの測定データ**

![img](img/pir-pinpoint=1579983943,1580091943.png)

このグラフは５分ごとの検出数描画している。室内に転がしてあるので、あれだが、でかけていたり寝ているときは検出数が０になるのでかんたんな見守りセンサーに使えそうだ。

## ▼ RSSIの測定

また、測定データが取れない原因としてWi-Fiの信号強度が弱い可能性があったため、RSSI(Received Signal Strength Indicator)と呼ばれる信号強度を取得できるようにしてみた。

**Wi-Fiの信号強度(近距離)**

![img](img/bme280_test_rssi-pinpoint=1579984538,1580092538.png)

Wi-Fiアクセスポイントに近いと上のグラフのように**-30dBm**程度の信号強度があるが、

**Wi-Fiの信号強度(遠距離)**

![img](img/bme280_local2_rssi-pinpoint=1579984840,1580092840.png)

距離があったり障害物が多いと上のグラフのように**-70dBm**程度になってしまう。また時々**-80dBm**を切ることもあり、なかなか厳しい状態である。

Munin-nodeではデフォルトで測定できるようにしてあり、この機能を使えばWi-Fiの安定性をある程度判断できるので、実際にセンサーを設置するときに役に立つと思われる。

## ▼ Muninの馬鹿

上記のグラフには-70dBmと-80dBmの目安線(緑と赤のライン)があるが、これを表示するのはなかなか苦労した。Muninは`{Field name}.warning`と`{Field name}.criical`で注意・警報レベルを設定できるのだが、グラフ上にはなぜか`{Field name}.warning`しか表示されない。なので苦肉の策で`{Field name}.line`コマンドで-80dBmのラインを描画している。追加で表示するラインが１本の場合はこれでいいのだが、２本以上引こうとすると、どちらか１本しか表示されない。

**複数の目安線の例**

![img](img/bme280_local3_di-pinpoint=1579985739,1580093739.png)

このように複数の目安線を引くためにはどうしたら良いのか試行錯誤した挙げ句なんとか引けたのが上のグラフ。

**注意・警告ラインを追加する例**

```shell
echo 'graph_title 倉庫不快指数(local2)'
echo 'graph_args --base 1000 -l 55.0 -u 85.0 --alt-y-grid --alt-autoscale'
echo 'graph_vlabel DI'
echo 'graph_scale no'
echo 'graph_category measure_di'
echo 'graph_info 不快指数'
echo 'di.label 不快指数'
echo 'di.info bme280'
echo 'di.warning 60:80'
echo 'di.critical 55:85'
echo 'graph_order di trend'
echo 'trend.warning 55:85'
echo 'trend.critical 55:85'
echo 'trend.label 移動平均'
echo 'trend.draw LINE1'
echo 'trend.min 0'
echo 'trend.info di trend'
echo 'trend.cdef di,3600,TREND'
echo 'trend.update no'
```

この場合、２つのデータ(di,trend)に注意ラインと警告ラインを振り分けて表示させている。

なぜこんなことになるかというと、Muninはこのような設定から**rrdtool**のコマンドライン引数を生成してrrdtoolを呼び出しているためフルにrrdtoolの記述ができるわけではない、ということである。`{Field name}.line`は実際にはrrdtoolの`HRULE`に変換されて渡されるといった外部からは見えない変換が入る。

欠損データを補完する場合も、

```text
inter.cdef temp,UN,PREV,temp,IF
```

は、

```text
CDEF:ainter=itemp,UN,PREV,itemp,IF
```

のように変換されるようだ。よく見るとitempやainterのような仮想名を生成してそれを使っている。このように変換されるのでrrdtoolでは書けてもMuninでは書けないといった現象が起こるのである。

非常に癖はあるものの、うまく使えばMuninだけで結構複雑なこともできるので、もう少し複雑な処理もチャレンジしてみたいと思っている。

このような設定で使っていたのだが、障害ライン、警告ラインを超えたときの赤枠や黄色の枠が表示されないことに気がついた。どうやら実データにwarning及びcriticalの設定をしないと、表示されないようだ。なので、

```C++
char *node_config_adt7410_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info adt7410センサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info ADT7410センサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.update no\n"
  "inter.graph no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";
```



のように、補完データを非表示にし、移動平均を補間データから作成するように変更した。

## ▼ OTAアップデート

ESP32は手軽にプログラムのリモートアップデートができるようにOTAアップデートをプログラム内に組み込めるライブラリがある。これとmDNSを使うことで、任意の名前のESP32デバイスをArduino IDEからアップデートできるようになる。これがBasicOTAで、他にもOTAWebUpdaterというESP32をWEBサーバにしてブラウザを使ってアップデートする事もできるようだ。ということでBasicOTAを使ってネットワーク越しのシリアルポートを使ってアップデートできるようにしてみた。

注意点としては、Windows 10の場合は、ESP32デバイスをOTAアップデート可能にしたあと、Windows 10マシンを一度再起動しておかないとネットワークポートが正常に認識されないことだ。

ただ、信号強度の弱い(-70dBm以下)場所に設置すると、munin-nodeサーバが認識できなくなりOTAアップデートができないということもあるので、設置場所はそれなりの信号強度が必要になる。

信号強度が弱い場合はTimerSampler方式のほうが良いようだ。

が、TimerSampler方式がOTAアップデートできないというのも非常に不便なのでなんとかできないかと思い次のような方法を考えて実装してみた。

1. データ収集サーバのOTAアップデートを行いたいデータの拡張子(xxxx.dat)を.otaにしたファイルを作る
1. データ収集用スクリプトはHTTPのレスポンスとして`OTA`を返す。
1. TimerSamplerはHTTPのレスポンスが`OTA`の場合には実行モードをota_modeにセットしてすぐに`deep sleep`で再起動する。
1. 再起動したTimerSamplerはOTAモードで起動し、通常は実行しない`void loop()`内で`ArduinoOTA.handle()`を呼び出し、OTA状態になる。
1. データ収集サーバのxxx.otaファイルが消えるのを待つ。
1. Arduino IDEでOTAアップデートを行う。
1. TimerSamplerは再起動され、通常モードでで`deep sleep`する。

ややっこしいと思うかもしれないが、ユーザーが行うのは1,5,6の部分だけでなので手順としてはそれほど複雑ではない。

と言うことで、めでたくmunin-node、TimerSamplerどちらもOTAでアップデートできるようになった。ちなみに、下のスクリーンショットがArduino IDEのシリアルポートメニューに追加されたネットワークポート。

![network-port](img/network-port.png)

OTAを組み込むときmDNSを使えるようにしておくと、シリアルポートと違い名前とIPアドレス、コンパイルしたときのボードの種類が表示される。シリアルポートだと、どのボードがどのシリアルポートでつながっているかがわかりにくいことが多々あるので、シリアルポートでアップデートするよりも間違いが少なくなるように思う。

## ▼ Muninのカスタマイズ

上でも書いたようにMuninは色々とカスタマイズしないときれいな見た目にならない。そのため、現在使用しているMuninには色々カスタマイズしている。

### ▶ munin_dynamic_template

まず、全体的に見た目を良くするために、munin_dynamic_templateというJQuery及びBootstrapを使用したフロントエンドに変更した。これでひと目でどこに障害があるかわかるわけである。下のスクリーンショットでも「屋外不快指数」と「倉庫信号強度」に障害があることがすぐに分かる。赤色枠は障害レベルを超えたもの、黄色枠は警告レベルを超えたものを表している。

**munin_dynamic_template**

![munin2改](img/munin2改.png)

munin_dynamic_templateは基本的に英語バージョンなので英語部分を日本語で表示できるように手動でパッチを当てている。結構苦労したが、rrdtoolに依存する部分はあまり変更できないので、それなりの変更にとどめている。

munin_dynamic_templateの特徴として、上のスクリーンショットのように**Overview**という一部のグラフをトップページにカスタマイズして表示する機能があり(翻訳版では**ダッシュボード**に語句を変更した)、これを使いたかったので、表示したいグラフを下記のように設定した。

**/etc/munin/munin_dynamic_template/munin2/template.conf**

```javascript
/******************************************************************************
 * Here you can customize the graph that is show in the overview page.
 * Fill in the js array, one line for each graph, ex:
 *
 * var overview_data = [
 *    "Category 1","group/host/service","Graph name",
 *    "Category 1","group/host/service","Graph name",
 *    "Category 2","domain/host/df","Host disks",
 * ];
 *
 *****************************************************************************/

var overview_data = [
  /* 温度 */
  "温度",    "VPS/skr2.sgmail.jp/bme280_temp",          "室内温度",
  "温度",    "VPS/skr2.sgmail.jp/bme280_local_temp",    "室内温度(local)",
  "温度",    "VPS/skr2.sgmail.jp/bme280_outer_temp",    "屋外温度(outer)",
  "温度",    "VPS/skr2.sgmail.jp/bme280_local2_temp",   "倉庫温度(local2)",
  "温度",    "local/floor/temp",                        "床面温度(floor)",
  "温度",    "local/local3/temp",                       "居間温度(local3)",
  "温度",    "local/stickc/temp",                       "WC温度(stickc)",
  "温度",    "local/coolerbox/temp",                    "保冷庫温度(cbox)",
...中略...
  /* その他 */
  "その他",  "VPS/skr2.sgmail.jp/bme280_outer_pressure","屋外気圧(outer)",
  "その他",  "VPS/skr2.sgmail.jp/multi_temp",           "温度",
  "その他",  "VPS/skr2.sgmail.jp/multi_humidity",       "湿度",
  "その他",  "local/pi3/munin_stats",                   "munin負荷",
  "その他",  "local/node-01/pir",                       "人感センサー(node-01)",
  // "その他",  "local/blank/blank",                       "_",
  // "その他",  "VPS/skr2.sgmail.jp/bme280_pressure",      "室内気圧",
  // "その他",  "VPS/skr2.sgmail.jp/esp32_latency",        "室内起動時間誤差",
];

```

### ▶ apacheのrewriteを使った置換

この設定で、上記のスクリーンショットのような画面になるのだが、`No Sensor`の表示は改造なしでは表示できない。これは少しトリッキーな方法を使っている。これがなぜ必要かというと、温度のみのセンサーでは不快指数や暑さ指数が計算できないのに対し、温湿度センサーの場合は計算が可能だ。このような場合に、異なるセンサーを使用すると、どうしても表示できないグラフが出てきてしまう。これをダッシュボードのようにマトリックスで表示しようとすると、表示位置がずれてしまい、非常に見にくくなるからである。

この部分は、まず適当なグラフを表示するMunin-node(ここではblankというダミーノード、ダミーグラフにした)を実際に作り、`blank`という名前でグラフを表示できるようにする。`local/blank/blank`がその表示部分である。

ただし、この状態では(ダミーではあるが)実際のグラフが表示されてしまうので、`No Sensor`のようなブランクとわかる表示にするために、apacheのrewrite機能を使用して、

**/etc/apache2/sites-enabled/munin.conf**

```text
<Directory /var/cache/munin/www>
  Order allow,deny
  Allow from all
  Require all granted
  Options FollowSymLinks MultiViews
  AllowOverride All
  ...
  <IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteRule ^(.*)blank-day.png$ /munin/static/blank-small.png
    RewriteRule ^(.*)blank-week.png$ /munin/static/blank-small.png
    RewriteRule ^(.*)blank-month.png$ /munin/static/blank-small.png
    RewriteRule ^(.*)blank-year.png$ /munin/static/blank-small.png
  </IfModule>
</Directory>
...
<Location /munin-cgi/munin-cgi-graph>
  Order allow,deny
  Allow from all
  ...
  <IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteRule ^(.*)blank-pinpoint=(.*)$ /munin/static/blank-big.png
  </IfModule>
</Location>
```

のような設定を追加した。これで特定のURLのみ`No Sensor`を表示するpng画像を表示するように変更することができる。もちろん、置き換えたい画像(`blank-small.png`,`blank-big.png`)は適宣用意しておく。

これを、`/etc/munin/munin_dynamic_template/munin2/static`に入れておけば、`/var/cache/munin/www/static`にコピーされる。

munin-cgi/munin-cgi-graphディレクティブ内のrewriteはdynamic_zoomの画像を置き換えるためのもので、これがないとズーム時にダミーグラフが表示されてしまう。

### ▶ DynamicZoom機能の日本語化

また、dynamic_zoomのとき、表示が英語版になってしまうので、

**/usr/lib/munin/cgi/munin-cgi-graph**

```perl
# BEGIN FAST-CGI LOOP:
# setlocale (LC_TIME, 'C');
my $nb_request = 0;
my $nb_request_max = 0;
while (new CGI::Fast) {
  ...
}
```

の`setlocale`部分のコメントアウトを外して、

```perl
# BEGIN FAST-CGI LOOP:
setlocale (LC_TIME, 'ja_JP.UTF8');
my $nb_request = 0;
my $nb_request_max = 0;
while (new CGI::Fast) {
  ...
}
```

のように`ja_JP.UTF8`を明示的に使用するようにした。

この結果**Dynamic Zoom**の時も、

![bme280_outer_pressure-pinpoint=1580527534,1580635534](img/bme280_outer_pressure-pinpoint=1580527534,1580635534.png)

のようにX軸の表示を日本語的(語順は英語のママ)にすることができた。現在のところ日本語化はこのあたりが限界のようだ。これ以上はrrdtool自体に手を入れる必要があると思われる。

### ▶ 昼夜の背景表示

グラフは日中は白の背景、夜間はグレーの背景になるようにしている。これは、[muninグラフで夜間帯に色を付ける](https://tech.buty4649.net/entry/2014/07/25/muninグラフで夜間帯に色を付ける)を参考に行ったものだが、夜間の判断は固定ではなく、実際に日の出、日の入の時刻を取得して色分けを行うようにしている。

### ▶ 複数ノードにまたがるグラフの表示

**複数ノードの温度表示**

![multi_temp-pinpoint=1580761828,1580869828](img/multi_temp-pinpoint=1580761828,1580869828.png)

複数のノードの同一項目を同じグラフに表示できると状態の比較などがやりやすくなる。しかし、rrdデータが複数のノードに分散しているため、普通のMunin-nodeの設定では表示することができない。Muninでは複数のDEF定義をconfig中に記述できないためだ。

しかし、なにか方法がないかと思い、それらしい情報を探して試行錯誤してみた結果、上のグラフのように複数ノードのデータを一つのグラフに表示することができた。これはMuninサーバー上にマルチグラフ用のプラグインを作成して表示している。

**/etc/munin/plugins/multi-temp**

```shell
#!/bin/bash

if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi
if [ "$1" = "config" ]; then
  echo 'graph_title 温度(multi)'
  echo 'graph_args --base 1000'
  echo 'graph_vlabel ℃'
  echo 'graph_scale no'
  echo 'graph_category measure_temperature'
  echo 'graph_info 温度'
  echo -n 'graph_order'
  echo -n ' outer_gr=VPS;skr2.sgmail.jp:bme280_outer_temp.temp'
  echo -n ' myroom_gr=VPS;skr2.sgmail.jp:bme280_local_temp.temp'
  echo -n ' floor_gr=local;floor:temp.temp'
  echo -n ' living_gr=local;living:temp.temp'
  echo -n ' storeroom_gr=local;storeroom:temp.temp'
  echo -n ' stickc_gr=local;stickc:temp.temp'
  echo -n ' coolerbox_gr=local;coolerbox:temp.temp'
  echo ''
  #
  echo 'outer_gr.draw LINE1'
  echo 'myroom_gr.draw LINE1'
  echo 'floor_gr.draw LINE1'
  echo 'living_gr.draw LINE1'
  echo 'storeroom_gr.draw LINE1'
  echo 'stickc_gr.draw LINE1'
  echo 'coolerbox_gr.draw LINE1'
  #
  echo 'outer_gr.label     屋外温度　'
  echo 'myroom_gr.label    室内温度　'
  echo 'floor_gr.label     床面温度　'
  echo 'living_gr.label    居間温度　'
  echo 'storeroom_gr.label 倉庫温度　'
  echo 'stickc_gr.label    トイレ温度'
  echo 'coolerbox_gr.label 保冷庫温度'
  echo 'outer_gr.info      外気温'
  echo 'outer_gr.line 25.0:880000'
  exit 0
fi
```

肝は、graph_orderに表示したいグラフのデータを列挙することにある。このようにすると明示的ではないものの複数の外部参照用のDEFを設定できるようだ。graph_orderに記述しただけではグラフのラベルに日本語のラベルを表示することができないようなので、追加の`{fieldName}.label`の定義を行ってラベルの変更を行っている。が、残念なことに少し不具合があり、`Field`の一覧に２つ情報が表示されてしまう。

**Field一覧の不具合**

![multi-temp](img/multi-temp.png)

現在のところ、対処方法は不明である。実害はないので対策は行っていない。

だったのだが、

```shell
#!/bin/bash

if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi
if [ "$1" = "config" ]; then
  echo 'graph_title 温度(multi)'
  echo 'graph_args --base 1000'
  echo 'graph_vlabel ℃'
  echo 'graph_scale no'
  echo 'graph_category measure_temperature'
  echo 'graph_info 温度'
  echo 'update no'
  echo -n 'graph_order'
  echo -n ' outer=VPS;skr2.sgmail.jp:bme280_outer_temp.temp'
  echo -n ' myroom=VPS;skr2.sgmail.jp:bme280_local_temp.temp'
  echo -n ' floor=floor:temp.temp'
  echo -n ' living=living:temp.temp'
  echo -n ' storeroom=storeroom:temp.temp'
  echo -n ' stickc=stickc:temp.temp'
  echo -n ' coolerbox=coolerbox:temp.temp'
  echo ''
  #
  echo 'outer.graph no'
  echo 'myroom.graph no'
  echo 'floor.graph no'
  echo 'living.graph no'
  echo 'storeroom.graph no'
  echo 'stickc.graph no'
  echo 'coolerbox.graph no'
  #
  echo 'outer_inter.cdef outer,UN,PREV,outer,IF'
  echo 'outer_inter.label 屋外温度　'
  echo 'outer_inter.draw LINE1'
  echo 'outer_inter.update no'
  #
  echo 'myroom_inter.cdef myroom,UN,PREV,myroom,IF'
  echo 'myroom_inter.label 室内温度　'
  echo 'myroom_inter.draw LINE1'
  echo 'myroom_inter.update no'
  #
  echo 'floor_inter.cdef floor,UN,PREV,floor,IF'
  echo 'floor_inter.label 床面温度　'
  echo 'floor_inter.draw LINE1'
  echo 'floor_inter.update no'
  #
  echo 'living_inter.cdef living,UN,PREV,living,IF'
  echo 'living_inter.label 居間温度　'
  echo 'living_inter.draw LINE1'
  echo 'living_inter.update no'
  #
  echo 'storeroom_inter.cdef storeroom,UN,PREV,storeroom,IF'
  echo 'storeroom_inter.label 倉庫温度　'
  echo 'storeroom_inter.draw LINE1'
  echo 'storeroom_inter.update no'
  #
  echo 'stickc_inter.cdef stickc,UN,PREV,stickc,IF'
  echo 'stickc_inter.label トイレ温度'
  echo 'stickc_inter.draw LINE1'
  echo 'stickc_inter.update no'
  #
  echo 'coolerbox_inter.cdef coolerbox,UN,PREV,coolerbox,IF'
  echo 'coolerbox_inter.label 保冷庫温度'
  echo 'coolerbox_inter.draw LINE1'
  echo 'coolerbox_inter.update no'
  #
  echo 'outer_inter.line 25.0:880000'
  #
  exit 0
fi
```

のように、欠損データの補間を行ったものを表示するようにすることで、

**正常なField一覧**

![screenshot-003](img/screenshot-003.png)

上のスクリーンショットのように正常な表示を行うことができるようになった。