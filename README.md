# **Off-Grid IoT device Project**
----

1. [デバイスの目的](./iot-purpose.md)
2. [電源系](./iot-power.md)
3. [本体・計測系](./iot-device.md)
4. [間欠動作用プログラム](./iot-interval.md)
5. [計測データの可視化](./iot-munin.md)
6. [筐体](./iot-case.md)
7. [Wi-Fiの設置](./iot-wifi.md)
8. [Raspberry pi の使い方色々](./iot-raspberrypi.md)
9. [IoTにおけるArduino その１](./iot-arduino1.md)
10. [IoTにおけるArduino その２](./iot-arduino2.md)
11. [IoTにおけるArduino その３](./iot-arduino3.md)
12. [IoTにおけるArduino その４](./iot-arduino4.md)
13. [IoTにおけるArduino その５](./iot-arduino5.md)
14. [I2Cで使えるセンサー](./i2c-sensor.md)
15. [Raspberry piでArduino IDEを使う](./arduino-IDE on raspberry pi.md)
16. [Raspberry pi + MovidiusでAI](./raspberrypi-movidius.md)
17. [工具など](./iot-tools.md)
18. [おまけ](./iot-etc.md)
19. [参考URL](./iot-reference.md)
----

