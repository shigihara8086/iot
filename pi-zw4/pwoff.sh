#!/bin/bash

# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

#測定
`./measure.sh`
#
offtime=`hexsec 30`
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x11 0x00 0x10 $offtime 0x00 i > /dev/null 2>&1 #30秒後にoff
  res=$?
  sleep 0.5
  if [ $res = 0 ]; then
    break
  fi
done
#
pontime=`hexsec 3450`
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x10 0x00 0x10 $pontime 0x00 i > /dev/null 2>&1 #3600-150秒後にon
  res=$?
  sleep 0.5
  if [ $res = 0 ]; then
    break
  fi
done
#
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x02 i > /dev/null 2>&1 #LED点滅
  res=$?
  sleep 0.5
  if [ $res = 0 ]; then
    break
  fi
done
#シャットダウン
/sbin/poweroff
