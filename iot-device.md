[TOC]

# ■ 本体・計測系
----

## ▼ 入手可能なRaspberry Piについて

現在のところ、消費電力などを考慮すると、使用できるのは**Raspberry Pi zero**もしくは**Raspberry Pi zero W**になると思うが、現時点ではかなり入手困難である。場合によっては[**Raspberry Pi Model B+**](https://raspberry-pi.ksyic.com/)のような少し古いが入手しやすいボードも候補に入れる必要があるかもしれない。**Raspberry Pi Model A**も候補にはなるがピンヘッダのピン数が違うので注意が必要である。

**Raspberry Pi zero**および**Raspberry Pi Model B+**は**Wi-Fi**が内蔵されていないので、別途[**Wi-Fiドングル**](https://www.amazon.co.jp/gp/product/B00ESA34GA/ref=oh_aui_detailpage_o01_s00?ie=UTF8&psc=1)を購入する必要がある。**Wi-Fiドングル**は非常に多くの製品があるが、ものによっては**Raspberry Pi zero**に接続して使用するためにドライバをインストールする必要があったり、消費電力が多く、動作が安定しない物があったりするので、前述のドングルにように**Raspberry Pi zero**での動作が検証されているものを購入する必要がある。

**Raspberry pi zero W**
![入手できたRaspberry pi zero W](./img/img20170730_123346.jpg)

と書いていたが、やっと技適にということで**Raspberry Pi zero W**が発売された。
１時間で売れ切れになるという人気ぶりの中、やっと２個入手できたので、早速起動テストをやってみたのだが、**Wi-Fi**を含めて消費電力が**100mA以下**になっており、どうもUSB電源やモバイルバッテリの**オートパワーオフ**の限界値以下で動作しているようで、通電しても放置すると電源が切れてしまう。うーむ、困った。付属のACアダプタでは動作するので、**オートパワーオフ**が働かなければ大丈夫なようだ。

## ▼ Wi-FiドングルとUSBハブ

また、**Raspberry Pi zero**の**USB端子**は**Micro-B**タイプなので、複数のUSB機器を接続する場合にはUSBハブが必要になるが、通常のUSBハブを変換コネクタで接続しても動作しない。Android端末で使われている、[**USB On-The-Go(OTG)**](https://www.wikiwand.com/ja/USB_On-The-Go)と呼ばれる規格に対応した[**USB-OTGハブ**](https://www.amazon.co.jp/gp/product/B01KA6U53A/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)が必要になる。ただし機器を一つだけ接続する場合は、[**USB-OTG変換アダプタ**](https://www.amazon.co.jp/gp/product/B01I1TD6IO/ref=oh_aui_search_detailpage?ie=UTF8&psc=1)のようなものを使えばダイレクトに接続できる。

**Raspberry Pi zero W**の場合は上記のような条件が緩和され、**内蔵Wi-Fi**を使用する場合は、USB端子には何も接続する必要がなく、一つだけ接続する場合には**USB-OTG変換アダプタ**程度で間に合わせることができる。

## ▼ 消費電力の低減

なぜ実験に**Raspberry Pi zero W**を使うかといえば、消費電力が**Raspberry pi**シリーズ中で一番小さいからである。それでも、**百数十mA**は消費するので、これをもっと少なくできないかと思い、やってみたのが**クロックダウン**である。その結果が下の表であるが、期待したほどの消費電力の低減は見られなかった。

|機種  |default|200MHz|1000MHz|
|:-----|------:|-----:|------:|
|zero  |  150mA| 140mA|  200mA|
|zero w|  100mA| 100mA|  100mA|

## ▼ 計測センサーとデータ送信

**Raspberry Pi**では、[**I2C**](https://www.wikiwand.com/ja/I2C)、[**SPI**](http://www.soramimi.jp/raspberrypi/spi/)、[**PIO**](https://www.wikiwand.com/ja/%E5%85%A5%E5%87%BA%E5%8A%9B%E3%83%9D%E3%83%BC%E3%83%88)、**USB**インターフェースを使用してセンサーを接続することができる。USBを除いて近距離インターフェースであるため、数cm以内にセンサーを接続する必要がある。通常は基板上にセンサーを実装する。

今回の実装では、測定そのものは重要ではなく、**太陽電池でRaspberry Piを間欠駆動する**ことがメインテーマであるが、何も測定しないのは実際の測定ができるかどうかの検証にならないので、[**気圧・気温・湿度センサー**](https://www.switch-science.com/catalog/2236/)を**I2C**インターフェースに接続し、計測を行うことを予定している。

また測定したデータは、WEB-APIを通じてサーバに送信する。本来はここで、[**Wi-Fiモバイルルータ**](https://www.amazon.co.jp/s/ref=nb_sb_ss_ime_i_4_10?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&url=search-alias%3Daps&field-keywords=wi-fi+%E3%83%A2%E3%83%90%E3%82%A4%E3%83%AB%E3%83%AB%E3%83%BC%E3%82%BF%E3%83%BC&sprefix=wi-fi+%E3%83%A2%E3%83%90%E3%82%A4%E3%83%AB%2Caps%2C264&crid=30KKDQLHM4JZW&rh=i%3Aaps%2Ck%3Awi-fi+%E3%83%A2%E3%83%90%E3%82%A4%E3%83%AB%E3%83%AB%E3%83%BC%E3%82%BF%E3%83%BC)を使用して、3GまたはLTE通信網を通じてデータを送信したいところであるが、今回はこの部分の検証は行わない。

調べていたところ、[**PIX-MT100 Conte(TM) LTE対応USBドングル**](http://shop.pixela.co.jp/pix-mt100.html)という製品が発売されており、**Raspberry Pi**を使った**IoT通信**もターゲットにしているらしい。価格はそれなりにするが、格安SIMやIoT向けのSIMと組み合わせれば、かなり有望ではないかと思うので、検証の対象に入れることとする。

本日、**気圧・気温・湿度センサー**が届いたので、とりあえず、**Raspberry Pi Model 3 B**にセンサーを接続して動かしてみた。**Raspberry Pi zero**は**RPi1114FDH**が接続されている都合上、**RPi1114FDH**にはんだ付けしなければならないので、テスト段階では**Raspberry Pi Model 3 B**を使った次第である。
サンプルのPythonプログラム([**bme280.py**](./bme280.py))があるので、それを少し手直しして実行してみた結果が、

```shell
$ sudo bme280/bme280.py
気圧 :   931.3 hPa
温度 :    29.9 ℃
湿度 :    45.8 ％
```

である。うーむ、なんか気圧が低すぎるし、温度が高すぎるような気がするが、まあ計測テストなので、深くは突っ込まないことにする。

実際に、**RPi1114FDH**に実装しての計測は、電源系などのテストが終わったら、最終的な仕上げとして行う予定である。とりあえず、簡単な**web-api**として**Rodeo**をベースに作成したものを以下に示す。

```python
#!/usr/bin/python
#coding: utf-8

import smbus
import time
import datetime
import locale
import sys

bus_number  = 1
i2c_address = 0x76

bus = smbus.SMBus(bus_number)

digT = []
digP = []
digH = []

t_fine = 0.0


def writeReg(reg_address, data):
	bus.write_byte_data(i2c_address,reg_address,data)

def get_calib_param():
	calib = []
	
	for i in range (0x88,0x9F+1):
		calib.append(bus.read_byte_data(i2c_address,i))
	calib.append(bus.read_byte_data(i2c_address,0xA1))
	for i in range (0xE1,0xE7+1):
		calib.append(bus.read_byte_data(i2c_address,i))

	digT.append((calib[1] << 8) | calib[0])
	digT.append((calib[3] << 8) | calib[2])
	digT.append((calib[5] << 8) | calib[4])
	digP.append((calib[7] << 8) | calib[6])
	digP.append((calib[9] << 8) | calib[8])
	digP.append((calib[11]<< 8) | calib[10])
	digP.append((calib[13]<< 8) | calib[12])
	digP.append((calib[15]<< 8) | calib[14])
	digP.append((calib[17]<< 8) | calib[16])
	digP.append((calib[19]<< 8) | calib[18])
	digP.append((calib[21]<< 8) | calib[20])
	digP.append((calib[23]<< 8) | calib[22])
	digH.append( calib[24] )
	digH.append((calib[26]<< 8) | calib[25])
	digH.append( calib[27] )
	digH.append((calib[28]<< 4) | (0x0F & calib[29]))
	digH.append((calib[30]<< 4) | ((calib[29] >> 4) & 0x0F))
	digH.append( calib[31] )
	#fixed get param
	#T2-T3
	for i in range(1,3):
		if digT[i] & 0x8000:
			digT[i] = (-digT[i] ^ 0xFFFF) + 1
	#P2-P9
	for i in range(1,9):
		if digP[i] & 0x8000:
			digP[i] = (-digP[i] ^ 0xFFFF) + 1
	#H2,H4,H5
	for i in [1,3,4]:
		if digH[i] & 0x8000:
			digH[i] = (-digH[i] ^ 0xFFFF) + 1  

def readData():
	data = []
	for i in range (0xF7, 0xF7+8):
		data.append(bus.read_byte_data(i2c_address,i))
	pres_raw = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
	temp_raw = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)
	hum_raw  = (data[6] << 8)  |  data[7]
	
	compensate_P(pres_raw)
	compensate_T(temp_raw)
	compensate_H(hum_raw)

def compensate_P(adc_P):
	global  t_fine
	pressure = 0.0
	
	v1 = (t_fine / 2.0) - 64000.0
	v2 = (((v1 / 4.0) * (v1 / 4.0)) / 2048) * digP[5]
	v2 = v2 + ((v1 * digP[4]) * 2.0)
	v2 = (v2 / 4.0) + (digP[3] * 65536.0)
	v1 = (((digP[2] * (((v1 / 4.0) * (v1 / 4.0)) / 8192)) / 8) + ((digP[1] * v1) / 2.0)) / 262144
	v1 = ((32768 + v1) * digP[0]) / 32768
	
	if v1 == 0:
		return 0
	pressure = ((1048576 - adc_P) - (v2 / 4096)) * 3125
	pressure = (pressure * 2.0) / v1
	v1 = (digP[8] * (((pressure / 8.0) * (pressure / 8.0)) / 8192.0)) / 4096
	v2 = ((pressure / 4.0) * digP[7]) / 8192.0
	pressure = pressure + ((v1 + v2 + digP[6]) / 16.0)  

	sys.stdout.write("p=%.1f&" % (pressure / 100))

def compensate_T(adc_T):
	global t_fine
	v1 = (adc_T / 16384.0 - digT[0] / 1024.0) * digT[1]
	v2 = (adc_T / 131072.0 - digT[0] / 8192.0) * (adc_T / 131072.0 - digT[0] / 8192.0) * digT[2]
	t_fine = v1 + v2
	temperature = t_fine / 5120.0
	sys.stdout.write("t=%.1f&" % (temperature))

def compensate_H(adc_H):
	global t_fine
	var_h = t_fine - 76800.0
	if var_h != 0:
		var_h = (adc_H - (digH[3] * 64.0 + digH[4]/16384.0 * var_h)) * (digH[1] / 65536.0 * (1.0 + digH[5] / 67108864.0 * var_h * (1.0 + digH[2] / 67108864.0 * var_h)))
	else:
		return 0
	var_h = var_h * (1.0 - digH[0] * var_h / 524288.0)
	if var_h > 100.0:
		var_h = 100.0
	elif var_h < 0.0:
		var_h = 0.0
	sys.stdout.write("m=%.1f" % (var_h))


def setup():
	#changed settting to indoor navigation
	osrs_t = 1			#Temperature oversampling x 1
	osrs_p = 5			#Pressure oversampling x 16
	osrs_h = 2			#Humidity oversampling x 2
	mode   = 3			#Normal mode
	t_sb   = 0			#Tstandby 0.5ms
	filter = 4			#Filter 16 
	spi3w_en = 0			#3-wire SPI Disable

	ctrl_meas_reg = (osrs_t << 5) | (osrs_p << 2) | mode
	config_reg    = (t_sb << 5) | (filter << 2) | spi3w_en
	ctrl_hum_reg  = osrs_h

	d = datetime.datetime.today()
	sys.stdout.write("https://sgmail.jp/demo/iot-api.cgi?d=")
	sys.stdout.write(d.strftime("%Y/%m/%dT%H:%M:%S&"))
	writeReg(0xF2,ctrl_hum_reg)
	writeReg(0xF4,ctrl_meas_reg)
	writeReg(0xF5,config_reg)

setup()
get_calib_param()

if __name__ == '__main__':
	try:
		readData()
	except KeyboardInterrupt:
		pass
```
**BME280**のサンプルプログラムを**web-api**呼び出し**URL**を生成するように変更したもの。

```shell
wget -q -O /dev/null `./bme280-api.py`
```
測定を行って、測定データを**wget**コマンドで送信する。

```ruby
#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require "#{File.dirname(__FILE__)}/lib/rodeo.rb"

dbname = File.basename(File.dirname(File.expand_path(__FILE__)))
sql = Rodeo::SQL.new(dbname,nil)
rodeo = Rodeo.new(__FILE__,nil,sql)
rodeo.get_cgi_data(nil,nil)
iot = rodeo.table_new("IoTテスト")
iot.insert_mode = true
iot.clear
begin
  iot["計測時刻"].value = Time.strptime(rodeo['d'],"%Y/%m/%dT%H:%M:%S")
  iot["気圧"].value = rodeo["p"].to_f
  iot["気温"].value = rodeo["t"].to_f
  iot["湿度"].value = rodeo["m"].to_f
  roid = iot.insert()
  if(roid)
    rodeo.eruby(__FILE__,"OK",binding)
  else
    rodeo.eruby(__FILE__,"NG",binding)
  end
rescue
  rodeo.eruby(__FILE__,"ER",binding)
end
rodeo.output()

__END__

#### OK

<html><body>OK</body></html>

#### NG

<html><body>NG</body></html>

#### ER

<html><body>ER</body></html>
```
**Rodeo**ベースのデータ格納用スクリプト。最低限の機能しかないが、これで一応、測定データをクラウドに送って、その後データベースで処理する、という一連の流れの実装テストを行って、きちんと動作することを確認できた。**Rodeo**で収集したデータをopenofficeでグラフ化した例を以下に示す。

![気圧・気温・湿度のグラフ](./img/iot-pmv.svg)

検証中の測定データは以下のURLで確認できる。

[sgmail.jp/demo/](https://sgmail.jp/demo/index.cgi)


検証中は、同一屋内で行っているため、通信は**Wi-Fi**で行っていたが、実際にフィールドで計測するとなれば、**Wi-Fi**ではなく**LTE網**などを使ってデータを送信する必要がある。

そこで、いわゆる**モバイルルーター**と呼ばれるものが必要になるのだが、多くのモバイルルーターはそれ自体にバッテリーを含んでいるため、太陽電池で駆動する場合、計測が終わってもモバイルルータの電源が切れず、無駄に電力を消費してしまうなど、普通のモバイルルーターはIoT向きではない。

IoT用途でも使用できるモバイルルータとして、[**PIX-MT100**](http://www.pixela.co.jp/products/network/pix_mt100/)が発売されているので、これを使用して通信を行うこととした。**PIX-MT100**はUSBで接続すると、PCからはUSB接続のLANアダプタとして認識されるので、**Raspberry pi zero**の**Wi-Fi**がないということが欠点ではなくなる。逆に**Wi-Fi**を殺す必要が無いので扱いやすくなる。

また、複数のセンサーを比較的狭い範囲で使う場合には、一つだけ**PIX-MT100**をつけたハブとなる**Raspberry pi zero**をおいて、その周りに**Raspberry pi zero W**で構成されたセンサーを配置して使うこともできる。

このモバイルルータは**SIMフリー**なので、何らかの**Docomo系のデータ通信用SIM**を用意する必要がある。IoT向けに幾つかの安価な契約のSIMがあるのだが、なかにはクラウドを使う前提になっていたりと落とし穴があるものもある。

そこで、[**0SIM**](http://mobile.nuro.jp/0sim/)を使用することにした。普通にインターネットを使うには遅くて使い物にならないと言う評価であるが、IoT向けには、遅いのはなんら問題にならないし、**500MB/月**まで無料なので、通信量の少ないIoT用途にはピッタリのSIMだと思われる。

この構成で、実際に接続し、テスト中のものが下の写真である。

![PIX-MT100を使った例](./img/img_0126.jpg)

この構成で実験したところ、どうやら起動時間が長くなるようで、**RPi1114FDH**に設定する次回起動までの時間を短くし、**150秒前**(無線LANの場合は**90秒**で良かった)から起動するように設定する必要があった。この時間は消費電力に直結するのでなるべく短い時間にしたいが、計測時刻を正確に保つには致し方ない。

この構成で稼働テストを行っていたところ、たまに**PIX-MT100**が起動しないという症状に見舞われた。原因ははっきりしないが、**PIX-MT100**の消費電流が**100mA**程度あるので、**Raspberry pi zero W**のUSBの電流供給能力が足りないか、**ソーラーチャージコントローラ**のUSBポートの電流供給能力が足りない可能性があるため、**PIX-MT100**の**Wi-Fi機能をOFF**にして消費電流を抑えた状態で稼働テストを行っている。

結論としては、**PIX-MT100**の**Wi-Fi機能をON**にした状態で電源ラインに**電気二重層コンデンサ**を追加して稼働テストを行ったところ、問題なく動作しているようである。

## ▼ 計測ネットワークの構成

今回のシステムでは、ネットワークも少し複雑になってきているので、ネットワークの構成を示しておく。

**ネットワーク構成図**

![ネットワーク構成図](./img/network.svg)

計測用のネットワークは**PIX-MT100**を介してインターネットに接続されており、計測を行っているときのみ一時的に**PIX-MT100**のWi-Fiネットワークが形成されることに留意する必要がある。

## ▼ 1-Wireでの温度測定

通常、**Raspberry pi**で使用する温度センサーは、基板上にチップがハンダづけされているため、気温を測定する場合には問題ないが、**水温**や**地温**を測定する用途には使用できない。

ところが、**Temper 1F**という温度計のプローブは、下記の写真のようにプローブ部分がステンレスのチューブになっており、水温や地温も測定できるようになっている。

![DS18B20](./img/img20170919_024119.jpg)

はじめは、[**熱電対**](https://www.wikiwand.com/ja/%E7%86%B1%E9%9B%BB%E5%AF%BE)か[**サーミスタ**](https://www.wikiwand.com/ja/%E3%82%B5%E3%83%BC%E3%83%9F%E3%82%B9%E3%82%BF)を使ったものだろうと思っていたのだが、どうも違ったらしい。どうやら、[DS18B20](http://akizukidenshi.com/catalog/g/gI-05276/)というデジタル温度センサーをステンレスチューブに封入したものだったようだ。

- [WINGONEER防水デジタルサーマルプローブまたはセンサーDS18B20](https://www.amazon.co.jp/gp/product/B01DCY9G0K/ref=oh_aui_detailpage_o03_s00?ie=UTF8&psc=1)

と言ったものがAmazonなどで非常に安価な価格で出回っているのを発見したので、一本購入して使ってみることにした。

このセンサーは**Raspberry pi**や**Arduino**で一般的な**I2C**ではなく、**1-Wire**という通信規格を使用している。**1-Wire**は最低二本の通信線で使用することができ、複数のセンサーを同じバス上に接続することができる。接続方法は[『1-wire温度センサーで部屋の温度を測定しよう』](http://blog.livedoor.jp/victory7com/archives/33399310.html)など「DS18B20」で検索すればたくさん発見できる。

とりあえず、プローブを接続して現在測定テスト中である。

## ▼ 簡易地震計を作る

誰でもやる温度・湿度・気圧の測定だけではつまらないので、[3軸加速度センサーモジュール LIS3DH](http://akizukidenshi.com/catalog/g/gK-06791/)を買って簡易的な地震計が作れないものかやってみた。Pythonで複雑なプログラムを作るのはちょっと大変なので、Rubyで作ってみた。

**lis3dh.rb**
```ruby
#!/usr/bin/ruby

require 'rubygems'
require 'i2c'
require 'time'

class LIS3DH
  attr :adjust_x
  attr :adjust_y
  attr :x
  attr :y
  attr :z
  attr :g
  def initialize(path,address = 0x19)
    @adjust_x = 0.0
    @adjust_y = 0.0
    @adjust_z = 0.0
    @x = 0.0
    @y = 0.0
    @z = 0.0
    @g = 0.0
    @device = I2C.create(path)
    @address = address
  end
  def s18(value)
    -(value & 0b100000000000) | (value & 0b011111111111)
  end
  def read()
    # x
    x_l = @device.read(@address,1,0x28).ord
    x_h = @device.read(@address,1,0x29).ord
    x_a = (x_h << 8 | x_l) >> 4
    @x = s18(x_a) / 1024.0 * 980.0
    # y
    y_l = @device.read(@address,1,0x2a).ord
    y_h = @device.read(@address,1,0x2b).ord
    y_a = (y_h << 8 | y_l) >> 4
    @y = s18(y_a) / 1024.0 * 980.0
    # z
    z_l = @device.read(@address,1,0x2c).ord
    z_h = @device.read(@address,1,0x2d).ord
    z_a = (z_h << 8 | z_l) >> 4
    @z = s18(z_a) / 1024.0 * 980.0
  end
  def adjust()
    10.times {|i|
      read()
      @adjust_x = ((@adjust_x * i) + @x) / (i + 1)
      @adjust_y = ((@adjust_y * i) + @y) / (i + 1)
      @adjust_z = ((@adjust_z * i) + @z) / (i + 1)
      sleep(0.1)
    }
  end
  def sampling()
    read()
    @x = @x - @adjust_x
    @y = @y - @adjust_y
    @z = @z - @adjust_z
    @g = Math.sqrt((@x * @x) + (@y * @y) + (@z * @z))
  end
  def logging(fd,sec)
    start = Time.now()
    limit = start + sec
    base  = Time.now().to_f
    loop {
      now = Time.now()
      fnow = now.to_f
      dsec = fnow - base
      sampling()
      fd.printf("%f,%.2f,%.2f,%.2f,%.2f\n",dsec,@x,@y,@z,@g)
      sleep(0.1)
      break if(now > limit)
    }
  end
end

dev = LIS3DH.new("/dev/i2c-1",0x19)
#puts "校正中..."
dev.adjust()
#puts "測定中..."
loop {
  dev.sampling()
  if(dev.g > 100)
    #puts "地震検知!!!"
    date = Time.now().strftime("%Y-%m-%dT%H%M%S")
    File.open("LIS3DH"+date+".csv","w") {|log|
      log.puts "t,x,y,z,g"
      dev.logging(log,30)
    }
    #puts "測定中..."
  else
    sleep(0.1)
  end
}
```

**munin**だと５分に一度加速度を測定するだけになるので、地震計などの突発的な計測は無理。そこで地震を検知したら、CSVファイルにデータを保存するようにしている。とりあえず現在は100gal以上の加速度を検知したら、地震が発生したとみなし、その時点から30秒間のデータをCSVファイルに保存している。

その結果がこれ。

![地震検知のサンプル](./img/LIS3DH2017-09-19T131740.svg)

実際に地震があったわけではないのでセンサーを手で揺すってデータを取得し、それをopenofficeでグラフ化したものである。本当にきちんとしたグラフができるかどうかは実際に地震が起こるのを待っているところである(起きては欲しくないけど)。

