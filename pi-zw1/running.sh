#!/bin/bash

vlimit=3.35
#
dt=`date "+%Y/%m/%dT%H:%M:%S"`
vt=`./ina226.py`
wget -q -O /dev/null "https://sgmail.jp/demo/iot-running.cgi?h=${HOSTNAME}&d=${dt}&v=${vt}"
#
if [ `echo "${vt} < ${vlimit}" | bc` == 1 ]; then
  /bin/rm ./uptime.txt
  ./pwoffmail.sh
  /sbin/poweroff
fi
