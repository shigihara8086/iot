#!/bin/bash

# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

sec=`expr $1 \* 60`
pontime=`hexsec $sec`
echo "$1分($sec秒)後に起動します。"
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x10 0x00 0x10 $pontime 0x00 i > /dev/null 2>&1 #指定秒後にon
  res=$?
  sleep 0.5
  if [ $res = 0 ]; then
    break
  fi
done
