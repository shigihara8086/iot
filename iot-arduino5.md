[TOC]

# ■ IoTにおけるArduino その５

その１が長くなりすぎたので、ここに続きを書くことにする。その２からその４は参考資料かな？

------

## ▼ ESP32-PICO-D4のバッテリー駆動

[IoTにおけるArduino その４](./iot-arduino4.md)でESP32-PICO-D4というより小型のESP32ボードを紹介したわけだが、通常のESP32に比べると消費電力が少なそうなので、これをモバイルバッテリーで動かしたときどの程度連続稼働できるものか試してみることにした。

Muninのblank表示用デバイスとして動かしていたものをそのまま24000mAhのモバイルバッテリーで駆動することにした。もちろん、30分もすると止まってしまうので、[モバイルバッテリー用USBダミー負荷](https://www.switch-science.com/catalog/3829/)を介して給電することにした。

実際にblankデバイスが停止するまでの時間は、約**６日間**駆動することが出来た。駆動時間は論理値(効率100%とした場合の値)の**60%**という結果で、実際のDC-DCコンバータの効率やダミー負荷の消費電力などを考えると、ほぼ妥当な駆動時間ではないかと思われる。



## ▼ Wi-Fi信号強度の怪

比較的離れた場所にある倉庫の測定を行っていたMunin-nodeの信号強度が大きかったので、もともとあったTimerSamplerの測定ボードを撤去した。すると、

![rssi-pinpoint=1580781417,1580889417](img/rssi-pinpoint=1580781417,1580889417.png)

途端に、Munin-nodeの信号強度が小さくなってしまった。

推測ではあるが、-70dBm程度のゲインはもともとあったTimerSamplerのデバイスが、導波器もしくは反射器の役目を果たしていたのではないかと思われる(デバイスはほぼ同じ場所に設置していた)。実際にRSSIの測定を行って初めてこういった現象を把握できたのは非常に意味のあることではないかと思う。

肝心の測定データはこのような電波状態でもほぼ問題なく取得できており、Munin-node方式の改良によって、よりロバストなデバイスにできた査証であると思う。

## ▼ Ambientを使った測定

IoT界隈ではAmbientというサービスが有名らしい。簡単にグラフが作れるらしいということで使用してみた。AmbientのESP32用のライブラリをインストールし、

**Ambient-bme280.ino**

```C++
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "Ambient.h"

#define HOSTNAME "ambient-bme280"

unsigned int channelId = 9999;                  // AmbientのチャネルID
const char* writeKey = "xxxxxxx";               // ライトキー

WiFiClient client;
Ambient ambient;

float meas_temp  = 0.0;
float meas_humi  = 0.0;
float meas_press = 0.0;

long loop_count = 0;

void setup() {
  Serial.begin(115200);
  wifi_init();
  bme280_init();
  ambient.begin(channelId, writeKey, &client);  // チャネルIDとライトキーを指定してAmbientの初期化
  bme280_getValues();
  ambient.set(1, meas_temp);
  ambient.set(2, meas_humi);
  ambient.set(3, meas_press);
  ambient.send();
}

void loop() {
  ArduinoOTA.handle();
  loop_count ++;
  if(loop_count > 1000 * 60 * 5) {
    loop_count = 0;
    bme280_getValues();
    ambient.set(1, meas_temp);
    ambient.set(2, meas_humi);
    ambient.set(3, meas_press);
    ambient.send();
  }
  delay(1);
}
```

**wifi.ino**

```C++
/*
 Wi-Fi
*/

#include <WiFi.h>
#include <WiFiMulti.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>

//
// wifi init
//

struct WifiMulti_aps {                          // ssid,password pare
  char *ssid;
  char *passwd;
};

struct WifiMulti_aps wifi_aps[] = {             // ssid,password table
  { "ssid1", "password1" },
  { "ssid2", "password2" },
  { NULL, NULL }
};

WiFiMulti wifiMulti;                            // Multi AP

//
// OTA
//

void ota_init() {
  Serial.println("OTA initialize start");
  ArduinoOTA.setHostname(HOSTNAME);
  // ArduinoOTA.setPassword("admin");
  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void wifi_init() {
int status;
int i;
  i = 0;
  while (wifi_aps[i].ssid) {
    wifiMulti.addAP(wifi_aps[i].ssid, wifi_aps[i].passwd);
    i ++;
  }
  Serial.print("WiFi connect start\n");
  WiFi.mode(WIFI_STA);
  i = 0;
  while(true) {
    status = wifiMulti.run();
    switch(status) {
      case WL_CONNECTED       : Serial.print("status : WL_CONNECTED\n"); break;
      case WL_NO_SHIELD       : Serial.print("status : WL_NO_SHIELD\n"); break;
      case WL_IDLE_STATUS     : Serial.print("status : WL_IDLE_STATUS\n"); break;
      case WL_NO_SSID_AVAIL   : Serial.print("status : WL_NO_SSID_AVAIL\n"); break;
      case WL_SCAN_COMPLETED  : Serial.print("status : WL_SCAN_COMPLETED\n"); break;
      case WL_CONNECT_FAILED  : Serial.print("status : WL_CONNECT_FAILED\n"); break;
      case WL_CONNECTION_LOST : Serial.print("status : WL_CONNECTION_LOST\n"); break;
      case WL_DISCONNECTED    : Serial.print("status : WL_DISCONNECTED\n"); break;
      default : {
        Serial.printf("status : Unknown : %d\n", status);
      }
    }
    if(status == WL_CONNECTED && WiFi.RSSI() != 0) break;
    i ++;
    if (i > 5) {
      Serial.print("Can not connect Wi-Fi. restart\n");
      ESP.restart();
    }
    while(WiFi.status() == WL_CONNECTED) {
      WiFi.disconnect();
      delay(100);
    }
    delay(1000);
  }
  Serial.printf("Connect Wi-Fi ssid:%s rssi:%d\n", WiFi.SSID().c_str(), WiFi.RSSI());
  // OTA
  ota_init();
  delay(1000);
}

void wifi_term() {
  while(WiFi.status() == WL_CONNECTED) {
    WiFi.disconnect();
    delay(100);
  }
}
```

**bme280.ino**

```C++
/*
BME280
*/

#include "Adafruit_BME280.h"
#include "Adafruit_Sensor.h"

#define I2C_SDA (21)
#define I2C_SCL (22)

#define BME280_ADD (0x76)
#define SEALEVEL_ADJUSTMENT ((260.0 / 100.0) * 12.0)

Adafruit_BME280 bme(I2C_SDA, I2C_SCL);

int bme280_init() {
  bool status;
  // 内蔵PULLUP ON
  pinMode(I2C_SDA, INPUT_PULLUP);
  pinMode(I2C_SCL, INPUT_PULLUP);
  //
  status = bme.begin(BME280_ADD);
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    return -1;
  }
  delay(100);
  return 0;
}

void bme280_getValues() {
  meas_temp = bme.readTemperature();
  meas_press = bme.readPressure() / 100.0F + SEALEVEL_ADJUSTMENT;
  meas_humi = bme.readHumidity();
  Serial.printf("気温 = % 5.1f ºC\n", meas_temp);
  Serial.printf("湿度 = % 5.1f %%\n", meas_humi);
  Serial.printf("気圧 = % 5.1f hPa\n", meas_press);
}
```

このコードは、munin-nodeのコードをAmbient用に流用したもの。これを動かせば、

**Ambientで作成したグラフ**

![screenshot-004](img/screenshot-004-1580934371367.png)

のようなグラフを作成することができる。なるほど、簡単である。色々含めて30分程度で出来てしまった。

とはいえ、簡単なことと、どのような機能を持つかは反比例。きれいなグラフは書けるけれど、それでお終いである。上限、下限の設定やIFTTT連携などの機能は持っていないようだ。まあ、グラフにできればそれで良い、という目的であればOKだと思う。

## ▼ ESP32の結露対策

その１で問題の原因は結露ではないかと目星をつけたわけだが、裸で置いても温度や湿度の条件によっては結露するわけだし、何らかの対策が必要になる。そこで「[日本製 吸水 速乾 セルロース スポンジ クロス 24×16cm ホワイト 3個セット](https://www.amazon.co.jp/gp/product/B01CXWY32I/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)」なるものが使用できないかと考えている。このスポンジは実に吸水性が良いのと、厚みが6mmと手頃な厚さのため、ESP32基盤の上部に載せて蓋をすればガタツキを抑えるのにも使用できそうで一石二鳥ではないかと考えた。とりあえず、仕込んでみて様子を見ることにする。

なぜ、上部かというと多分結露で異常を起こしているのは、ESP32-WROOM-32の金属カバーの内部だろうと思われるからだ。金属カバーには小孔があり外部の空気が流入する構造になっているし、金属なので内部で結露が起こりやすいと思われるからである。

最初はシートのままESP32の上に載せようとしたのだが、すこーし厚みがありすぎてきちんとプラケースの蓋をすることが出来なかった。これでは逆効果になりかねないので、細かく千切ってケースに詰め込むことにした。

## ▼ シンプルな太陽電池駆動

一応、太陽電池で駆動することはできているがもっと簡単な構成でできないかと思い試してみたのが下の写真の太陽電池。

![IMG_0005](img/IMG_0005.jpeg)

もう完璧におもちゃレベルなのだが、太陽電池の出力は**5V**なので、そのままリチウムイオン電池の充電側に接続でき、シンプルかつ安価になる。これで動けば御の字である。で、継続して実験中。

これには、５分に１回測定する(４分程度はdeep sleep状態)ボードを接続していたのだが、日中の充電量が足りない状態で１日持たないことがあった。５分に１回の測定ができれば良いなあと思っていたので、少し残念。LTEドングル等は接続していないので、もう少し測定頻度を下げればなんとか実用になりそうである。

が、どうにも解せないのでバッテリーシールドを、新しいものから

![IMG_0006-1608110331412](img/IMG_0006-1608110331412.jpeg)

以前買った古いバッテリーシールドに変えてみた。

![IMG_0067](img/IMG_0067.jpg)

すると、期待通りにバッテリー駆動できるではないか。うーむ、同じバッテリーシールドでも時々このような仕様変更があるのは困ったものだ。

この太陽電池は3週間ほどは正常に動作していたが、

![humidity-pinpoint=1611447328,1611555328](img/humidity-pinpoint=1611447328,1611555328.png)

天候が良くなかったため12:00頃に発電量が不足し停止してしまった。

そのまま放っておいたら復旧するかと思い放置していたのだが、そのような気配はなく、復帰することはなかった。そのため、13:00頃に新しい電池に交換して復旧させた。

## ▼ DS18B20センサーの追加

ESP32用の`munin_node`プログラムに、DS18B20センサーを追加した。

![img20170919_024119](img/img20170919_024119.jpg)

センサーは、ステンレスチューブに封入されているものがあるので、これを使えば水温とか地温のような測定に適している。

![IMG_0016](img/IMG_0016-1609111578692.jpeg)

絶縁スリーブがいらない場合は写真のようなTO92パッケージやSOパッケージ、μSOPパッケージのものがある。TO92パッケージを使ったもが、こちら。

![IMG_0017](img/IMG_0017.jpeg)

右下のトランジスタ状のものがTO92パッケージのDS18B20。マルチドロップで2個接続した例となる。

![IMG_0013](img/IMG_0013-1608635439292.jpeg)

測定ボードはこんな感じ。4.7KΩでプルアップしただけの簡単な構成である。

測定用のコードは以下の通り。

``` C++
/*
 DS18B20
*/

#if defined(ENABLE_DS18B20)

char *node_config_ds18b20_temp =
  "graph_title 温度(%s)\n"
  "graph_args --base 1000 --alt-autoscale\n"
  "graph_vlabel ℃\n"
  "graph_scale no\n"
  "graph_order temp inter trend\n"
  "graph_category measure_temperature\n"
  "graph_info DS18B20センサーで測定した温度\n"
  "temp.label 温度　　\n"
  "temp.info DS18B20センサーの温度\n"
  "inter.cdef temp,UN,PREV,temp,IF\n"
  "inter.graph no\n"
  "inter.update no\n"
  "trend.label 移動平均\n"
  "trend.line 25.0:880000\n"
  "trend.draw LINE1\n"
  "trend.min 0\n"
  "trend.info 温度の移動平均\n"
  "trend.cdef inter,1800,TREND\n"
  "trend.update no\n";

float meas_temp = 0.0;

#include <OneWire.h>
#include <DallasTemperature.h>

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

//
// initialize
//

int ds18b20_init() {
  sensors.begin();
  return 0;
}

void do_config_ds18b20_temp(WiFiClient &client) {
  while(true) {
    sensors.requestTemperatures(); 
    meas_temp = sensors.getTempCByIndex(0);
    if(meas_temp > -50 && meas_temp < 100) break;
    delay(100);
  }
  client.printf(node_config_ds18b20_temp,node_name);
  Serial.printf(node_config_ds18b20_temp,node_name);
}

//
// fetch
//

void do_fetch_ds18b20_temp(WiFiClient &client) {
  client.printf("temp.value %f\n",meas_temp);
  Serial.printf("meas_temp: %5.1f\n", meas_temp);
}

//
// loop
//

void ds18b20_loop() {
}

#endif
```

基本的にサンプルプログラムを`munin_node`に合うように改造した。

これで測定したのが以下のグラフ。

![temp-pinpoint=1608526231,1608634231](img/temp-pinpoint=1608526231,1608634231.png)

とりあえず、規定のケーブル長(1m)では正常に測定できることは確認できた。

![IMG_0014](img/IMG_0014-1608798056412.jpeg)

どのくらいの距離なら測定できるかが不不明だったので、とりあえず10mのフラットケーブルで延長してみた。

測定データを見ると、

![temp-pinpoint=1608696632,1608804632](img/temp-pinpoint=1608696632,1608804632.png)

センサーを10mに延長した後、数回の測定値が不正なものになっているが、その後は正常に取得できている。

が、その後、

![boot-pinpoint=1609867231,1609975231](img/boot-pinpoint=1609867231,1609975231.png)

のようなサーバが無反応になる期間(のこぎりの平らな部分)が多発することがわかった。どうやらWi-Fi環境の悪化が原因のようだ。思い当たるのは延長したケーブルがぐるぐる巻の状態なので、これがWi-Fiに干渉している可能性があるので、巻いてある部分を伸ばして様子を見ることにした。

結果的には、ケーブル長ではなく、ESP32モジュールそのものが不良だったようだ。別のモジュールに交換したところ正常に動作するようになった。

が、その後様子を見ていると、

![](img/boot-pinpoint=1610693131,1610801131.png)

のように、同様の現象が発生するようになった。ということは、ESP32モジュールの不良は否定されると思う。

さて、どうしたものか、ということでDS18B20なしのESP32モジュール(上記の動作不良を起こしたもの)を動かしてみると、途中でハングアップすることなく正常に動作しているようだ。

どうやらDS18B20センサーライブラリがWi-Fiに影響を与えている可能性が高いようだ。

## ▼ ToFセンサー VL53L0Xを入手

![IMG_0015](img/IMG_0015.jpeg)

赤外線レーザー式の測距センサー。2m程度の距離まで測定できるらしい。端子のシルクを見ればわかるようにI2Cインターフェースで扱えるようなので買ってみた。

## ▼ 0~100%(ただし結露なきこと)

外気温の測定にBME280を使っていたのだが、ついにというか、やっぱりというか、異常な測定値が発生してしまった。

|                           屋外温度                           |                           屋外気圧                           |                           屋外湿度                           |
| :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| ![bme280_outer_temp-day](img/bme280_outer_temp-day-1609645716036.png) | ![bme280_outer_pressure-day](img/bme280_outer_pressure-day-1609645950771.png) | ![bme280_outer_humidity-day](img/bme280_outer_humidity-day-1609646113788.png) |

-143.19℃というありえない値を測定してしまった。気圧も1249hPaというありえない値だ。結露によるセンサーエラーだと考えるのが妥当だと思う。そもそも屋外湿度の100%というのが、結露しているということであろう。

## ▼ DHT22センサーならどうか？

ということで、BME280を屋外で使用するのは無理ではないかと思うようになった。ではどうする？ということで、DHT22温湿度センサーを使ってみることにした。

![IMG_0018](img/IMG_0018.jpeg)

写真奥に見えるのがDHT22センサーである。1-wireライクなインターフェースで３線しかないので屋内から屋外にセンサーを出すのも楽である。センサーむき出しはなく一応プラスチックの容器に入っている。だから何？ではあるのだがこの程度でも結露対策になるかどうか検証する意味もある。

DHT22はDHT11に比べ、温度の分解能が0.5℃になっているのでDHT11よりは分解能の良いデータが取れる。湿度に関しては３%分解能であるとのことなので、精度的には十分である。これで湿度が100%に張り付くようなことがなければ万々歳である。

![humidity-pinpoint=1609733431,1609841431](img/humidity-pinpoint=1609733431,1609841431.png)

でDHT22で湿度を測定したグラフが上の図である。湿度の値からは結露しているようには見えない。同じ条件でBME280の測定データは、

![bme280_outer_humidity-pinpoint=1609733740,1609841740](img/bme280_outer_humidity-pinpoint=1609733740,1609841740.png)

で、15:30分頃から結露し始めていると思われる。しばらく２つのセンサーの値を比較することにする。

## ▼ 変わり種ブレッドボード

![91G5xJixSGL._AC_SL1500_](img/91G5xJixSGL._AC_SL1500_.jpg)

![61ksTzpkbAL._AC_](img/61ksTzpkbAL._AC_-1609692297785.jpg)

![61q4Z5rn-CL._AC_](img/61q4Z5rn-CL._AC_.jpg)

ちょっと変わったブレッドボード。普通のブレッドボードにあるセンターライン部分がなかったり、レゴブロックと連結できたり、複数ボードを縦に連結できたりと、ブレッドボードを超えた使い方ができそうである。

しかーし、最大の欠点は価格が高いこと。１枚1800円もする。が面白そうなので１枚だけ買ってみた。

![IMG_0019](img/IMG_0019.jpeg)

わからなかった裏面の状態が上の写真である。写真ではわからないが、結線部分の**ピンの穴が貫通**していた。背面もプラスチックなので通常のブレッドボードのように両面テープや透明樹脂テープではないので足をつけない状態でも座りが良い。しかし、穴が貫通しているので、ジャンパピンや抵抗の足などは背面まで貫通して少し飛び出してしまうので、足をつけたほうが良いようだ。

こういった特徴をうまく使うと、２段、３段と重ねたブレッドボードの配線を貫通させて、結構複雑な配線を行うことができるかもしれない。

穴の径はほんの僅かではあるが通常のブレッドボードより小さいような気がする。そのため太めの(多分断面が四角の)ジャンパーピンはかなりきついかもしれない。

![IMG_0020](img/IMG_0020-1609731490716.jpeg)

で、ESP32を載せてみたのが上の写真。残念なことに６列の穴があるものの幅は従来の５列のものと同じなので、ESP32のどちらか片側しか空きホールがない状態になってしまう。うーむ、微妙な状態である。もう１ピン分幅の狭いモジュールを使わないと配線が大変である。が、この１ピン分狭いモジュールがあまりないのだ。

![IMG_0106](img/IMG_0106.jpg)

幅の狭いモジュールは写真のTTGOのモジュールくらいしかない。がこいつは技適を通ってないので大手を振って使えないという…。

もしくは、ブレッドボードを2つ繋いで、それをまたぐようにモジュールを乗せるかなのだが、ブレッドボードを2つ使うのは、いかにも富豪的な方法である。ましてや普通のブレッドボードの４倍もするものを２つというのはあまりにもコストパフォーマンスが悪い。が気になるのでもう一枚買って、やってみた。

![IMG_0021](img/IMG_0021.jpeg)

わかりにくいかもしれないが、ESP32-DevkitCを連結したCakeBoardに載せてみるとあーらら、１ピン分幅が広いではないか！

また、実際にESP32をCakeBoardに挿してみた(上の写真は置いただけ)。すると、予想した通り、ピンがきついためなかなか抜けない。プラスチックのヘラ状のものを差し込んで騙し騙しなんとか抜くことができた。

**結論：つかえねー**

残念。

## ▼ ESP.restart()の怪

ESP32をコールドスタートするために、ESP.restart()を使うのだが、どうもWi-Fiに接続した直後のUDP通信が失敗しているようだ。そのため、24回起動後のコールドスタート時の次のdeep_sleep()復帰時のUDPができていないのではないかと思われる。

どうもよくわからない現象で、動作も追いにくいので、24回起動後のリスタートをなくし、HTTPでデータを送った後は常にdeep_sleep()で眠らせるようにした。通信エラーで測定が歯抜けになることはあるが、リスタート時の測定ができない状況はなくなった。

## ▼ ESP32計測システムの構成

この文書で記述しているシステムのネットワーク構成を改めて解説したい。

![internet](img/internet.png)

まず、`munin server`は自宅の`Raspberry Pi`で動作している。これをインターネットから参照可能にするにはちょっとした方策が必要である。

そのために、インターネット側にある`skr2.sgmail.jp`で`HTTP Proxy`(正確にはHTTP Reverse Proxyであるが)を立てて、`IPv6`経由で`Raspberry Pi`上の`HTTP server`に中継している。この時、インターネットからは`HTTPS`でアクセスしたものを`HTTP`で`Raspberry Pi`上の`HTTP`サーバーに中継することで擬似的に`HTTPS`サーバとして見えるようにしている。

また、`skr2.sgmail.jp`では`munin-node`を動かして`ESP32 TimerSampler`からのデータを中継している。なので、`ESP32 TimerSampler`のデータは、`skr2.sgmail.jp`のサーバーの稼働状況データの一部として`Raspberry Pi`上の`munin server`が取得している。

## ▼ 突然計測できなくなった

１日ほど前(2021/2/6あたり)から、LTEと同じくskr2.sgmail.jp経由で測定している室内の測定器が、

![bme280_local_boot-pinpoint=1612583131,1612691131](img/bme280_local_boot-pinpoint=1612583131,1612691131.png)

のような不可解な状態になって測定データがほとんど抜けてしまう状態になった。LTE経由は正常に動作しているし、ローカルネットワーク経由のデータも正常に取得できている。この１つだけが不可解な動作を起こし、skr2.sgmail.jpにデータを送れていないようだ。skr2.sgmail.jpのログを見ると、アクセスしてきた形跡がない。

頭を捻る状態だったのだが、試しにWi-Fiを別のSSIDの無線ルーター経由にしたら嘘のように正常に動いてしまった。どうもメインに使っているWi-Fiルーターをアップデートしたら、**TLS1.2**(ESP32のHTTPSはTLS1.2を使っているようだ)を使った通信ができなくなってしまったようだ。

まあ、家庭用のWi-FiはTLS1.3以上を使ったほうが良いのでセキュリティー的にはうなずける動作なのだが、家庭用ということもあって突然仕様が変更されたりするのは困ったものだ。

## ▼ Wi-Fiの信号強度

stickcデバイスの信号強度が非常に悪い状態になった。壁は挟んでいるもののWi-Fiアクセスポイントからの距離は2m程度と、信号強度が-80dB以下になるとは思えない状況であった。

![rssi-pinpoint=1615305021,1615413021](img/rssi-pinpoint=1615305021,1615413021.png)

もしやと思いコールドブートすると嘘のように-55dBあたりを維持するようになった。どうもESP32のWi-Fiライブラリはコールドブート時に送信電力を決定し、ウォームブート時(deep-sleepを含む)はその状態を保持しているようだ。stickcは内部にバッテリーがあるので、単純にUSBコネクタを外して現場に持っていくと、コールドリブートした場所の送信電力を維持してしまうわけである。最近はバッテリーを搭載したESP32システムも多いので注意が必要である。

## ▼ 再度、太陽電池駆動を考える

太陽電池で駆動するのは成功しているが、回路の構成上、鉛バッテリーが必要になるのが欠点である。そこで、鉛バッテリーを使わない構成を試してみた。

![IMG_0023](img/IMG_0023.jpeg)

一番下がリチウムイオン電池、その上の左側が太陽電池からDC5Vを生成するDC-DCコンバータ、その右側が5Vからリチウムイオン電池を充電するための充電コントローラ、その左上が3.7VDCから5VDCを生成するためのDC-DCコンバータ、及びESP32モジュールと言う構成になっている。スリープ時の微小電流駆動に対応するためこのように上げたり下げたり忙しい構成になっている。

![IMG_0024](img/IMG_0024.jpg)

上の写真が実際に稼働させた状態。写真だけ見るとまともそうだが小さなモジュールは全て中華モジュールなので精度がめちゃくちゃ悪い。足をハの字型にしてインチピッチに無理やり合わせている状態である。中華製の場合や安さと引き換えに精度が悪いことを許容する必要がある。

これを以前購入していた、

![IMG_0025](img/IMG_0025.jpeg)

のような密閉可能なケースに入れ、太陽電池の裏に貼り付けて使用するようにした。これで完全自立型の計測ボックスができるはずである。

ここまで書いてきてなんだが、太陽電池が5V出力のものがあれば初段のDC-DCコンバーターは省略できるのである。が、しかし、この**5V出力可能な太陽電池**というのが曲者で、まともな商品が皆無に近いというオチであった。なので、このように初段の4.5V〜28V入力のDC-DCコンバーターが必要になるのである。

で、このケース大きさ自体はOKそうなのだがボードをどうやって固定するかと言うのが結構難問である。完全に固定してしまうと不具合があったときにケースから取り出せなくなるのはまずい。

そこで、

![IMG_0026](img/IMG_0026.jpg)

こんなもので固定できないかと考えている。これならば剥がしたいときは簡単に剥がせる。

![IMG_0028](img/IMG_0028.jpeg)

実際に設置したのが上の写真。太陽電池はキャンプ用の小型の椅子に結束バンドで固定し、足の部分にはコンクリートブロックを乗せて風などで飛ばないようにしている。太陽電池下のプラスチックボックスが今回作成した本体。実際の配線には両ピンタイプのジャンパ線ではなく、単線の両端を剥いてコの字型に曲げたジャンプワイヤで配線したものを使用した。ちなみに太陽電池は12V,5Wのもので2,280円で購入したもの。

![esp32_outer4_boot-pinpoint=1615691423,1615799423](img/esp32_outer4_boot-pinpoint=1615691423,1615799423.png)

で、太陽電池で駆動してみると上のグラフのように、動作はするもののdeep_sleepせずにリセットしてしまうような動作をするようになった。消去法で電源関係の各モジュールを交換しながら検証したところ、どうやらリチウムイオン電池充電用のモジュールが不良品だったようだ。DC5Vを外部から供給している状態ではきちんと動作するが、バッテリーだけの状態の時動作が不安定になっているようだ。とりあえず別のモジュールに交換して様子を見ることにする。

とりあえず、バッテリー駆動が正常に動作することは確認できたので、再度太陽電池をつないでみたところ、症状が再発した。やはり太陽電池側に問題がありそうである。以前購入した太陽電池は逆流防止のショットキーバリアダイオードがついていたのだが、今回購入したものについているかどうかは不明。なんとなくこのあたりが問題なような気がする。なので逆流防止ダイオードを入手して再チャレンジすることにする。

![esp32_outer4_boot-pinpoint=1615845622,1615953622](img/esp32_outer4_boot-pinpoint=1615845622,1615953622.png)

ショットキーバリアダイオードが届いたのでこれを噛ませて接続したのが上のグラフ。水曜日12:00頃に接続したのだが上のようなリセットを繰り返す動作は見られなくなった。ただ、届いたダイオードが予想外に大きく、ブレッドボードに刺さらないので、再度、小さめのダイオードを注文した。

![IMG_0031](img/IMG_0031.JPG)

実際にダイオードを追加したのが上の写真。太い赤のラインの下に少し見える黒いシミのようなものがそれ。これではわかりにくいので、別に作ったボードの写真が、

![IMG_0030](img/IMG_0030.JPG)

これ。紫と白のラインが太陽電池からの配線でダイオードを介してDC-DCコンバータにつながっているのがわかると思う。

これを外に出して、スタンドアロンの測定器として運用を開始した。あとは太陽電池の発電量に合わせた測定頻度の調整を行えばOKである。

数週間駆動テストを行った結果、全体的には満足できる状況であったが、太陽電池が供給できる電力に比べて消費電力がやや大きく、悪天候が続いた場合、電力不足になり測定が中断することがあった。

![esp32_outer4_temp-pinpoint=1617563422,1617671422](img/esp32_outer4_temp-pinpoint=1617563422,1617671422.png)

22:00頃からフラットになっている部分が電力不足で測定できなかった期間だと思われる。ということで、測定間隔を10分から30分に変更してテストを継続している。

別の同一構成の電源モジュールでテストしていたところ、12V電源を繋がずにリブートした場合にWi-Fiが接続できずにリブートを繰り返しているような現象が発生した。どうもWi-Fi動作時に電圧降下が発生している疑いがあるので、試しに4.1V->5V昇圧用の電源部分に680μFのコンデンサを追加してバッテリー駆動を行ってみた。すると、ビンゴ！。正常に立ち上がるようになった。

また、この状態で稼働テストを行うことにする。

680μFのコンデンサを追加してみたところあまり効果がなかったので、以前やってみたように0.1Fの空気二重層コンデンサを680μFの代わりに使用してみた。

![esp32_outer4_boot-pinpoint=1618077634,1618185634](img/esp32_outer4_boot-pinpoint=1618077634,1618185634.png)

その稼働状況が上のグラフ。48回でリセットされているのは意図的なものでエラーなく稼働していることを示している。ただ、どうしても個体差などがあるようでこの方式でOKかどうかはまだよくわからない。

![IMG_0036](img/IMG_0036-1618332410594.JPG)

いずれにしろ、ESP32の消費電力をDC-DCコンバータの出力でまかないきれないのが原因なので、電解コンデンサーは5V出力側に移動し、DC-DCコンバータの入力は空気二重層コンデンサーのみにした。

この構成でバッテリーのみで駆動した場合の死活グラフは以下のようになる。動作間隔は１０分間である。

![esp32_outer3_boot-pinpoint=1618358725,1619049925](img/esp32_outer3_boot-pinpoint=1618358725,1619049925.png)

本来100回でリセットされるグラフが数十回でリセットされるようになったが、測定動作自体は正常に行われており、現時点で８日間バッテリーのみで駆動できている。これは天候が悪くても一週間程度は電池のみで駆動できることを意味する。また死活が0になったあとも、たまに測定が飛ぶことはあってもほぼ正常に測定できており、全く測定できなくなるまではもう何日間か駆動できるようだ。

しかし、別のセンサー(DHT22)を使用してバッテリー駆動を行った場合、40時間程度しか持たなかった。上の時間はあくまでもBME280を使用した場合の駆動時間である。どうしてこれほど違うのか考えてみて思い当たるのはプルアップ抵抗である。試しにプロアップ抵抗なしでも動作するようなのでプルアップ抵抗を外して再度テスト中である。

電源に追加した680μFのコンデンサであるが、どうやらこれでも足りなかったようだ。

![esp32_outer3_boot-pinpoint=1620095729,1620203729](img/esp32_outer3_boot-pinpoint=1620095729,1620203729.png)

02:00から08:00あたりまでフラットになっているのがバッテリーのみで駆動した場合の死活の値である。この時、Wi-Fiは外向けのAPに接続していたため、

![esp32_outer3_rssi-pinpoint=1620095729,1620203729](img/esp32_outer3_rssi-pinpoint=1620095729,1620203729.png)

Wi-FiのRSSIの値はかなり弱い状態になっている。そのため、Wi-Fi接続もしくはNTPでの時刻取得時にエラーが発生し、数回リセットが掛かった後、正常に動作する状況になっていたのではないかと思われる。

コンデンサを1000μFに交換した後の状況が、16:00以降の40分の死活になっている。~~またRSSIも同時間帯に少し改善しているのではないかと思われる。~~

このような状況が発生するのは、RSSIが-70DBmを切ることが多い場合であり、信号強度が大きい場合には発生しないため、設置場所や電源供給状態で異なる動作になるという面倒な状態になっていたようだ。

