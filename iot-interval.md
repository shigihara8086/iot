[TOC]

# ■ 間欠動作用プログラム
----

間欠的に**Raspberry Pi**を起動・停止するには次のようなスクリプトをrootのcronで起動するようにする。

[cronの設定]
```text
# m h  dom mon dow   command
@reboot     ./pwled.sh
0 */1 * * * ./pwoff.sh
```

`@reboot`の行は起動時に.`/pwled.sh`を起動することを意味する。
`./pwoff.sh`は１時間に１回起動するようになっている。

## ▼ pwled.sh

```shell
#!/bin/bash
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x01 i > /dev/null 2>&1  #LED点灯
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
```

**Raspberry Pi zero**に**RPi1114FDH**をかぶせてしまうと、パイロットランプが見にくくなるので**RPi1114FDH**のランプで代行できるように**Raspberry Pi zero**が起動したらLEDを点滅から点灯になるようにする。電源を落とす前に点滅になるように制御することでスタンバイ時と稼働時の見分けがつくようにしている。

`i2cset`コマンドがフルパスで書いてあるのは、cron実行時のパスに`/usr/sbin`が含まれていないためである。また、コマンドの送信はエラーになることがあるので、成功するまで送信するようにしている。

## ▼ pwoff.sh

```shell
#!/bin/bash
# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

/bin/date >> ./date.log

offtime=`hexsec 30`
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x11 0x00 0x10 $offtime 0x00 i > /dev/null 2>&1 #30秒後にoff
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
#
pontime=`hexsec 3510`
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x10 0x00 0x10 $pontime 0x00 i > /dev/null 2>&1 #3600-90秒後にon
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
#
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x02 i > /dev/null 2>&1  #LED点滅
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
#
/sbin/poweroff
```

hexsecはシェルの関数で、10進数から**RPi1114FDH**に渡すための16進表記の文字列に変換する。
`cron`でこのスクリプトが呼び出されると、`i2cset`コマンドで、

* 30秒後に電源を切る
* 3510秒(58分30秒)後に電源を入れる
* LEDを点滅にする

という指示を**RPi1114FDH**に発行する。連続して送り出すとエラーになるので、`sleep 1.0`でディレイを取りながらコマンドを発行する。その後、`/sbin/poweroff`で**Raspberry Pi**のシャットダウンを行う。シャットダウンには25秒ほどかかるので、シャットダウン後に電源が切れるようにタイミングを取って30秒後に電源が切れるように先に指示を出しておくことで正常にシャットダウンを行えるようにしている。

この後3510秒経つと、**Raspberry Pi**の電源供給が再開するので、**Raspberry Pi**は起動する。起動は`cron`で設定した時刻の前までに起動シーケンスが終了している必要がある。起動シーケンスが間に合わない場合は、`3510`の秒数を減らすなりしてタイミングを合わせる必要がある。

**RPi1114FDH**には低消費電力の[**CPU(ARM Cortex-M0)**](http://akizukidenshi.com/catalog/g/gI-10224/)(これが製品名の数字の由来)が常時動作しており、`i2cset`で発行されたコマンドを認識し、その動作を行うようになっている(しかし32bitのCPUが190円だと!!)。

このように**RPi1114FDH**が起動している限り**Raspberry Pi**とは非同期に電源管理のプログラムが動作しているので、簡単なスクリプトではあるが、動作タイミングを間違えないように注意する必要がある。

![タイミングチャート](./img/timing.svg)

上記の設定で動作させた場合、測定にかかる時間を無視すると、**Raspberry Pi**の稼働時間は**１回あたり約２分**になるため、連続動作に比べれば、消費電力は**1/30**になる計算である(起動時は通常時に比べて電力を消費するのであくまで計算上であるが)。

## ▼ pwsleep.sh

```shell
#!/bin/bash
# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

offtime=`hexsec 30`
echo "30秒後に電源を切ります。"
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x11 0x00 0x10 $offtime 0x00 i > /dev/null 2>&1 #30秒後にoff
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
#
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x02 i > /dev/null 2>&1  #LED点滅
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
/sbin/poweroff
```

`pwsleep.sh`は`pwoff.sh`から測定部分と次回の起動タイマの設定を除いたスクリプトである。このシステムは一度起動してしまうと、大部分の時間は電源がOFFになっているので、設定を変えるなどの操作を行いたい場合は、**RPi1114FDH**上の電源スイッチで手動で電源を入れる必要がある。この時単に`poweroff`を行ってしまうと、**Raspberry Pi**の電源供給が切れない状態にで次の起動タイミングまで無駄に動いたままになってしまうので、30秒後に電源OFFになるように設定を行ってから`poweroff`でシャットダウンするようにしている。

## ▼ pwon.sh

```shell
#!/bin/bash
# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

sec=`expr $1 \* 60`
pontime=`hexsec $sec`
echo "$1分($sec秒)後に起動します。"
while :
do
  /usr/sbin/i2cset -y 1 0x28 0x10 0x00 0x10 $pontime 0x00 i > /dev/null 2>&1 #指定秒後にon
  res=$?
  sleep 0.3
  if [ $res = 0 ]; then
    break
  fi
done
```

`pwon.sh`は何らかの原因(おそらくタイミング指定のミス)で起動タイミングが狂ってしまったときに次の起動タイミングを再設定するコマンドである。引数に起動までの秒数を指定する。

