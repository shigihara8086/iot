#!/bin/bash
# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}

`./bme280-csv.py >> bme-data.csv`
wget -q -O /dev/null `./bme280-api.py`
