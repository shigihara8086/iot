#!/bin/bash

if [ "$1" = "autoconf" ]; then
  echo yes
  exit 0
fi

if [ "$1" = "config" ]; then

  echo 'graph_title Room Temperature'
  echo 'graph_args --base 1000 -l 0'
  echo 'graph_vlabel Celsius'

  echo 'graph_scale no'
  echo 'graph_category Measure'

  echo 'temp.label MyRoom'
  print_warning temp
  print_critical temp

  echo 'graph_info Room Temperature'
  echo 'temp.info /usr/local/bin/temper-poll'

  # Last, if run with the "config"-parameter, quit here (don't
  # display any data)
  exit 0
fi


echo -n "temp.value "
/usr/local/bin/temper-poll -c
