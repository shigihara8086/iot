#!/usr/bin/python

import time
import datetime
import commands

#
def GetV():
	check = commands.getoutput("/usr/sbin/i2cget -y 1 0x40 0x02 w")
	return (int("0x"+check[4:6],16)*256+int("0x"+check[2:4],16))*1.25/1000
#
def GetA():
	check = commands.getoutput("/usr/sbin/i2cget -y 1 0x40 0x04 w")
	if int(check[4:6],16)<128:
		return (int("0x"+check[4:6],16)*256+int("0x"+check[2:4],16))
	else:
		return (int("0x"+check[4:6],16)*256+int("0x"+check[2:4],16)-256*256)

#
check = commands.getoutput("/usr/sbin/i2cset -y 1 0x40 0x05 0x0a 0x00 i")
#
v = 0
for var in range(0,10):
	v = v + GetV()
	time.sleep(0.3)
#
print "%.2f" % (v / 10)
exit
