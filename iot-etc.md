[TOC]

# ■ おまけ
----

## ▼ Tinker Boardを手に入れた

[**Tinker Board**](https://www.asus.com/jp/Single-Board-Computer/Tinker-Board/)は**Raspberry pi**のパクリとも言えるシングルボードコンピュータである。**Raspberry pi**と比べると、CPU、GPU、LANなどが強化されていて、**Raspberry pi**では少し能力不足かと思われる用途に好適である。

OSは[**Tinker OS**](https://tinkerboarding.co.uk/wiki/index.php?title=Software)等があるが、**Raspberry pi**に比べるとまだ未完成な部分も多く、利用する際には注意が必要である。

強化点を見る限り、LANのスピードが重要になるNASなどの用途に好適であると思われる。
実際にUSB-HDDを接続してNASを構成してみたところ、**Raspberry pi2**と比較して、**約2.5倍**程度のパフォーマンスの向上(具体的にはファイルの転送速度が**10MB/s**程度から**25MB/s**程度に向上した)が見られた。

## ▼ **Raspberry pi zero**を**OTGスレーブモード**で使う

**Raspberry pi zero**はネットワーク機能がないので、通常は**Wi-Fiドングル**や**USB-NICアダプタ**を接続して使用することになる。しかし、裏技として[**Raspberry Pi ZeroをUSBケーブル1本で遊ぶ**](http://www.raspi.jp/2016/07/pizero-usb-otg/)といった方法があるようだ。この設定を行うと、PCに**Raspberry pi zero**をつなぐと**Raspberry pi zero**PCからは**USB-NICアダプタ**として認識される。そのため、USBケーブル一本で給電とネットワーク接続が可能になるという次第である。実際にやってみたところ、

```text
usb0      Link encap:イーサネット  ハードウェアアドレス 06:b6:5f:69:28:92
          inetアドレス:192.168.137.100 ブロードキャスト:192.168.137.255  マスク:255.255.255.0
          inet6アドレス: 2408:213:26e1:9ef1:6c47:3111:c970:2e0a/64 範囲:グローバル
          inet6アドレス: fe80::af18:9c5:7460:31ce/64 範囲:リンク
          UP BROADCAST RUNNING MULTICAST  MTU:1500  メトリック:1
          RXパケット:1051 エラー:0 損失:6 オーバラン:0 フレーム:0
          TXパケット:433 エラー:0 損失:0 オーバラン:0 キャリア:0
          衝突(Collisions):0 TXキュー長:1000
          RXバイト:122453 (119.5 KiB)  TXバイト:75587 (73.8 KiB)
```

のように、USB経由で接続することができた。IPアドレスは自動で割り振られる。このアドレスはPC、**Raspberry pi zero**間のみのローカルアドレスになるので、PC側で**インターネット共有**の設定を行えば**Raspberry pi zero**からPCを経由してインターネットにアクセスすることができる。

```text
pi@pi-zero:~ $ traceroute www.sgmail.jp
traceroute to www.sgmail.jp (153.126.128.89), 30 hops max, 60 byte packets
 1  Win10.mshome.net (192.168.137.1)  0.190 ms * *
 2  * * *
 3  ntt.setup (192.168.254.1)  0.587 ms  0.589 ms  0.746 ms
 4  r017.fksmnt01.ap.so-net.ne.jp (219.98.227.14)  7.058 ms  7.138 ms  8.364 ms
 5  tn01gi2.fksmnt01.ap.so-net.ne.jp (116.67.128.65)  5.936 ms  5.777 ms  5.626 ms
 6  note-13Vl547.net.so-net.ne.jp (110.67.191.97)  11.542 ms  11.891 ms  11.702 ms
 7  202.213.194.1 (202.213.194.1)  11.773 ms  12.185 ms  11.948 ms
 8  202.213.193.35 (202.213.193.35)  12.224 ms  12.437 ms  12.874 ms
 9  tkort2-as2527-10g.bb.sakura.ad.jp (157.17.131.17)  11.924 ms  12.223 ms  12.064 ms
10  tkort3-ort2.bb.sakura.ad.jp (157.17.130.122)  11.978 ms  11.791 ms  11.619 ms
11  iskrt3-tkort3.bb.sakura.ad.jp (157.17.131.34)  30.567 ms  31.164 ms  31.115 ms
12  iskrt2s-rt3.bb.sakura.ad.jp (103.10.113.114)  30.706 ms  31.043 ms iskrt1s-rt3.bb.sakura.ad.jp (103.10.113.110)  30.994 ms
13  iskrt101b-rt2s-1.bb.sakura.ad.jp (103.10.113.18)  31.168 ms  31.191 ms iskrt102b-rt1s-2.bb.sakura.ad.jp (103.10.113.98)  31.149 ms
14  iskrt125e-rt101b.bb.sakura.ad.jp (103.10.114.210)  31.042 ms  31.175 ms  34.637 ms
15  ik1-300-10085.vs.sakura.ne.jp (153.126.128.89)  31.188 ms  31.896 ms  31.614 ms
```

tracerouteでwww.sgmail.jpまでの経路を見ると、最初の`Win10.mshome.net`がゲートウエイとして動いているPCとして認識されている。ホスト名がFQDNで表示されているので、DNSもきちんと引けている状態である。

## ▼ Arduino Yun

IoTデバイスの代表格といえば、**Raspberry pi**と並んで**Arduino**が有名である。**Arduino**はどちらかと言うと、あまり通信機能が強くないという印象があったのと、価格が結構高い(コストパフォーマンスが低い)と思っていたため、敬遠していたのだが、[**pcDuino Linux Dev Board**](http://akizukidenshi.com/catalog/g/gM-06931/)なるものを見つけた。詳細はよくわからない部分もあるのだが、価格も比較的安く**Ubuntu**がインストール済みということで、食指が動いたので、購入してみた(安い思ったら在庫処分品だった)。

で、色々調べていたら、どうやら[**Arduino YUN**](http://akizukidenshi.com/catalog/g/gM-08901/)というボードがあり、**Raspberry pi**のように**1CPU**で全て賄うのではなく、**Aruduino**に**Linuxチップ**を搭載した**2CPU**構成になっているようだ。**Linuxチップ**は基本的に**Wi-Fiデバイス**として搭載されているようで、なんだか[**食玩**](https://www.wikiwand.com/ja/%E9%A3%9F%E7%8E%A9)みたいな構成になっている。あまり期待はできないが、もし**Arduino**部分から**Linuxチップ**の電源制御ができれば、IoTには非常に都合が良いと思う。

## ▼ GitBucketを使ってみた

現在、このドキュメントは**BitBucket**を使って管理しているが、**GitBucket**というプライベートに立てることができる**Gitクローン**を見つけたので、使ってみた。

- [GitBucketで立てたサイト](http://sgmail.jp:8080/root/rpi-iot)

非常に簡単に立てることができるので、公開する気のないプロジェクトはこれを使って管理するのも良いと思う。

## ▼ Dynamic DNSとIPv6ではまった

最近、[So-net](http://www.so-net.ne.jp/)から「[ダイナミックDNSサービス　サービス終了のお知らせ](https://www.so-net.ne.jp/info/2017/op20170317_0018.html)」というメールが届いたのが事の顛末の始まりであった。使っているユーザーが少ないのかどうなのかはわからないが、廃止されてしまうわけだ。

うーむ、困った。固定アドレスがないと不便なので、仕方がないので、[固定IPアドレス割当てサービス](http://www.so-net.ne.jp/staticip/entry/)を申し込んで使用していたのだが、ふとIPv6アドレスの割当を見ると、`2408:212:8043:5500:2011:802a:453d:280`のようなアドレスになっているではないか。`2408:`で始まっているってことはNTTの閉域網のアドレスになっているわけで、これではインターネットには接続できない。

なぜだ？と思ったが、固定IPの利用とIPv6アドレスの確認の間のタイムラグがあったため、この２つが関係しているとは思っていなかったのだが、ひょっとしてと思い、[MyDNS.JP](https://www.mydns.jp/)を利用して、固定IPの利用をやめることにした。試行錯誤は多少あったが、無事、**rodeo.mydms.jp**に現在使用しているアドレスが割り当てられて利用できるようになり、**munin**も無事**リバースプロクシ**経由で[https://sgmail.jp/munin/](https://sgmail.jp/munin/)を参照できるようになった。

ところが、翌日アクセスして見ると、**アクセスに非常に時間がかかるようになり**、場合によっては**タイムアウト**になってしまう。うーむ、昨日から変えたところはないのになあ？と思いつつ試行錯誤。この時点でわけがわからない状態ですね。で、最終的に**sgmail.jp**からマニュアルで

```text
wget http://rodeo.mydns.jp/munin/
```

でアクセスしたら、なんと**IPv6**でアクセスしているではないか。だめだった時の記録はないので、通信できる時の結果を載せるが、

```text
$ wget http://rodeo.mydns.jp/munin/
--2017-09-09 01:37:37--  http://rodeo.mydns.jp/munin/
rodeo.mydns.jp (rodeo.mydns.jp) をDNSに問いあわせています... 240d:0:2902:4600:b758:e387:1f90:6ae6, 218.110.48.65
rodeo.mydns.jp (rodeo.mydns.jp)|240d:0:2902:4600:b758:e387:1f90:6ae6|:80 に接続しています... 接続しました。
HTTP による接続要求を送信しました、応答を待っています... 200 OK
長さ: 8924 (8.7K) [text/html]
`index.html' に保存中

index.html                    100%[=================================================>]   8.71K  --.-KB/s    in 0s

2017-09-09 01:37:37 (92.3 MB/s) - `index.html' へ保存完了 [8924/8924]
```

だとな。どうやら**MyDNS.JP**は**IPv6**にも対応しているようだ。しかし、**IPv6**なら届いて良いはずなのになあ？と思いつつ、ふとNTTのルータである**RP-400KI**のランプを見るとIPv6で接続している時の`オレンジのランプ`が点灯していない。むむむ、ここでまた悩む。で、設定を色々見ていったら、

**トップページ ＞ 詳細設定 ＞ セキュリティ設定 ＞ IPv6パケットフィルタ設定（IPoE）**

```text
IPv6パケットフィルタ設定機能にて、
IPv6通信に関するファイアウォール機能の「有効」／「無効」の設定、
及びIPv6通信のセキュリティレベルを
「標準」／「高度」の二種類から選択することができます。

IPv6ファイアウォール機能が「有効」でかつ、セキュリティレベルが「標準」の場合、
NTT東日本・NTT西日本のフレッツ光ネクスト網内で折り返す通信
（NTT東日本・NTT西日本との契約により可能となるもの）は許容し、
その他のIPv6通信を使用したインターネット側からの通信を拒否します。

※セキュリティレベルが「高度」の場合は、
NTT東日本・NTT西日本のフレッツ光ネクスト網内で折り返す通信
（NTT東日本・NTT西日本との契約により可能となるもの）を拒否します。

IPv6ファイアウォール機能を「無効」にした場合、
NTT東日本・NTT西日本のフレッツ光ネクスト網内で折り返す通信
（NTT東日本・NTT西日本との契約により可能となるもの）を許容し、
かつその他のIPv6を使用したインターネット側からの通信を許容します。
IPv6ファイアウォール機能を「無効」にした場合、
IPv6パケットフィルタ設定（IPv6 PPPoE）も無効となりますのでご注意ください。
IPv6ファイアウォール機能を「無効」に設定することで、
LANに接続した機器が危険にさらされる可能性がありますので、
設定する際は十分注意してください。

[ IPv6セキュリティのレベル ]
IPv6ファイアウォール機能  ◉有効  ◎無効
IPv6セキュリティのレベル  [標準]
```

と**赤**で表示しているのを発見。回りくどく書いてあるが、

- IPv6ファイアウォール機能[有効]だとNTTの閉域網しか使えない
- IPv6でインターネットを使うならIPv6ファイアウォール機能は[無効]にせよ

と言っているわけだ。うーみゅ、これかあ、しかし以前はこんな表示見たことがなかった、と思いつつファイアーウォール機能をOFFにしたら、**ビンゴ**、**sgmail.jp**から**rodeo.mydns.jp**にアクセスできるようになった。はまった部分を列挙すると、

- So-netの固定IPを使うとIPv6の割当がなくなる
- NTTのルータはデフォルトでグローバルなIPv6を叩き落としている
- 最近のDynamicDNSサービスはIPv6にも対応している
- さくらインターネットでUbuntuを立てると現時点ではデフォルトでIPv6が有効になっている

といったことが複合的に絡み合っているわけだ。

なんとかなったけど、これだけ原因が複合していると問題解決はなかなか大変だ。

## ▼ うはー、壊れた！

太陽電池駆動用のバッテリーが**6Ah**のものだと、少し大きすぎて小さめの筐体には入らないなあと思っていたので、もう少し小さい、[1.2Ahのバッテリー](http://akizukidenshi.com/catalog/g/gB-06743/)に変更することにした。

で、バッテリーを交換したら**起動しない**。ぐわーん、どうしたものかと思いつつ、色々試行錯誤して試していたら、**INA226モジュールから煙が！**。一瞬で、**RPi1114**、**Raspberry pi zero W**もろともお亡くなりになりました。

電源容量不足かと思い、**12V->5Vダウンコンバータ**を試しているときに壊れたので、多分もろに**INA226モジュール**の計測部分に**12V**をチャージコントローラの12Vラインに繋いでしまったのが失敗だった。使っていた**INA226モジュール**が非アイソレーションタイプだったため、これで異常な電圧がかかって焼けてしまったと思われる。

まあ、電源系をいじりまわしていれば、ありうることとは言え、壊れてしまったのはちょっと悲しい。幸い、もう１セット作るだけの機材はあるのでなんとかなるが、ちょっと財布に痛い出来事でした。

で、新しく作ったのがこれ。

![new-set](./img/new-set.jpg)

- バッテリーを小さいものに変更(5Ah→1.2Ah)
- DC-DCコンバータを最大5Aのものに変更し出力電圧を5.2V程度に設定
- PIX-MT100の電源をRPi1114から取るように変更

現在はこの構成で動作の安定性を検証中である。

## ▼ メール送信に何を使うか？

測定データは、**web-api**などを使ったり、**munin**などのプロトコルを使用すれば良いが、何らかの通知を行いたい場合には、やはりメールが使えないと困る。最初は**postfix**を入れていたが、もっと軽く簡単なものがないかと思い探したところ、**ssmtp**と言うパッケージがあったので**apt**で入れた。

```text
$ sudo apt install ssmtp mailutils
```

設定は、

**/etc/ssmtp/ssmtp.conf**

```conf
#
# Config file for sSMTP sendmail
#
root=a.shigi@gmail.com
mailhub=smtp.gmail.com:587
hostname=rodeo.mydns.jp
AuthUser=a.shigi@gmail.com
AuthPass=<パスワード>
UseSTARTTLS=YES
```

といった設定をすれば良い。

`/usr/sbin/sendmail`が使えるので、

**メール送信スクリプト**

```shell
#!/bin/bash
LANG=C
DATE=`date +'%a, %d %b %Y %H:%M %z'`
/usr/sbin/sendmail -t <<EOS
From: a.shigi@gmail.com
To: shigi8086@gmail.com
Date: ${DATE}
Subject: ${HOSTNAME} Force power off
Content-Type: text/plain; charset="UTF-8"

バッテリーが切れるためシャットダウンします。
EOS
```

といったスクリプトでメールを送ることができる。

## ▼ 定番Arduinoクロック

**I2C**および**1-Wire**のテストということで、**Arduino nano**、**RTC(DS1703)**、**1-Wire温度計(DS18B20)**、**I2C接続20桁x4行LCD**を組み合わせて日時と温度を表示する簡単な時計を作ってみた。

![簡単な時計](./img/img_0137.jpg)

この時計は、以下のようなスケッチで動作している。

**Arduinoクロック**

```C
/* Demonstration sketch for PCF8574T I2C LCD Backpack 
Uses library from https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads GNU General Public License, version 3 (GPL-3.0) */
#include <Time.h>
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include "RTClib.h"
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 10 // データ(黄)で使用するポート番号
#define SENSER_BIT    9 // 精度の設定bit
// #define OFFSET 9

RTC_DS1307 RTC;
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7); // 0x27 is the I2C bus address for an unmodified backpack
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

void setup() {
  Serial.begin(115200);
  Wire.begin();
  RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    #ifdef OFFSET
    DateTime compile_time = DateTime(__DATE__,__TIME__);
    DateTime now (compile_time.unixtime() + OFFSET);
    RTC.adjust(now);
    #endif
  }
  #ifdef OFFSET
  DateTime pc_time = DateTime(__DATE__,__TIME__);
  DateTime now (pc_time.unixtime() + OFFSET);
  RTC.adjust(now);
  #endif
  // activate LCD module
  lcd.begin(20,4); // for 20 x 4 LCD module
  lcd.setBacklightPin(3,POSITIVE);
  lcd.setBacklight(HIGH);
  // lcd.setCursor(0,0);        // go to start of 1st line
  // lcd.print("====Simple clock====");
  sensors.setResolution(SENSER_BIT);
}

void loop() {
  DateTime now = RTC.now();
  lcd.setCursor(0,0);
  lcd.print("Date ");
  lcd.print(now.year(),DEC); lcd.print('/');
  if(now.month() < 10) { lcd.print("0"); } lcd.print(now.month(),DEC); lcd.print('/');
  if(now.day()   < 10) { lcd.print("0"); } lcd.print(now.day(),  DEC);
  lcd.print('(');
  switch(now.dayOfWeek()) {
    case 0: lcd.print("Sun"); break;
    case 1: lcd.print("Mon"); break;
    case 2: lcd.print("Tue"); break;
    case 3: lcd.print("Wed"); break;
    case 4: lcd.print("Thu"); break;
    case 5: lcd.print("Fri"); break;
    case 6: lcd.print("Sat"); break;
    default:
      lcd.print("***");
  }
  lcd.print(')');
  //
  lcd.setCursor(0,1);
  lcd.print("Time ");
  if(now.hour()   < 10) { lcd.print("0"); } lcd.print(now.hour(),  DEC); lcd.print(':');
  if(now.minute() < 10) { lcd.print("0"); } lcd.print(now.minute(),DEC); lcd.print(':');
  if(now.second() < 10) { lcd.print("0"); } lcd.print(now.second(),DEC);
  //
  lcd.setCursor(0,2);
  lcd.print("Temp ");
  sensors.requestTemperatures();
  lcd.print(sensors.getTempCByIndex(0),1);
  lcd.print("\xdf"); lcd.print("C");
  delay(100);
}
```

