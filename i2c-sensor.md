[TOC]

# ■ I2Cで使えるセンサー

----

マイコンやRaspberry Piではセンサー用のデジタルインターフェースとしてI2CやSPIと呼ばれる比較的低速なシリアルインターフェースを使用することが一般的である。もちろん、ADCなどのアナログインターフェースも使用できるが、ノイズや精度を考慮すると、デジタルインターフェースを使用するほうが、簡単かつ高精度な測定ができる。

ここでは、I2Cに対応したセンサーにどのようなものがあるかをベンダー毎に紹介する。もちろんこれらのセンサーは以下のベンダー以外からも購入できる。

### ▼ [秋月電子通商](http://akizukidenshi.com/catalog/)

老舗の部品商社なので、商品数は豊富にある。I2Cで使えそうなセンサーをリストアップしたが、これ以外にもいろいろおもしろいセンサーやモジュールがあるので、色々探索してみるのも面白い。

[ＡＤＴ７４１０使用　高精度・高分解能　Ｉ２Ｃ・１６Ｂｉｔ　温度センサモジュール](http://akizukidenshi.com/catalog/g/gM-06675/)

[シャープ測距モジュール　ＧＰ２Ｙ０Ｅ０３　（Ｉ２Ｃ＆アナログ出力）](http://akizukidenshi.com/catalog/g/gI-07547/)

[３軸加速度センサモジュール　ＡＤＸＬ３４５（ＳＰＩ／ＩＩＣ）](http://akizukidenshi.com/catalog/g/gM-06724/)

 [Ｉ２Ｃ接続ＲＴＣ（リアルタイムクロック）ＤＳ１３０７＋](http://akizukidenshi.com/catalog/g/gI-06949/)

[温湿度センサ　モジュール　ＡＭ２３２０](http://akizukidenshi.com/catalog/g/gM-08663/)

[温湿度センサ　モジュール　ＡＭ２３２２](http://akizukidenshi.com/catalog/g/gM-10880/)

 [ＤＳ１３０７　Ｉ２Ｃリアルタイムクロックモジュール（ＲＴＣ）](http://akizukidenshi.com/catalog/g/gM-09874/)

[ＴＣＳ３４７２５使用　カラーセンサーモジュール（白色ＬＥＤ搭載）](http://akizukidenshi.com/catalog/g/gM-08220/)

[ＩＮＡ２１９使用　電流センサーモジュール（カレントセンサー）](http://akizukidenshi.com/catalog/g/gM-08221/)

[ＢＭＰ０８５使用気圧センサモジュール（Ｉ２Ｃインタフェース）](http://akizukidenshi.com/catalog/g/gM-08764/)

[ＭＳ５６０７高度計測モジュール](http://akizukidenshi.com/catalog/g/gM-05399/)

[３軸加速度センサモジュール　ＫＸＳＤ９－２０５０](http://akizukidenshi.com/catalog/g/gI-03824/)

[ＢＭＥ２８０使用　温湿度・気圧センサモジュールキット](http://akizukidenshi.com/catalog/g/gK-09421/)

### ▼ [ストロベリーリナックス](https://strawberry-linux.com/catalog/)

秋月電子通商では扱っていないセンサーなどがある。社名の通りRaspberry Pi用のモジュールなどを扱うことが多いのだが、I2Cなどは汎用のインターフェースなので、ESP32やESP8266などのマイコン系でも使用できるのもが豊富にある。

[LPS33HW 【防水】気圧センサモジュール(I2C/SPIタイプ) ](https://strawberry-linux.com/catalog/items?code=12133)

[INA226iso 絶縁I2Cディジタル電流・電圧・電力計モジュール(20Aタイプ)](https://strawberry-linux.com/catalog/items?code=11226)

[MCP9600 熱電対温度センサモジュール（高耐熱熱電対付）](https://strawberry-linux.com/catalog/items?code=19600)

[ディジタル・コンパスモジュール HMC6352](https://strawberry-linux.com/catalog/items?code=18038)

[VL53L1X TOFレーザー測距センサモジュール](https://strawberry-linux.com/catalog/items?code=15311)

[MPU-9250 ９軸センサモジュール(３軸加速度＋３軸ジャイロ＋３軸コンパス)](https://strawberry-linux.com/catalog/items?code=12250)

[CCS811 エアークオリティセンサモジュール](https://strawberry-linux.com/catalog/items?code=11811)

[TMP006 超小型赤外線サーモパイルセンサモジュール](https://strawberry-linux.com/catalog/items?code=12006)

[VCNL4000 赤外線エミッタ・ブレイクアウト](https://strawberry-linux.com/catalog/items?code=18196)

[音声合成モジュール](https://strawberry-linux.com/catalog/items?code=32003)

[BME680 温湿度・気圧・ガスセンサモジュール](https://strawberry-linux.com/catalog/items?code=12680)

[IQS624 ProxFusion 磁気センサモジュール](https://strawberry-linux.com/catalog/items?code=12624)

[SHT-35 １チップ高精度温度・湿度センサ・モジュール](https://strawberry-linux.com/catalog/items?code=80035)

### ▼ [スイッチサイエンス](https://www.switch-science.com/)

多くは秋月電子通商やストロベリーリナックスで扱っているものだが、たまに変なセンサーを扱っている。

[LSM303D搭載 3Dコンパス(加速度・地磁気センサ)](https://www.switch-science.com/catalog/1580/)

[S-300L-3V CO₂センサモジュール](https://www.switch-science.com/catalog/3685/)

[心拍・酸素飽和度モニタモジュール MAXREFDES117#](https://www.switch-science.com/catalog/3208/)

[APDS9960搭載 ジェスチャーセンサ（RGB、環境光、近接距離センサ付き）](https://www.switch-science.com/catalog/3531/)

[SI1145 UV指数/赤外線/可視光センサ](https://www.switch-science.com/catalog/1709/)

[OMRON MEMS非接触温度センサD6T-44L-06](https://www.switch-science.com/catalog/1297/)

