#!/usr/bin/ruby

require 'cgi'

cgi = CGI.new()
File.open(File.dirname(__FILE__) + "/" + File.basename(__FILE__,".cgi") + ".dat","w") {|fd|
  cgi.params.each {|k,v|
    fd.puts "#{k}:#{v.join(",")}"
  }
}
cgi.out({'type'=>"text/plain"}) { "DONE" }
