#!/usr/bin/ruby

v = "NaN"
begin
  File.open("/var/www/cgi/esp8266.dat","r") {|fd|
    while(line = fd.gets()) do
      ARGV.each {|k|
        if(line =~ /^#{k}:(.*?)$/)
          v = $1
        end
      }
    end
  }
rescue
end
puts v
