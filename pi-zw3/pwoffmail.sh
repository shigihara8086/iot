#!/bin/bash

/usr/sbin/sendmail -t <<EOS
From: root
To: root
Subject: $HOSTNAME Force power off
Content-Type: text/plain; charset="UTF-8"

バッテリーが切れるためシャットダウンします。

`./battery.sh`
EOS
/usr/sbin/service postfix restart
sleep 10
/usr/sbin/postqueue -f
