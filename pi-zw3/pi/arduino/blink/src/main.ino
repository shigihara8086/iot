#include <MsTimer2.h>

volatile boolean led_on = LOW;
volatile int duty_high = 1;
volatile int duty_low = 9;
volatile int count_high = 0;
volatile int count_low = 0;

void led_handler() {
  if(led_on == HIGH) {
    digitalWrite(LED_BUILTIN, HIGH);
    count_high ++;
    if(count_high > duty_high) {
      led_on = LOW;
      count_high = 0;
    }
  } else {
    digitalWrite(LED_BUILTIN, LOW);
    count_low ++;
    if(count_low > duty_low) {
      led_on = HIGH;
      count_low = 0;
    }
  }
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  MsTimer2::set(100,led_handler);
  MsTimer2::start();
}

void loop() {
  char val;
  if(Serial.available() > 0) {
    val = Serial.read();
    switch(val) {
      case '1': duty_high = 1; duty_low = 9; break;
      case '2': duty_high = 2; duty_low = 8; break;
      case '3': duty_high = 3; duty_low = 7; break;
      case '4': duty_high = 4; duty_low = 6; break;
      case '5': duty_high = 5; duty_low = 5; break;
      case '6': duty_high = 6; duty_low = 4; break;
      case '7': duty_high = 7; duty_low = 3; break;
      case '8': duty_high = 8; duty_low = 2; break;
      case '9': duty_high = 9; duty_low = 1; break;
      default: break;
    }
  }
}
