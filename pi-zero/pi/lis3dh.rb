#!/usr/bin/ruby

require 'rubygems'
require 'i2c'
require 'time'

class LIS3DH
  attr :adjust_x
  attr :adjust_y
  attr :x
  attr :y
  attr :z
  attr :g
  def initialize(path,address = 0x19)
    @adjust_x = 0.0
    @adjust_y = 0.0
    @adjust_z = 0.0
    @x = 0.0
    @y = 0.0
    @z = 0.0
    @g = 0.0
    @device = I2C.create(path)
    @address = address
  end
  def s18(value)
    -(value & 0b100000000000) | (value & 0b011111111111)
  end
  def read()
    # x
    x_l = @device.read(@address,1,0x28).ord
    x_h = @device.read(@address,1,0x29).ord
    x_a = (x_h << 8 | x_l) >> 4
    @x = s18(x_a) / 1024.0 * 980.0
    # y
    y_l = @device.read(@address,1,0x2a).ord
    y_h = @device.read(@address,1,0x2b).ord
    y_a = (y_h << 8 | y_l) >> 4
    @y = s18(y_a) / 1024.0 * 980.0
    # z
    z_l = @device.read(@address,1,0x2c).ord
    z_h = @device.read(@address,1,0x2d).ord
    z_a = (z_h << 8 | z_l) >> 4
    @z = s18(z_a) / 1024.0 * 980.0
  end
  def adjust()
    10.times {|i|
      read()
      @adjust_x = ((@adjust_x * i) + @x) / (i + 1)
      @adjust_y = ((@adjust_y * i) + @y) / (i + 1)
      @adjust_z = ((@adjust_z * i) + @z) / (i + 1)
      sleep(0.1)
    }
  end
  def sampling()
    read()
    @x = @x - @adjust_x
    @y = @y - @adjust_y
    @z = @z - @adjust_z
    @g = Math.sqrt((@x * @x) + (@y * @y) + (@z * @z))
  end
  def logging(fd,sec)
    start = Time.now()
    limit = start + sec
    base  = Time.now().to_f
    loop {
      now = Time.now()
      fnow = now.to_f
      dsec = fnow - base
      sampling()
      fd.printf("%f,%.2f,%.2f,%.2f,%.2f\n",dsec,@x,@y,@z,@g)
      sleep(0.1)
      break if(now > limit)
    }
  end
end

dev = LIS3DH.new("/dev/i2c-1",0x19)
#puts "校正中..."
dev.adjust()
#puts "測定中..."
loop {
  dev.sampling()
  if(dev.g > 100)
    #puts "地震検知!!!"
    date = Time.now().strftime("%Y-%m-%dT%H%M%S")
    File.open("LIS3DH"+date+".csv","w") {|log|
      log.puts "t,x,y,z,g"
      dev.logging(log,30)
    }
    #puts "測定中..."
  else
    sleep(0.1)
  end
}
