#!/bin/bash

maddr="a-shigi@sgmail.jp"

# 10進->16進変換
hexsec () {
  echo -n $1 | od -tx1 -An | sed 's/\s/ 0x/g'
}
# postfix再起動
postfix_restart() {
  /usr/sbin/service postfix restart
  sleep 10
  /usr/sbin/postqueue -f
  sleep 10
}
# LED点滅
led_blink() {
  while :
  do
    /usr/sbin/i2cset -y 1 0x28 0x20 0x00 0x02 i > /dev/null 2>&1
    res=$?
    sleep 0.5
    if [ $res = 0 ]; then
      break
    fi
  done
}
# 電源ON
pw_on() {
  pontime=`hexsec $1`
  while :
  do
    /usr/sbin/i2cset -y 1 0x28 0x10 0x00 0x10 $pontime 0x00 i > /dev/null 2>&1
    res=$?
    sleep 0.5
    if [ $res = 0 ]; then
      break
    fi
  done
}
# 電源OFF
pw_off() {
  offtime=`hexsec $1`
  while :
  do
    /usr/sbin/i2cset -y 1 0x28 0x11 0x00 0x10 $offtime 0x00 i > /dev/null 2>&1
    res=$?
    sleep 0.5
    if [ $res = 0 ]; then
      break
    fi
  done
}

#測定
dt=`date "+%Y/%m/%dT%H:%M:%S"`
vp=`./ina226.py`
wget -q -O /dev/null "https://sgmail.jp/demo/iot-api.cgi?d=$dt&v=$vp"
#
if [ `echo "$vp < 3.50" | bc` == 1 ]; then
  /usr/bin/mail $maddr -s "$HOSTNAME battery ${vp}V" <<EOS
Bad battery voltage.
EOS
  `postfix_restart`
  /sbin/poweroff
else
  `pw_off 30`
  `pw_on 3510`
  `led_blink`
  /sbin/poweroff
fi
