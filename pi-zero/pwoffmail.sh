#!/bin/bash
LANG=C
DATE=`date +'%a, %d %b %Y %H:%M %z'`
/usr/sbin/sendmail -t <<EOS
From: a.shigi@gmail.com
To: shigi8086@gmail.com
Date: ${DATE}
Subject: ${HOSTNAME} Force power off
Content-Type: text/plain; charset="UTF-8"

バッテリーが切れるためシャットダウンします。
EOS
