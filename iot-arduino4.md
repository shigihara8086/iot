[TOC]

# ■ IoTにおけるArduino その４

------

## ▼ 計測と安定性

Arduinoに限らず、インターネットを介してデータを送受信する場合、確実性を確保するのが肝要であるが、なかなかこれが難しい。安定して計測を行うためには、

- 機器の安定性
- 通信の安定性

を確保する必要がある。そのため、複数の測定方法、通信方法を使用してそれぞれの得失について検証を行っている。以下それぞれについての検証結果である(まだ検証中のものもある)。

## ▼ LTE、WiFi経由のプッシュ型測定

**LTE-TimerPush型 : 電源なし、WiFiなし、メンテナンス困難**

このプロジェクトでの本来の計測・通信方法である。１時間毎に起動し、計測器本体とLTEドングルを起動して計測を行い、計測結果をサーバに送信後、起動タイマーを設定してからdeep sleepに入る動作を繰り返す。

機器自体の安定性は良好なのだが、LTEドングルを起動し通信を行う必要があるため、安定して通信を行うことがなかなか難しい。

![esp32_boot-pinpoint=1524556829,1525248029](./img/esp32_boot-pinpoint=1524556829,1525248029.png)

このグラフはdeep sleepの回数をカウントしたものである。２４回deep sleepすると強制再起動するので、ノコギリ状のグラフとなる。平坦な部分は測定データが送られてこなかった部分になる。不定期に数時間に渡って測定データが送られて来ないことがあり、LTEの通信の不安定部分と思われる。

## ▼ WiFi経由のプッシュ型測定

**WiFi-TimerPush型 : 電源なし、WiFiあり、メンテナンス困難**

LTE,WiFi経由の測定からLTEドングルの起動停止を行わず、LTEドングルには常時給電する方法である。LTE経由に関する不安定さがないので、この方法も安定して計測することが可能である。

![esp32_bme280_boot-pinpoint=1525140029,1525248029](./img/esp32_bme280_boot-pinpoint=1525140029,1525248029.png)

LTE経由と同じソフトウエアを使用しているので、通信は光回線経由で行われている。LTE経由のような通信できない現象は全く観測できていない。

## ▼ WiFi経由のプッシュ型測定

**WiFi-Push型 : 電源あり、WiFiあり、メンテナンス困難**

測定器の電源は常時供給されているが、消費電力を削減するためと機器自体のハングアップ対策のため、測定後、比較的短いdeep sleep動作を行う。測定間隔はmuninのインターバルより短い２分とし、いわばオーバーサンプリングのような計測を行っている。ウォッチドッグタイマーも併用しているので、安定性は一番良好である。

![doit_esp32_47a4ae30_coretemp-pinpoint=1525140029,1525248029](./img/doit_esp32_47a4ae30_coretemp-pinpoint=1525140029,1525248029.png)

## ▼ WiFi経由のプル型測定

**WiFi-Pull型 : 電源あり、WiFiあり、メンテナンス容易**

測定器がmunin-nodeとして振る舞い、TCPの4949番ポートでサーバとして動作し、muninからデータを取得するタイプの測定方法である。muninと組み合わせて使う場合は一番相性が良い。

![temp-pinpoint=1525140030,1525248030](./img/temp-pinpoint=1525140030,1525248030.png)

上のグラフは５分毎の測定がきれいにできているが、何らかの違いで下のように、

![temp-pinpoint=1525140637,1525248637](./img/temp-pinpoint=1525140637,1525248637.png)

ポツポツと測定データが途切れてしまうものもある。ソフトウエア自体は同じものを使用しているので、ESP32の個体差によるものではないかと思われるが、他に原因がある可能性もまだある。このあたりは、まだ検証中である(ちなみにグラフのギャップは、ESP32の個体差かもしれないと考えて別のESPS32に変更したため、計測値に不連続が現れている)。

この通信の不安定性についてだが、自宅のLAN環境では、この**WiFi経由のプル型測定**と**LTE、WiFi経由のプッシュ型測定**間で干渉が発生していることがわかった。**WiFi経由のプル型測定**をすべて停止したところ、**LTE、WiFi経由のプッシュ型測定**で欠落していた通信がかなり改善され、ほとんど通信の欠落が起こらなくなった。どのようなメカニズムで干渉が起こっていたかは不明なのだが、とりあえず、この２つの測定は共存できないことが判明した。

## ▼ どの方法で測定すべきか？

一概にどの方法が良いとは言えないのが現状である。つまり、

- 外部電源が確保できるか？
- WiFiが接続可能か？
- 容易にメンテナンスできる場所か？

といった条件を考慮しないと、どの方法が適しているかは判断できないということである。なので、どの方法であっても最大限の安定性を確保しておき、上記の条件に適合する測定方法を採用することになる。

## ▼ ESP32のコア温度

|            サンプル１             |       サンプル２        |
| :-------------------------------: | :---------------------: |
| ![](./img/esp32_coretemp-day.png) | ![](./img/temp-day.png) |
|          放射温度計：26℃          |     放射温度計：26℃     |

上のグラフはほぼ同じ条件で測定した２つのESP32のコア温度である。左のCPUは31℃、右のCPUは68℃と大きな差がある。そこでモジュールの表面温度を放射温度計で調べてみるとどちらも26℃と差がない。ということはESP32内臓のCore温度センサー自体の絶対精度が全く使い物にならないものであるということだと思われる。なので、Core温度を見て、負荷を制限したり、警告を出したりするような場合、絶対値ではなく相対値でコントロールするような工夫が必要になると思われる。

中華CPUに限らず、中華製品は、検査工程が無いか通り一遍の検査しかしていないと思われ、こういった信じられないようなばらつきが当たり前のようにある。なので、こういう事があるということを前提に使用する必要がある。これは安さとのトレードオフなので、安い製品を使おうとすれば受け入れざるを得ないリスクと言えるだろう。

## ▼ SONYのArduino互換ボード

![2018-09-18 12.36.38](img/2018-09-18%2012.36.38.jpg)

SONYからArduino IDEで開発可能なマイコンボードが発売された。特徴としては、GPSチップが内蔵されていることのようだ。通信機能はBluetoothしかないようなので、利用するにはBluetoothをどううまく使うかにかかっていると思われる。とりあえず本体だけは買ってみた。オプションとして拡張ボードやカメラが接続できるようだが、現時点では詳細は不明である。

実際に入手して見てみると、小型だし、基板はきれいだし非常に良い製品に見える。しか～しである。随所に「**日本製の悪いところ**」が見受けられるのが残念である。

- 価格が高い。
- メインボードのI/O基準電圧が**1.8V**
- メインボードのコネクタが特殊。
- 実質的にUno互換の拡張ボードが必須になっている

まだ詳しくは見ていないのでアレだが、ざっと見てもコスト増になる**専用品**を使わざるを得ないようになっていて、ESP32などと比べると、コスト的には雲泥の差がある。コスト的に見て、これをメインの開発に使おうとは思えない商品である。

I/O基準電圧が1.8Vというもは痛い。I2Cインターフェースなどは、3.3Vないし5Vというのが一般的なため、1.8Vに接続するためには、レベルコンバータなどを使用しなければならないなど、3.3Vや5Vであれば不要な回路を追加する必要があり、これもコストに響く。

コネクタ部分は、ピッチ自体は2.54mmピッチの汎用ピッチになっているが、コネクタの高さが低いので、普通の端子を使うと隙間が空いてしまう。場合によっては、接触不良などを起こす可能性がある。一応、専用のピンコネクタが売られているようだが、このような製品に専用のコネクタが必要というのはいただけない。

先程のようにI/O基準電圧が1.8Vのため、3.3V,5Vのデバイスを接続する最も簡単な方法はこのUno互換のI/Fボードを使う方法になってしまう。このボードには必要がなくてもSDカードI/Fなどがついているので、その分消費電力も増える。本来なら高速、低消費電力を売りにするところが、無駄な回路がついていては、本末転倒ではないかと思う。

とりあえず拡張ボードも購入して、センサー等を接続して使ってみるつもりである。

## ▼ 変わり種ESP32

![IMG_0033](img/IMG_0033.jpg)

**ESP32**に小さな**有機EL**のディスプレイを搭載したもの。測定値をすぐにモニターできるので、用途によっては便利だと思う。これに**INA226**をつけて、電圧・電流・消費電力計を作ってみようと思っている。カメラで撮ると少し汚く写っているが、実際にはもう少しきれいに表示されている。価格は2200円程度と少し高め。

![IMG_0034](img/IMG_0034.jpg)

これも少し変わったArduino互換ボードである。USBコネクタの大きさから解るように非常に小さい。ネットワーク機能などはないが、組み込み的に使うのには好適ではないかと思われる。入手したのはESP32よりも後。当初はもう少し早く来るはずだったのだが、indigogoというスタートアップのサイトから注文したものだったので、製品ができるまで少々遅れがあったようだ(届かなくても仕方がないかなと思っていたので、まあ届いて良かった)。

![mg_004](./img/img_0047.jpg)

これは、ESP-WROOM-32用のモジュールコネクタが付いたボード。ブレッドボードで使う場合、NodeMCU-32Sのようなモジュールを使うことが多いが、消費電力の観点からはESP-WROOM-32そのもののみを実装したほうが良い場合がある。その場合、このようなボードで予めプログラミングしてから実装するような方法もありえる。またチップ部分のみ交換して使えるので、複数のチップを交換して使う用途にも向きそうだ。

## ▼ ESP32-WROVER-B

![ESP32-WROVER-B](./img/IMG_0006.jpeg)

内容はESP32とほぼ同じだがパッケージがDIP(端子が左右にのみ配置されている)になっており、はんだ付けするのが楽になっているようだ。

またこのDev-Kitは写真ではわからないが、裏に18650リチウムイオン電池が入るようになっているため、バッテリー駆動が可能になっている。またUSBに電源をつなげばバッテリーの充電も可能になっているため、5V出力の太陽電池と組み合わせると外部給電なしの動作が可能になっている。

価格もAmazonで1900円程度なので、気軽に使えうのが良いところである。

## ▼ ESP32-CAM

![ESP32-CAM](./img/2019-06-24 23.42.53.jpg)

ESP32にカメラを付けたもの。

## ▼ M5STICK-C

![M5STICK-C](./img/IMG_0012.jpeg)

![M5STICK-C裏](./img/IMG_0013.jpeg)

M5STACKと同じメーカーから発売された小型のESP32モジュール。

コンパクトな筐体(48mmx24mmx14mm)に沢山のセンサー・デバイスが詰め込まれている。またGROVEインターフェースなどもあるため、工夫次第では非常にコンパクトな測定器を作ることができる。また、短時間であれば内臓のバッテリーで動作させることができる。価格も(M5STACKに比べれば)安いので、大量にセンサーをばらまくような用途に向いていると思う。

## ▼ WINGONEERのモジュール

![IMG_0083](img/IMG_0083.jpg)

数少ない、15x2ピン配列のモジュール。機能的にはESP32-WROVER-Bから電池ボックスを取ったもの相当ではないかと思われる。とはいえバッテリーコネクタはあるので、バッテリーさえ繋げばESP32-WROVER-Bとほとんど同じになる。が、詳細は不明。一般のESP32モジュールとは違ってあの特徴的な金属シールドが存在せず、CPUやメモリーは直接基板上に実装している。

写真左端の金属部分がWi-Fiアンテナである。で、どこを見ても技適やULの認証マークがないという非常に怪しいもの。右上の金属部分はSDカードインターフェースである。これがあるので、15ピンで済んでいるのではないかと思われる。

ピン数は15ピンだがUSBコネクタまでがかなりはみ出しているので、基板からはみ出した状態で実装しなければならないのが悩ましい。

まあ、技適を通っているとは思えないので、日本でおおっぴらに使うことは不可能な製品である。

## ▼ ESP32-PICO-D4のモジュール

ESP32-PICO-D4というより小型モジュールに適したESP32を採用したモジュール。

![IMG_0106](img/IMG_0106.jpg)

基板アンテナではなく、白いチップアンテナを使用している。基板上には、ESP32-PICO-D4モジュール、電源関係のチップ、UARTチップという最小構成になっている。裏面には部品は実装されていない。

## ▼ M5Stick ATOM Matrix & ATOM Lite

![IMG_0008](img/IMG_0008.jpg)ATOM Lite。サイズは、24mmx24mmx10mm。価格はスイッチサイエンスで、968円。

![IMG_0010](img/IMG_0010.jpg)

ATOM Matrix。サイズは、24mmx24mmx14mm。厚みが少し大きい。価格はスイッチサイエンスで、1397円。

久々にM5Stack系のESP32みジュールを入手した。

基本的な部分は、M5STICK-Cと同じでESP32-PICOを使用している。Matrixには5x5マトリックスのLEDとMPU6886 6軸センサを内蔵している。LiteにはLEDが一つだけある。どちらも非常に小型の筐体なので、単独で使用することよりも基板上にビルトインするような使い方があっているような気がする。いずれにしろ、小さく安いので気軽に組み込んで使えると思う。

## ▼ TTGO T-Camera

![IMG_0014](img/IMG_0014.jpeg)

基本的には、ESP32-CAMと同じだが、外部I/OがI2Cのみ、USBインターフェースが付いている、OLED表示機がついている、何故かmicroSDカードインターフェースがない、人感センサーがついているなど、中身はかなり違っている。基本的にはカメラを利用したものとなるので利用方法はかなり限られる。

## ▼ ATOM GPS Development Kit (M8030-KT)

![IMG_0015](img/IMG_0015.jpg)

ATOM LiteにGPSユニットを組み合わせたもの。ATOM Liteが付属していて、3000円ほどで入手できるのはリーズナブル。microSDカードに位置情報が記録できるようになっている。サンプルプログラムで動作を確認したが、位置の精度はあまり良くないようだ。数百メートルの誤差は未婚で置かなければならない。

## ▼ 18650バッテリーシールド

![IMG_0006](img/IMG_0006-1608110331412.jpeg)

以前入手したものとはかなり違っている。まず電源スイッチがタクトスイッチになった。

![IMG_0007](img/IMG_0007.jpeg)

~~一番の違いは、充電用のコネクタが**Type-C**になってしまった。~~むうー、この手のものもType-Cの流れに乗ってきているんだなあと。で、反対側を見たらMicro-Bのコネクタもあった。

訂正：Type-Cコネクタ経由で充電できなかった。原因は不明。Micro-Bの方から充電したら、正常に充電ランプが付きレベルも表示するようになった。

![IMG_0008](img/IMG_0008.jpeg)

基盤裏はこんな感じ。中央には、

![IMG_0011](img/IMG_0011.jpeg)

というチップがありこれで充放電をコントロールしているらしい。基板上に５個のLEDがついているがこれは充電レベルのインジケータのようだ。Micro-B側には別の充電コントローラーがついているようにも見える。最大5V/3.4Aまで供給できるようだ。

で、仕様を確認しようとAmazonの商品ページを見ると、写真が古いもののままでコネクタの用途(Type-Cの場合入力か出力かがシルクがないと判断できない)がわからない。が、探してみたら18650を4本使うタイプが同じ回路構成のようでType-Cは充電側のようだ。

| 裏面写真                                                    | 裏面説明                                                    |
| ----------------------------------------------------------- | ----------------------------------------------------------- |
| ![51SO0h3IMVL._AC_SL1000_](img/51SO0h3IMVL._AC_SL1000_.jpg) | ![517P5VABA3L._AC_SL1000_](img/517P5VABA3L._AC_SL1000_.jpg) |

なのだが、先の訂正のようにType-C端子から充電はできなかった。

